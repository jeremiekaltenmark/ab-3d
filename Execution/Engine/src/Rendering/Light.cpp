/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 25-06-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#include "Engine/Engine.h"
#include "Rendering/Light.h"

using namespace Engine;

////////////////////////////////////////////////////////////////////////
Light::Light()
{
//    m_type = LIGHT;
	//m_Dirty = false; // do not update if not initialized
	m_Properties.Color = Vec3(1.0f, 1.0f, 1.0f);
	m_Properties.AmbientIntensity = 0.15f;
	m_Properties.Direction = Vec3(0.0f, 0, 1.0f);
	m_Properties.Direction.Normalize();
	m_Properties.DiffuseIntensity = 0.9f;


    mPos = CommonSharedPtr<ViewMatrix>(new ViewMatrix());
    mComponents[&typeid(ViewMatrix)] = mPos;
	mPos->SetForward(m_Properties.Direction);
	//m_Dirty = true;
}

////////////////////////////////////////////////////////////////////////
Light::Light(const Light& /*aCopy*/)
{
//    m_type = aCopy.m_type;
}

////////////////////////////////////////////////////////////////////////
Light::~Light()
{
	//Logger::LogDebugSoft("~Light", DEBUG_SOFT_INFO);
}

////////////////////////////////////////////////////////////////////////
void Light::InternalInit()
{
    Program::InternalInit();
	m_DirColor     = &GetUniform("g_DirectionalLight.Base.Color");
	m_DirAmbIntens = &GetUniform("g_DirectionalLight.Base.AmbientIntensity");
	m_DiffDir      = &GetUniform("g_DirectionalLight.Direction");
	m_DiffIntens   = &GetUniform("g_DirectionalLight.Base.DiffuseIntensity");
	m_LightMVPIndex	   = &GetUniform("LightMat");

    mPos->Init();
	mPos->SetPosition(Engine::Vec3(0.f, 3.f, -6.f));
	mPos->SetUpVector(Engine::Vec3(0, 1, 0));
	mPos->LookAt(Vec3(0, 0, 0));

	m_Properties.Direction = mPos->GetForward();
}


bool Light::CustomRegister(const Uniform& a_Uniform)
{
    if (a_Uniform.mName == "g_DirectionalLight.Base.Color" ||
        a_Uniform.mName == "g_DirectionalLight.Base.AmbientIntensity" ||
        a_Uniform.mName == "g_DirectionalLight.Direction" ||
        a_Uniform.mName == "g_DirectionalLight.Base.DiffuseIntensity" ||
        a_Uniform.mName == "LightMat")
    {
        return true;
    }

    return false;
}

////////////////////////////////////////////////////////////////////////
void Light::Update()
{
	mPos->Update();
	m_Properties.Direction = mPos->GetForward();

	glUniform3f(m_DirColor->mLocation, m_Properties.Color.X(), m_Properties.Color.Y(), m_Properties.Color.Z());
	glUniform1f(m_DirAmbIntens->mLocation, m_Properties.AmbientIntensity);
	glUniform3f(m_DiffDir->mLocation, m_Properties.Direction.X(), m_Properties.Direction.Y(), m_Properties.Direction.Z());
	glUniform1f(m_DiffIntens->mLocation, m_Properties.DiffuseIntensity);

	glUniformMatrix4fv(m_LightMVPIndex->mLocation, 1, GL_TRUE, mPos->GetViewMatrix().mLineMat);
//    Logger::Log(mPos->GetPosition());
}

////////////////////////////////////////////////////////////////////////
/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 09-07-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#include "Engine/Engine.h"
#include "Rendering/GraphicDevice.h"
#include "Common/Singleton.cpp"

#include "Components/Material/Shader.h"
#include "Components/Material/Program.h"
#include "Components/Material/Texture.h"
#include "Components/Material/Material.h"
#include "Rendering/Light.h"
#include "Components/GraphicComponent.h"

using namespace Engine;

////////////////////////////////////////////////////////////////////////
template class Engine::Singleton<Engine::GraphicDevice>;

////////////////////////////////////////////////////////////////////////
GraphicDevice::GraphicDevice()
	:
m_Materials(),
m_Textures(),
m_DefaultTexture(NULL)
{
}


////////////////////////////////////////////////////////////////////////
GraphicDevice::~GraphicDevice()
{
	m_DefaultTexture.reset();
	//Logger::LogDebugSoft("~GraphicDevice", DEBUG_SOFT_INFO);
	m_Materials.clear();
	m_Textures.clear();
}

////////////////////////////////////////////////////////////////////////
void GraphicDevice::InitShaders()
{
	Logger::LogLine("--\nBuild engine shaders");

	GraphicComponent t; // on MacOs, we need to create at least one vao before compiling shaders

//	CommonSharedPtr<VertexShader> tSimpleVert = Shader::LoadShader<VertexShader>("shaders/SimpleVertexShader.vert");
//	if (tSimpleVert->Build())
//	{
//		CommonSharedPtr<FragmentShader> tSimpleFrag = Shader::LoadShader<FragmentShader>("shaders/SimpleFragmentShader.frag");
//		if (tSimpleFrag->Build())
//		{
//			CommonSharedPtr<Program> tSimpleProg(new Program);
//			tSimpleProg->Attach(tSimpleVert, tSimpleFrag);
//			if (tSimpleProg->Link())
//				m_ShadersProgram[PROGRAM_SIMPLE_3D] = tSimpleProg;
//			else
//				Logger::LogLine("\tProgram_simple_3d failed link");
//		}
//		else
//			Logger::LogLine("\tProgram_simple_3d failed build FragmentShader");
//	}
//	else
//		Logger::LogLine("\tProgram_simple_3d failed build VertexShader");
//
//	CommonSharedPtr<VertexShader> tTexVert = Shader::LoadShader<VertexShader>("shaders/TextureVertexShader.vert");
//	if (tTexVert->Build())
//	{
//		CommonSharedPtr<FragmentShader> tTexFrag = Shader::LoadShader<FragmentShader>("shaders/TextureFragmentShader.frag");
//		if (tTexFrag->Build())
//		{
//			CommonSharedPtr<Program> tTexProg(new Program);
//			tTexProg->Attach(tTexVert, tTexFrag);
//			if(tTexProg->Link())
//				m_ShadersProgram[PROGRAM_TEXTURE_3D] = tTexProg;
//			else
//				Logger::LogLine("\tProgram_texture_3d failed link");
//		}
//		else
//			Logger::LogLine("\tProgram_texture_3d failed build FragmentShader");
//	}
//	else
//		Logger::LogLine("\tProgram_texture_3d failed build VertexShader");
//
	CommonSharedPtr<VertexShader> tLitVert = Shader::LoadShader<VertexShader>("shaders/LitTextureVertexShader.vert");
	if (tLitVert->Build())
	{
		CommonSharedPtr<FragmentShader> tLitFrag = Shader::LoadShader<FragmentShader>("shaders/LitTextureFragmentShader.frag");
		if (tLitFrag->Build())
		{
			CommonSharedPtr<Program> tLitProg(new Light);
			tLitProg->Attach(tLitVert, tLitFrag);
			if (tLitProg->Link())
				m_ShadersProgram[PROGRAM_LIT_TEXTURE_3D] = tLitProg;
			else
				Logger::LogLine("\tProgram_lit_texture_3d failed link");
		}
		else
			Logger::LogLine("\tProgram_lit_texture_3d failed build FragmentShader");
	}
	else
		Logger::LogLine("\tProgram_lit_texture_3d failed build VertexShader");

	CommonSharedPtr<VertexShader> tShadowVert = Shader::LoadShader<VertexShader>("shaders/ShadowMap.vert");
	if (tShadowVert->Build())
	{
		CommonSharedPtr<FragmentShader> tShadowFrag = Shader::LoadShader<FragmentShader>("shaders/ShadowMapFragmentShader.frag");
		if (tShadowFrag->Build())
		{
			CommonSharedPtr<Program> tShadowProg(new Program);
			tShadowProg->Attach(tShadowVert, tShadowFrag);
			if (tShadowProg->Link())
				m_ShadersProgram[PROGRAM_SHADOW_MAP] = tShadowProg;
			else
				Logger::LogLine("\tProgram_shadow_map failed link");
		}
		else
			Logger::LogLine("\tProgram_shadow_map failed build FragmentShader");
	}
	else
		Logger::LogLine("\tProgram_shadow_map failed build VertexShader");

}

////////////////////////////////////////////////////////////////////////
CommonSharedPtr<Material>& GraphicDevice::GetMaterial(int a_index)
{
	return m_Materials[a_index];
}

//////////////////////////////////////////////////////////////////////////
CommonSharedPtr<Material>& GraphicDevice::GetMaterial(PROGRAMS a_ProgID, const CommonSharedPtr<Texture>& a_Tex)
{
	for (MatContainer::iterator it = m_Materials.begin(); it != m_Materials.end(); ++it)
	{
        if ((*it)->GetShader() == m_ShadersProgram[a_ProgID] && (*it)->GetTexture(Material::DIFFUSE) == a_Tex)
		{
			return *it;
		}
	}

	// material doesn't exist yet
    CommonSharedPtr<Material> t_Mat(new Material);
    t_Mat->SetTexture(a_Tex, Material::DIFFUSE);
    t_Mat->SetTexture(GetDefaultNormalMap(), Material::NORMAL_MAP);
	t_Mat->SetShader(m_ShadersProgram[a_ProgID]);

	m_Materials.AddFirst(t_Mat);
	return m_Materials.getFirst();
}

//////////////////////////////////////////////////////////////////////////
CommonSharedPtr<Material>& GraphicDevice::GetMaterial(PROGRAMS a_ProgID,
                                                      const CommonSharedPtr<Texture>& a_DiffuseTex,
                                                      const CommonSharedPtr<Texture>& a_ShadowTex,
                                                      const CommonSharedPtr<Texture>& a_NormalTex)
{
    for (MatContainer::iterator it = m_Materials.begin(); it != m_Materials.end(); ++it)
    {
        if ((*it)->GetShader() == m_ShadersProgram[a_ProgID] &&
            (*it)->GetTexture(Material::DIFFUSE) == a_DiffuseTex &&
            (*it)->GetTexture(Material::SHADOW_MAP) == a_ShadowTex &&
            (*it)->GetTexture(Material::NORMAL_MAP) == a_NormalTex)
        {
            return *it;
        }
    }

    // material doesn't exist yet
    CommonSharedPtr<Material> t_Mat(new Material);
    t_Mat->SetTexture(a_DiffuseTex, Material::DIFFUSE);
    t_Mat->SetTexture(a_ShadowTex, Material::SHADOW_MAP);
    if(a_NormalTex != NULL)
        t_Mat->SetTexture(a_NormalTex, Material::NORMAL_MAP);
    else
        t_Mat->SetTexture(GetDefaultNormalMap(), Material::NORMAL_MAP);
    t_Mat->SetShader(m_ShadersProgram[a_ProgID]);

    m_Materials.AddFirst(t_Mat);
    return m_Materials.getFirst();
}

//////////////////////////////////////////////////////////////////////////
CommonSharedPtr<Material>& GraphicDevice::GetMaterial(PROGRAMS a_ProgID,
                                                      const CommonSharedPtr<Texture>& a_DiffuseTex,
                                                      const CommonSharedPtr<Texture>& a_ShadowTex)
{
    for (MatContainer::iterator it = m_Materials.begin(); it != m_Materials.end(); ++it)
    {
        if ((*it)->GetShader() == m_ShadersProgram[a_ProgID] &&
            (*it)->GetTexture(Material::DIFFUSE) == a_DiffuseTex &&
            (*it)->GetTexture(Material::SHADOW_MAP) == a_ShadowTex)
        {
            return *it;
        }
    }

    // material doesn't exist yet
    CommonSharedPtr<Material> t_Mat(new Material);
    t_Mat->SetTexture(a_DiffuseTex, Material::DIFFUSE);
    t_Mat->SetTexture(a_ShadowTex, Material::SHADOW_MAP);
    t_Mat->SetTexture(GetDefaultNormalMap(), Material::NORMAL_MAP);
    t_Mat->SetShader(m_ShadersProgram[a_ProgID]);

    m_Materials.AddFirst(t_Mat);
    return m_Materials.getFirst();
}

//////////////////////////////////////////////////////////////////////////
i32 GraphicDevice::GetMaterialIndex(const CommonSharedPtr<Material>& a_Mat) const
{
	int cpt = 0 ;
	for (MatContainer::const_iterator it = m_Materials.begin(); it != m_Materials.end(); ++it, ++cpt)
	{
		if ( *it == a_Mat )
		{
			return cpt;
		}
	}
	return ENGINE_FAILURE;
}


////////////////////////////////////////////////////////////////////////
CommonSharedPtr<Texture>& GraphicDevice::GetTexture(const CommonString& a_TextureName, bool a_IsModelTexture)
{
	if(a_IsModelTexture)
	{
		return GetModelTexture(a_TextureName); // will look inside textures and models folders
	}

	for (TexContainer::iterator it = m_Textures.begin(); it != m_Textures.end(); ++it)
	{
		if (it->first == a_TextureName)
		{
			return it->second;
		}
	}

	CommonSharedPtr<Texture> t_tex(new Texture());
	if (t_tex->LoadTexture(a_TextureName.GetChars()))
	{
		m_Textures.Add(a_TextureName, t_tex);
	}
	else // load default texture
	{
		t_tex->LoadTexture("textures/damier.png");
		t_tex->SetIsDefault(true);
		m_Textures.Add(a_TextureName, t_tex);
	}

	return m_Textures[a_TextureName];
}

CommonSharedPtr<Texture>& GraphicDevice::GetDefaultTexture()
{
	if (m_DefaultTexture != NULL)
		return m_DefaultTexture;

	CommonSharedPtr<Texture> t_tex(new Texture());
	t_tex->LoadTexture("textures/damier.png");
	t_tex->SetIsDefault(true);
	m_DefaultTexture = t_tex;
	return m_DefaultTexture;
}

CommonSharedPtr<Texture>& GraphicDevice::GetDefaultNormalMap()
{
    if (m_DefaultNormalMap != NULL)
        return m_DefaultNormalMap;

    CommonSharedPtr<Texture> t_tex(new Texture());
    t_tex->LoadTexture("textures/normal_up.jpg");
    t_tex->SetIsDefault(true);
    m_DefaultNormalMap = t_tex;
    return m_DefaultNormalMap;
    
}



////////////////////////////////////////////////////////////////////////
CommonSharedPtr<Texture>& GraphicDevice::GetModelTexture(const CommonString& a_ModelTextureName)
{
	CommonString t_ModelsTexPath;
	t_ModelsTexPath += "models/"; t_ModelsTexPath += a_ModelTextureName;
	CommonString t_TexturesTexPath;
	t_TexturesTexPath += "textures/" ; t_TexturesTexPath += a_ModelTextureName;

	for (TexContainer::iterator it = m_Textures.begin(); it != m_Textures.end(); ++it)
	{
		if (it->first == t_ModelsTexPath || it->first == t_TexturesTexPath)
		{
			return it->second;
		}
	}

	CommonSharedPtr<Texture> t_tex(new Texture());
	if (t_tex->LoadTexture(t_ModelsTexPath.GetChars()))
	{
		m_Textures.Add(t_ModelsTexPath, t_tex);
		return m_Textures[t_ModelsTexPath];
	}
	else // load default texture
	{
		if (t_tex->LoadTexture(t_TexturesTexPath.GetChars()))
		{
			m_Textures.Add(t_TexturesTexPath, t_tex);
		}
		else // load default texture
		{
			t_tex->LoadTexture("textures/damier.png");
			t_tex->SetIsDefault(true);
			m_Textures.Add(t_TexturesTexPath, t_tex);
		}

		return m_Textures[t_TexturesTexPath];
	}

}
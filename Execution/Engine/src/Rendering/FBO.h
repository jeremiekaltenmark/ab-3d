/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
*	Project PER - Internal Game Engine
*	Organisation: www.jeremkalt.xyz
*
*	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
*	Created on: 11-02-2015
*
*	Copyright (c) 2015 CDRIN. All rights reserved.
*/

#ifndef FBO_H
#define	FBO_H

#include "Engine/Engine.h"
#include "Maths/Vec2.h"

namespace Engine
{
	class FBO
	{
	public:
		FBO();
		virtual ~FBO();

		virtual bool Init(uint a_WindowsWidth, uint a_WindowsHeight) = 0;
		virtual void BindForWriting() = 0;
		virtual void BindForReading(GLenum a_TextureUnit) = 0;
		virtual void BindDefaultFBO() = 0;

		GLuint GetFBO() { return m_fbo; }
		GLuint GetDepth() { return m_DepthTextureID; }
		GLuint GetColor() { return m_DepthTextureID; }
		GLuint GetStencil() { return m_StencilTextureID; }

		GLenum GetDepthFormat() { return m_DepthFormat; }
		GLenum GetColorFormat() { return m_ColorFormat; }
		GLenum GetStencilFormat() { return m_StencilFormat; }

		Vec2 GetSize() { return Vec2((float) m_Width, (float) m_Height); }
		uint GetWidth() { return m_Width; }
		uint GetHeight() { return m_Height; }

		virtual void* GetDepthPixels() { return nullptr;} 
		virtual void* GetColorPixels() { return nullptr;} 
		virtual void* GetStencilPixels() { return nullptr; }

	protected:
		GLuint m_fbo;
		GLuint m_DepthTextureID;
		GLuint m_ColorTextureID;
		GLuint m_StencilTextureID;

		uint m_Width;
		uint m_Height;
		GLenum   m_DepthFormat;
		GLenum   m_ColorFormat;
		GLenum m_StencilFormat;

		GLenum   m_DepthType;
		GLenum   m_ColorType;
		GLenum m_StencilType;


		GLenum   m_DepthTarget;
		GLenum   m_ColorTarget;
		GLenum m_StencilTarget;

	private:

	};

}

#endif
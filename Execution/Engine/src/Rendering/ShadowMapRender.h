/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project MoteurCDRIN - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: Jérémie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 26-01-2015
 *
 *	Copyright (c) 2015 CDRIN. All rights reserved.
 */

#ifndef Engine_Shadow_Map_Renderer_H
#define Engine_Shadow_Map_Renderer_H

//#include "IO/Camera.h"
#include "IO/Viewport.h"
#include "Rendering/Renderer.h"
#include "Rendering/RenderVisitor.h"
#include "Components/Material/ShaderWrapper.h"

namespace Engine
{
    class Component;
    class GraphicComponent;
    class Material;
    class Viewport;
	class ShadowMapFBO;
	class GraphicDevice;
    class ViewMatrix;
}

extern CommonSharedPtr<const Engine::Material> NullMaterial;

namespace Engine
{
    class ShadowMapRender : public RendererVisitor
    {
    public:
        ShadowMapRender();
        ~ShadowMapRender();

        void Update(float aElapsedTime,
                    ArrayGraphicComponents& aDrawables,
                    CommonSharedPtr<ViewMatrix>& aViewPosition,
                    CommonSharedPtr<Viewport>& aViewport,
                    bool a_ForceCamera);
        virtual void Update(float aElapsedTime,
                            ArrayGraphicComponents& aDrawables,
                            CommonSharedPtr<ViewMatrix>& aViewPosition,
                            CommonSharedPtr<Viewport>& aViewport);

		void SetSubroutinePass(int idx);

		void PostUpdate(float aElapsedTime,
                        ArrayGraphicComponents& aDrawables,
                        CommonSharedPtr<ViewMatrix>& aViewPosition,
                        CommonSharedPtr<Viewport>& aViewport);

        virtual void DebugUpdate(float aElapsedTime,
                                 ArrayGraphicComponents& aDrawables,
                                 CommonSharedPtr<ViewMatrix>& aCamPos,
                                 CommonSharedPtr<Viewport>& aViewport) ;
        
        virtual void Render(CommonSharedPtr<Component>& a_Component);
        virtual void Render(CommonSharedPtr<GraphicComponent>& a_Component);

        void VisitComponents(CommonSharedPtr<GraphicComponent>& a_GraphComp);
        void VisitChildren(CommonSharedPtr<GraphicComponent>& a_GraphComp);

        inline void SetViewport(CommonSharedPtr<Viewport>& aViewport) { m_CurrentViewport = aViewport; }
		virtual void InternalInit();


    private:
		Subroutine mFirstSubroutine;
		Subroutine mSecondSubroutine;
		CommonSharedPtr<Material> m_UberMat;		// to get ShadowMapping in second pass
		CommonSharedPtr<Material> m_ShadowMat;		// for target object
        CommonSharedPtr<Viewport> m_CurrentViewport;
        CommonSharedPtr<ViewMatrix> m_PostRenderCamera;
        CommonSharedPtr<ViewMatrix> m_ShadowingCamera;
		GraphicDevice* m_Device;
		CommonSharedPtr<const Material> m_PreviousMat;

		CommonSharedPtr<ShadowMapFBO> m_ShadowFBO;
		CommonSharedPtr<Engine::GraphicComponent> m_Target;

        ShadowMapRender& operator=(const ShadowMapRender&);
        bool m_SecondPass;
		int m_LightUniformPosition;
		//bool shadowPass = false;
    };
}

#endif

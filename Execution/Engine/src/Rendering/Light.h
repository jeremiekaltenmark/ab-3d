/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 25-06-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */


#ifndef Engine_Light_H
#define Engine_Light_H

#include "Engine/Engine.h"
#include "Components/Material/Program.h"
#include "Maths/Vec3.h"
#include <string>
#include "Components/ViewMatrix.h"

namespace Engine
{

    struct DirectionalLight
    {
        Vec3 Color;
        float AmbientIntensity;
        Vec3 Direction;
        float DiffuseIntensity;
    };

    class Light : public Program
    {
    private:

        const Uniform *m_DirColor;
        const Uniform *m_DirAmbIntens;
        const Uniform *m_DiffDir;
		const Uniform *m_DiffIntens;
		const Uniform *m_LightMVPIndex;

        DirectionalLight m_Properties;

        CommonSharedPtr<ViewMatrix> mPos;

    protected:
        virtual void InternalInit();

    public:
        Light();
        Light(const Light& aCopy);

        virtual ~Light();

        void Update();

        virtual bool CustomRegister(const Uniform& a_Uniform) ;

        const CommonSharedPtr<ViewMatrix>& GetViewMatrix() const {return mPos;}

        inline void UpdateAmbient(float a_NewVal)
        {
            m_Properties.AmbientIntensity = a_NewVal ; 
        }
        inline void AddAmbiant(float a_Offset)
        {
            m_Properties.AmbientIntensity += a_Offset;
        }

        inline void UpdateDiffuseIntensity(float a_NewVal)
        {
            m_Properties.DiffuseIntensity = a_NewVal ;
        }
        inline void AddDiffuseIntensity(float a_Offset)
        {
            m_Properties.DiffuseIntensity += a_Offset;
        }

        inline void UpdateColor(float a_NewVal)
        {
            m_Properties.Color.Set(a_NewVal, a_NewVal, a_NewVal) ;
        }
        inline void UpdateColor(const Vec3& a_NewVal)
        {
            m_Properties.Color = a_NewVal ;
        }

        inline void AddColor(float a_Offset)
        {
            m_Properties.Color.Translate(a_Offset, a_Offset, a_Offset);
        }
        inline void AddColor(const Vec3&  a_Offset)
        {
            m_Properties.Color += a_Offset;
        }

		inline void UpdateDirection(const Vec3&  a_NewVal)
		{
			m_Properties.Direction = a_NewVal;
			m_Properties.Direction.Normalize();
			mPos->SetForward(m_Properties.Direction);
		}

		inline void UpdatePosition(const Vec3&  a_NewVal)
		{
			mPos->SetPosition(a_NewVal);
		}
		inline void LookAt(const Vec3&  a_NewVal)
		{
			mPos->LookAt(a_NewVal);
			m_Properties.Direction = mPos->GetForward();
			m_Properties.Direction.Normalize();
		}

        inline void RotateY(float a_angle)
        {
            m_Properties.Direction.SelfRotateY(a_angle);
            mPos->SetForward(m_Properties.Direction);
        }
    };
    
}
#endif // Engine_Light_H

/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Engine/Engine.h"
#include "Rendering/ShadowMapRender.h"
#include "Rendering/ShadowMapFBO.h"

#include "Common/CommonList.h"
#include "Components/Component.h"
#include "Components/ViewMatrix.h"
#include "Components/GraphicComponent.h"
#include "Components/GraphicPrimitives/Plan.h"
#include "Components/GraphicPrimitives/Mesh.h"
#include "Components/Material/Material.h"
#include "Components/Material/Texture.h"
#include "Components/Material/Shader.h"

#include "IO/Camera.h"
#include "IO/Viewport.h"
#include "Rendering/GraphicDevice.h"

#include "Engine/Application.h"
#include "Components/Scene.h"
#include "Common/CommonList.h"



using namespace Engine;

ShadowMapRender::ShadowMapRender() : m_CurrentViewport(NULL), m_PostRenderCamera(new ViewMatrix()), m_Device(GraphicDevice::GetSingleton()), m_UberMat(NULL), m_ShadowFBO(new ShadowMapFBO), m_Target(NULL), m_SecondPass(false), m_LightUniformPosition(0)
{
	m_PostRenderCamera->Init();
	m_PostRenderCamera->SetPosition(Vec3(0, 0, -6));
	m_PostRenderCamera->SetUpVector(Vec3(0, 1, 0));
	m_PostRenderCamera->LookAt(Vec3(0, 0, -5));

}

ShadowMapRender::~ShadowMapRender()
{
	m_ShadowFBO.reset();
	m_CurrentViewport.reset();
	m_CurrentCamera.reset();
	m_UberMat.reset();
}

void ShadowMapRender::Render(CommonSharedPtr<Component>& a_Component)
{
	if (! a_Component->IsGraphic())
		return;

	CommonSharedPtr<GraphicComponent> t_GraphComp = CommonDynamicSharedCast<GraphicComponent>(a_Component);

	Render(t_GraphComp);
}

void ShadowMapRender::Render(CommonSharedPtr<GraphicComponent>& a_GraphComp)
{
	VisitComponents(a_GraphComp);

	VisitChildren(a_GraphComp);

	if (a_GraphComp->GetMaterial() != NULL) // validate that component can be draw
	{
		a_GraphComp->GetMaterial()->Bind(m_PreviousMat);
        if(m_PreviousMat != a_GraphComp->GetMaterial())
        {
            a_GraphComp->GetMaterial()->GetShader()->FrameUpdate(m_CurrentViewport->GetProjectionMatrix(), *m_CurrentCamera.get());
            m_PreviousMat = a_GraphComp->GetMaterial();
        }

		a_GraphComp->GetMaterial()->GetShader()->Update(a_GraphComp, m_CurrentViewport->GetProjectionMatrix(), *m_CurrentCamera.get());
		a_GraphComp->Draw(/*m_UberMat*/);

	}
}

void Engine::ShadowMapRender::VisitComponents(CommonSharedPtr<GraphicComponent>& a_GraphComp)
{
	CommonSharedPtr<GraphicComponent> t_GraphComp = NULL;
	ArrayComponents& t_Components = a_GraphComp->GetComponents();
	for (ArrayComponents::iterator it = t_Components.begin(); it != t_Components.end(); ++it)
	{
		if (it->second == nullptr || !it->second->IsGraphic())
		{
			continue;
		}
		t_GraphComp = CommonDynamicSharedCast<GraphicComponent>(it->second);
		t_GraphComp->Draw(this);
	}
}

void Engine::ShadowMapRender::VisitChildren(CommonSharedPtr<GraphicComponent>& a_GraphComp)
{
	CommonList<CommonSharedPtr<GraphicComponent> >& t_Children = a_GraphComp->GetChildren();
	for (CommonList<CommonSharedPtr<GraphicComponent> >::iterator it = t_Children.begin(); it != t_Children.end(); ++it)
	{
		(*it)->Draw(this);
	}
}

void Engine::ShadowMapRender::Update(float aElapsedTime, ArrayGraphicComponents& aDrawables, CommonSharedPtr<ViewMatrix>& aViewPosition, CommonSharedPtr<Viewport>& aViewport)
{
    Update(aElapsedTime, aDrawables, aViewPosition, aViewport, false);
}

void Engine::ShadowMapRender::Update(float aElapsedTime, ArrayGraphicComponents& aDrawables, CommonSharedPtr<ViewMatrix>& aViewPosition, CommonSharedPtr<Viewport>& aViewport, bool a_ForceCameraUpdate)
{
	(void) aElapsedTime;

    if(a_ForceCameraUpdate)
        m_CurrentCamera = aViewPosition;
    else
        m_CurrentCamera = m_ShadowingCamera;

	m_CurrentViewport = aViewport;
	if (m_PreviousMat != NullMaterial)
	{
		m_PreviousMat->GetShader()->PostUpdate();
		m_PreviousMat = NullMaterial;
	}
	m_ShadowFBO->BindForWriting();
	glClear(GL_DEPTH_BUFFER_BIT);
    m_UberMat->Bind();
    m_UberMat->GetShader()->FrameUpdate(m_CurrentViewport->GetProjectionMatrix(), *m_CurrentCamera.get());
    m_PreviousMat = m_UberMat;
	SetSubroutinePass(1);

    m_SecondPass = false;
    m_CurrentCamera->Update();
	for (ArrayGraphicComponents::iterator it = aDrawables.begin(); it != aDrawables.end(); ++it)
	{
		(*it)->Draw(this);
	}


	if (m_PreviousMat != NullMaterial)
	{
		m_PreviousMat->GetShader()->PostUpdate();
		m_PreviousMat = NullMaterial;
	}
//	if (m_Delegate)
//	{
//		CommonSharedPtr<FBO> t_FBO = CommonDynamicSharedCast<FBO>(m_ShadowFBO);
//		m_Delegate(t_FBO);
//	}
    m_ShadowFBO->BindDefaultFBO();
}


void Engine::ShadowMapRender::PostUpdate(float aElapsedTime, ArrayGraphicComponents& aDrawables, CommonSharedPtr<ViewMatrix>& aViewPosition, CommonSharedPtr<Viewport>& aViewport)
{
    (void) aElapsedTime;
    m_CurrentCamera = aViewPosition;
    m_CurrentViewport = aViewport;

//	m_ShadowFBO->BindDefaultFBO();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //m_PreviousMat = NullMaterial;
    m_UberMat->Bind(m_PreviousMat);
    m_UberMat->GetShader()->FrameUpdate(m_CurrentViewport->GetProjectionMatrix(), *m_CurrentCamera.get());
    m_PreviousMat = m_UberMat;
	SetSubroutinePass(2);

	m_ShadowFBO->BindForReading(GL_TEXTURE1);
	CheckGlError("m_ShadowFBO->BindForReading(GL_TEXTURE1)", DEBUG_INFO);

    m_CurrentCamera->Update();
    m_SecondPass = true;
    for (ArrayGraphicComponents::iterator it = aDrawables.begin(); it != aDrawables.end(); ++it)
    {
        (*it)->Draw(this);
    }
}


void Engine::ShadowMapRender::DebugUpdate(float aElapsedTime, ArrayGraphicComponents& aDrawables, CommonSharedPtr<ViewMatrix>& aCamPos, CommonSharedPtr<Viewport>& aViewport)
{
    (void) aElapsedTime;
    (void) aDrawables;
    (void) aCamPos;

    m_CurrentCamera = m_PostRenderCamera;
    m_CurrentViewport = aViewport;

	m_UberMat->GetShader()->PostUpdate();
    m_PreviousMat = NullMaterial;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_CurrentCamera->Update();
	m_SecondPass = false;

	m_ShadowFBO->BindForReading(GL_TEXTURE1);
	CheckGlError("m_ShadowFBO->BindForReading(GL_TEXTURE1)", DEBUG_INFO);

    m_Target->Draw(this);
}

void Engine::ShadowMapRender::InternalInit()
{
	CheckGlError("ShadowMapRender::InternalInit", DEBUG_INFO);
	m_ShadowFBO->Init((uint) m_OwnOutput->GetViewport()->GetWidth(), (uint) m_OwnOutput->GetViewport()->GetHeight());
	
	CommonSharedPtr<Texture> tDepthTex = Texture::Wrap(CommonString("FBO_Depth"), GL_TEXTURE_2D, m_ShadowFBO->GetGLTextureHandle());

	if (m_UberMat == NULL)
    {
//        m_UberMat = m_Device->GetMaterial(GraphicDevice::PROGRAM_LIT_TEXTURE_3D, CommonSharedPtr<Texture>(), tDepthTex);
        m_UberMat = m_Device->GetMaterial(GraphicDevice::PROGRAM_LIT_TEXTURE_3D, m_Device->GetTexture("textures/damier.png"), tDepthTex);
		CommonSharedPtr<Program> tShader = m_UberMat->GetShader();
		
		if (tShader->GetSubroutineNbr() == 0)
			Logger::LogDebug("Error in shadowMap subroutines nbr",DEBUG_INFO);
		
		mFirstSubroutine = tShader->GetSubroutine("RecordDepth");
		mSecondSubroutine = tShader->GetSubroutine("shadeWithShadow");

		//m_UberMat->Bind(NullMaterial);
	}
	if (m_ShadowMat == NULL)
	{
		m_ShadowMat = m_Device->GetMaterial(GraphicDevice::PROGRAM_SHADOW_MAP, tDepthTex);
		//m_ShadowMat->Bind(NullMaterial);
	}

    if (m_Target == NULL)
    {
        m_Target = CommonSharedPtr<Engine::GraphicComponent>(new Engine::Plan());
    }
	m_Target->GetTransform()->SetScale(Vec3(3.f, 3.f, 3.f));
	m_Target->GetTransform()->SetRotation(Engine::Quaternion(Vec3(1, 0, 0), Engine::PI1_2));
	m_Target->GetTransform()->SetPosition(Vec3(0,0,2));
	m_Target->SetMaterial(m_ShadowMat);

    CommonSharedPtr<Program> t_Light = GraphicDevice::GetSingleton()->GetShaderProgram(GraphicDevice::PROGRAM_LIT_TEXTURE_3D);

    if(t_Light == NULL)
    {
        Logger::LogDebugSoft("fail to get light", DEBUG_SOFT_INFO);
        return;
    }

    m_ShadowingCamera = t_Light->GetComponent<ViewMatrix>();

    if(t_Light == NULL || m_ShadowingCamera == NULL)
        Logger::LogDebugSoft("fail to add ViewMatrix on light", DEBUG_SOFT_INFO);
    else
        Logger::Log("successfully add viewmatrix component on light");

	m_LightUniformPosition = t_Light->GetUniform("LightMat").mLocation;

}

void Engine::ShadowMapRender::SetSubroutinePass(int idx)
{
//	m_UberMat->GetShader()->Bind();
//	m_PreviousMat = m_UberMat;
	if (idx == 1)
		m_UberMat->GetShader()->SelectSubroutine(mFirstSubroutine);
	else
		m_UberMat->GetShader()->SelectSubroutine(mSecondSubroutine);

}

/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
*	Project PER - Internal Game Engine
*	Organisation: www.jeremkalt.xyz
*
*	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
*	Created on: 11-02-2015
*
*	Copyright (c) 2015 CDRIN. All rights reserved.
*/
#include "FBO.h"

Engine::FBO::FBO() :
m_fbo(0),
m_DepthTextureID(0),
m_ColorTextureID(0),
m_StencilTextureID(0),
m_Width(0),
m_Height(0),
m_DepthFormat(0),
m_ColorFormat(0),
m_StencilFormat(0),
m_DepthType(0),
m_ColorType(0),
m_StencilType(0)
{
}

Engine::FBO::~FBO()
{
	if (m_fbo != 0)
	{
		glDeleteFramebuffers(1, &m_fbo);
	}

	if (m_DepthTextureID != 0)
	{
		glDeleteTextures(1, &m_DepthTextureID);
	}

	if (m_ColorTextureID != 0)
	{
		glDeleteTextures(1, &m_ColorTextureID);
	}

	if (m_StencilTextureID != 0)
	{
		glDeleteTextures(1, &m_StencilTextureID);
	}
}


/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Engine/Engine.h"
#include "ShadowMapFBO.h"
#include "IO/IOManager.h"
#include "Engine/Application.h"
#include "IO/Viewport.h"
#include "IO/AOutput.h"


using namespace Engine;


ShadowMapFBO::ShadowMapFBO()
{
}


ShadowMapFBO::~ShadowMapFBO()
{
}

////////////////////////////////////////////////////////////////////////
bool ShadowMapFBO::Init(uint a_WindowsWidth, uint a_WindowsHeight)
{
	CheckGlError("FBO Init begin with previous error", DEBUG_INFO);
#if defined __APPLE_CC__
    m_Width = a_WindowsWidth *2;
    m_Height = a_WindowsHeight*2;
#else
    m_Width = a_WindowsWidth;
    m_Height = a_WindowsHeight;
#endif

    m_DepthTarget = GL_TEXTURE_2D;
    m_DepthFormat = GL_DEPTH_COMPONENT;
    m_DepthType = GL_FLOAT;

	glGenTextures(1, &m_DepthTextureID);
	glBindTexture(m_DepthTarget, m_DepthTextureID);
	glTexParameteri(m_DepthTarget, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(m_DepthTarget, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(m_DepthTarget, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(m_DepthTarget, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	CheckGlError("FBO glTexParameteri", DEBUG_INFO);

	//NULL means reserve texture memory, but texels are undefined
	//You can also try GL_DEPTH_COMPONENT16, GL_DEPTH_COMPONENT24 for the internal format.
	//If GL_DEPTH24_STENCIL8_EXT, go ahead and use it (GL_EXT_packed_depth_stencil)
	glTexImage2D(m_DepthTarget, 0, GL_DEPTH_COMPONENT24, m_Width, m_Height, 0, m_DepthFormat, m_DepthType, NULL);
	CheckGlError("FBO glTexImage2D", DEBUG_INFO);

//	glGetTexLevelParameteriv(m_DepthTarget, 0, GL_TEXTURE_INTERNAL_FORMAT, (GLint*) (&m_DepthFormat));
//	CheckGlError("FBO Get real Internal format", DEBUG_INFO);
//	glGetTexLevelParameteriv(m_DepthTarget, 0, GL_TEXTURE_DEPTH_TYPE, (GLint*) (&m_DepthType));
//	CheckGlError("FBO Get real Internal type", DEBUG_INFO);


	//-------------------------

#ifdef WIN32
    glGenFramebuffersEXT(1, &m_fbo);
    CheckGlError("glGenFramebuffersEXT", DEBUG_INFO);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, m_fbo);
    CheckGlError("glBindFramebufferEXT", DEBUG_INFO);
    //Attach
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, m_DepthTarget, m_DepthTextureID, 0);
    CheckGlError("glFramebufferTexture2DEXT", DEBUG_INFO);
#else
    glGenFramebuffers(1, &m_fbo);
    CheckGlError("glGenFramebuffers", DEBUG_INFO);
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
    CheckGlError("glBindFramebuffer", DEBUG_INFO);
    //Attach
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, m_DepthTarget, m_DepthTextureID, 0);
    CheckGlError("glFramebufferTexture2D", DEBUG_INFO);
#endif

	//-------------------------
	//Does the GPU support current FBO configuration?
	//Before checking the configuration, you should call these 2 according to the spec.
	//At the very least, you need to call glDrawBuffer(GL_NONE)
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);

	bool ret = true;
	GLenum status;
#ifdef WIN32
	status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
	if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
#else
        status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE)
#endif
	{
		Logger::LogDebug("FB error", DEBUG_INFO);
		Logger::Printf("frame buffer code error: 0x%x\n", status);
		ret = false;
	}

//	//-------------------------
//	//----and to render to it, don't forget to call
//	//At the very least, you need to call glDrawBuffer(GL_NONE)
//	glDrawBuffer(GL_NONE);
//	glReadBuffer(GL_NONE);
	//-------------------------
	//If you want to render to the back buffer again, you must bind 0 AND THEN CALL glDrawBuffer(GL_BACK)
	//else GL_INVALID_OPERATION will be raised
#ifdef WIN32
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
#else
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
#endif
    glDrawBuffer(GL_BACK);
	glReadBuffer(GL_BACK);
	return ret;
}

////////////////////////////////////////////////////////////////////////
void ShadowMapFBO::BindForWriting()
{
	CheckGlError("error prior FBO BindForWriting", DEBUG_INFO);
#ifdef WIN32
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, m_fbo);
#else
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
#endif
	//glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	CheckGlError("FBO BindForWriting", DEBUG_INFO);

//	glDrawBuffer(GL_NONE); // to disable color buffer. we can set here GL_COLOR_ATTACHMENT0 to draw in color buffer
//	CheckGlError("FBO BindForWriting glDrawBuffer(GL_NONE)", DEBUG_INFO);
//
//	glReadBuffer(GL_NONE);
//	CheckGlError("FBO BindForWriting glReadBuffer(GL_NONE)", DEBUG_INFO);
}

////////////////////////////////////////////////////////////////////////
void ShadowMapFBO::BindForReading(GLenum a_TextureUnit)
{
	glActiveTexture(a_TextureUnit);
	glBindTexture(m_DepthTarget, m_DepthTextureID);
	CheckGlError("FBO texture BindForReading", DEBUG_INFO);
}

////////////////////////////////////////////////////////////////////////
void ShadowMapFBO::BindDefaultFBO()
{
#ifdef WIN32
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0); // Restore rendering to back buffer
#else
    glBindFramebuffer(GL_FRAMEBUFFER, 0); // Restore rendering to back buffer
#endif
//		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0); // Restore rendering to back buffer
    CheckGlError("FBO BindDefaultFBO", DEBUG_INFO);
    glDrawBuffer(GL_BACK);
    glReadBuffer(GL_BACK);
}

////////////////////////////////////////////////////////////////////////
void* Engine::ShadowMapFBO::GetDepthPixels()
{

    glBindTexture (m_DepthTarget, m_DepthTextureID);
    CheckGlError("glBind depth texture to extract its pixels", DEBUG_INFO);
//	ClearGlError();
	switch (m_DepthType)
	{
		case GL_SIGNED_NORMALIZED:
		case GL_UNSIGNED_NORMALIZED:
		case GL_FLOAT:
		{
            GLfloat* pix = new GLfloat[m_Width * m_Height];
#ifdef WIN32
            glGetTextureImageEXT(m_DepthTextureID, m_DepthTarget,0, m_DepthFormat, m_DepthType, pix);
            CheckGlError("glGetTextureImageEXT of depth texture", DEBUG_INFO);
#else
            glGetTexImage (m_DepthTarget, 0, m_DepthFormat, m_DepthType, pix);
            CheckGlError("glGetTexImage of depth texture", DEBUG_INFO);
#endif
            return pix;
		}
		case GL_NONE:
			Logger::LogDebugSoft("Really ? no type of this texture ? ", DEBUG_SOFT_INFO);
        default:
            Logger::LogDebugSoft("TODO, case not handle", DEBUG_SOFT_INFO);
			return nullptr;
	}
}

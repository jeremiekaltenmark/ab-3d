/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 03-03-2014
 *	Refactor on: 10-07-2014
 *	
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */


#include "Engine/Engine.h"
#include "Rendering/Renderer.h"
//#include "Rendering/Light.h"
#include "Components/GraphicComponent.h"
//#include "Components/Material/Program.h"
//#include "Components/Material/Shader.h"
//#include "Components/Material/Material.h"
#include "Rendering/GraphicDevice.h"
#include "Rendering/ForwardRender.h"
#include "Rendering/ShadowMapRender.h"
#include "IO/AOutput.h"

using namespace Engine;
using Engine::Renderer;

////////////////////////////////////////////////////////////////////////
Renderer::Renderer() : m_GraphicDevice(GraphicDevice::GetSingleton()), m_ForwardRender(new ForwardRender), m_ShadowRender(new ShadowMapRender), m_OwnOutput(NULL), m_passType(PASS::FORWARDPASS)
{
	
}

////////////////////////////////////////////////////////////////////////
Renderer::Renderer(const Renderer& aCopy)
{
    Logger::LogDebug("implement me", DEBUG_INFO);
	(void) aCopy;
}

////////////////////////////////////////////////////////////////////////
Renderer::~Renderer()
{
	//Logger::LogDebugSoft("Renderer", DEBUG_SOFT_INFO);
	GraphicDevice::DeleteSingleton();
}

////////////////////////////////////////////////////////////////////////
void Renderer::Init(CommonSharedPtr<AOutput>& a_AOutput)
{
	CheckGlError("Renderer::Init", DEBUG_INFO);
	m_OwnOutput = a_AOutput;
	// Dark gray background
	glClearColor(0.4f, 0.4f, 0.4f, 0.0f);
    
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
    //glEnable(GL_CULL_FACE);

	CheckGlError("Renderer::Init", DEBUG_INFO);
	m_GraphicDevice->InitShaders();
	CheckGlError("m_GraphicDevice->InitShaders", DEBUG_INFO);

    SetPass(m_passType);
}

////////////////////////////////////////////////////////////////////////
void Renderer::Update(float aElapsedTime, ArrayGraphicComponents& aDrawables, CommonSharedPtr<Camera>& aCam, CommonSharedPtr<Viewport>& aViewport)
{
	CheckGlError("Renderer::Update", DEBUG_INFO);
    
	if (m_passType & PASS::SHADOWPASS)
    {
		if (m_passType & PASS::FORWARDPASS)
        {
            m_ShadowRender->Update(aElapsedTime, aDrawables, aCam->GetViewMatrix(), aViewport);
            CheckGlError("m_ShadowRender->Update", DEBUG_INFO);
			m_ShadowRender->PostUpdate(aElapsedTime, aDrawables, aCam->GetViewMatrix(), aViewport);
			CheckGlError("m_ShadowRender->PostUpdate", DEBUG_INFO);
        }
        else
		{
            m_ShadowRender->Update(aElapsedTime, aDrawables, aCam->GetViewMatrix(), aViewport, false);
            CheckGlError("m_ShadowRender->Update", DEBUG_INFO);
            m_ShadowRender->DebugUpdate(aElapsedTime, aDrawables, aCam->GetViewMatrix(), aViewport);
            CheckGlError("m_ShadowRender->PostUpdate", DEBUG_INFO);
		}
	}
	else if (m_passType & PASS::FORWARDPASS)
	{
		m_ForwardRender->Update(aElapsedTime, aDrawables, aCam->GetViewMatrix(), aViewport);
		CheckGlError("m_ForwardRender->Update", DEBUG_INFO);
	}

	CheckGlError("End Renderer::Update", DEBUG_INFO);


}

void Engine::Renderer::SetPass(int t_type)
{
	m_passType = (PASS) t_type;

	if ((m_passType & PASS::SHADOWPASS) && !m_ShadowRender->IsInit() && m_OwnOutput != NULL)
	{
		m_ShadowRender->Init(m_OwnOutput);
		CheckGlError("m_ShadowRender->Init", DEBUG_INFO);
	}

	if ((m_passType & PASS::FORWARDPASS) && !m_ForwardRender->IsInit() && m_OwnOutput != NULL)
	{
		m_ForwardRender->Init(m_OwnOutput);
		CheckGlError("m_ForwardRender->Init", DEBUG_INFO);
	}
}

RendererVisitor* Engine::Renderer::GetPass(const PASS& aPassType)
{

	if ((aPassType & PASS::SHADOWPASS) && !(aPassType & PASS::FORWARDPASS))
	{
		return m_ShadowRender;
	}

	if ((aPassType & PASS::FORWARDPASS) && !(aPassType & PASS::SHADOWPASS))
	{
		return m_ForwardRender;
	}

	return nullptr;
}

////////////////////////////////////////////////////////////////////////

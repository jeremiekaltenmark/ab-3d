/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project MoteurCDRIN - internal Game Engine 
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 26-01-2015
 *
 *	Copyright (c) 2015 CDRIN. All rights reserved.
 */

#ifndef Engine_Forward_Renderer_H
#define Engine_Forward_Renderer_H

//#include "IO/Camera.h"
#include "IO/Viewport.h"
#include "Rendering/Renderer.h"
#include "Rendering/RenderVisitor.h"


namespace Engine
{

	class Component;
	class GraphicComponent;
	class Material;
	class Viewport;
}

extern CommonSharedPtr<const Engine::Material> NullMaterial;

namespace Engine
{
	class ForwardRender : public RendererVisitor
	{
	public:
		ForwardRender();
		~ForwardRender();

		virtual void Update(float aElapsedTime, ArrayGraphicComponents& aDrawables, CommonSharedPtr<ViewMatrix>& aCam, CommonSharedPtr<Viewport>& aViewport);
		virtual void Render(CommonSharedPtr<Component>& a_Component);
		virtual void Render(CommonSharedPtr<GraphicComponent>& a_Component);

		void VisitComponents(CommonSharedPtr<GraphicComponent>& a_GraphComp);
		void VisitChildren(CommonSharedPtr<GraphicComponent>& a_GraphComp);

		inline void SetViewport(CommonSharedPtr<Viewport>& aViewport) { m_CurrentViewport = aViewport; }

	private:
		CommonSharedPtr<Material> m_PreviousMat;
		CommonSharedPtr<Viewport> m_CurrentViewport;

		ForwardRender& operator=(const ForwardRender&);
	};
}

#endif
/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Engine/Engine.h"
#include "Rendering/ForwardRender.h"

#include "Common/CommonList.h"
#include "Components/Component.h"
#include "Components/GraphicComponent.h"
#include "Components/Material/Material.h"

#include "IO/Camera.h"
#include "IO/Viewport.h"
#include "Components/ViewMatrix.h"

using namespace Engine;

ForwardRender::ForwardRender()
{

}

ForwardRender::~ForwardRender()
{

}

void ForwardRender::Render(CommonSharedPtr<Component>& a_Component)
{

	if (! a_Component->IsGraphic())
		return;
	
	CommonSharedPtr<GraphicComponent> t_GraphComp = CommonDynamicSharedCast<GraphicComponent>(a_Component);

	Render(t_GraphComp);
}

void ForwardRender::Render(CommonSharedPtr<GraphicComponent>& a_GraphComp)
{
	VisitComponents(a_GraphComp);

	VisitChildren(a_GraphComp);

	if (a_GraphComp->GetMaterial() != NULL)
	{
		a_GraphComp->GetMaterial()->Bind(m_PreviousMat);
        if(m_PreviousMat != a_GraphComp->GetMaterial())
        {
            m_PreviousMat = a_GraphComp->GetMaterial();
            a_GraphComp->GetMaterial()->GetShader()->FrameUpdate( m_CurrentViewport->GetProjectionMatrix(),
                                                                 *m_CurrentCamera.get());

        }
        a_GraphComp->GetMaterial()->GetShader()->Update(a_GraphComp,
                                                        m_CurrentViewport->GetProjectionMatrix(),
                                                        *m_CurrentCamera.get());
		a_GraphComp->Draw();
	}
	
}

void Engine::ForwardRender::VisitComponents(CommonSharedPtr<GraphicComponent>& a_GraphComp)
{
	CommonSharedPtr<GraphicComponent> t_GraphComp = NULL;
	ArrayComponents& t_Components = a_GraphComp->GetComponents();
	for (ArrayComponents::iterator it = t_Components.begin(); it != t_Components.end(); ++it)
	{
		if (it->second == nullptr || !it->second->IsGraphic())
		{
			continue;
		}
		t_GraphComp = CommonDynamicSharedCast<GraphicComponent>(it->second);
		t_GraphComp->Draw(this);
	}
}

void Engine::ForwardRender::VisitChildren(CommonSharedPtr<GraphicComponent>& a_GraphComp)
{
	CommonList<CommonSharedPtr<GraphicComponent> >& t_Children = a_GraphComp->GetChildren();
	for (CommonList<CommonSharedPtr<GraphicComponent> >::iterator it = t_Children.begin(); it != t_Children.end(); ++it)
	{
		(*it)->Draw(this);
	}
}

void Engine::ForwardRender::Update(float aElapsedTime, ArrayGraphicComponents& aDrawables, CommonSharedPtr<ViewMatrix>& aCam, CommonSharedPtr<Viewport>& aViewport)
{
	(void) aElapsedTime;
	m_CurrentCamera = aCam;
	m_CurrentViewport = aViewport;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	m_PreviousMat.reset();

    m_CurrentCamera->Update();
	for (ArrayGraphicComponents::iterator it = aDrawables.begin(); it != aDrawables.end(); ++it)
	{
		(*it)->Draw(this);
	}
}


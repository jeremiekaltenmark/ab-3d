/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
*	Project MoteurCDRIN - internal Game Engine
*	Organisation: www.jeremkalt.xyz
*
*	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
*	Created on: 26-01-2015
*
*	Copyright (c) 2015 CDRIN. All rights reserved.
*/

#ifndef Engine_Renderer_Visitor_H
#define Engine_Renderer_Visitor_H

#include "IO/AOutput.h"
#include "Rendering/Renderer.h"
#include "Rendering/FBO.h"
#include "Components/ViewMatrix.h"
#include <functional> // delegate

namespace Engine
{
	class Component;
	class GraphicComponent;
	class Material;
	class Viewport;
	class Camera;
}

namespace Engine
{
	class RendererVisitor
	{
	public:
		inline RendererVisitor() : m_OwnOutput(NULL), m_IsInit(false), m_Delegate(nullptr), m_CurrentCamera(NULL) {};
		virtual ~RendererVisitor() = 0;


		virtual void Update(float aElapsedTime,
                            ArrayGraphicComponents& aDrawables,
                            CommonSharedPtr<ViewMatrix>& aCamPos,
                            CommonSharedPtr<Viewport>& aViewport) = 0; // Launch Render

		inline virtual void DebugUpdate(float aElapsedTime,
										ArrayGraphicComponents& aDrawables,
										CommonSharedPtr<ViewMatrix>& aCamPos,
										CommonSharedPtr<Viewport>& aViewport) { (void)aElapsedTime; (void)aDrawables; (void)aCamPos; (void)aViewport; }

		virtual void Render(CommonSharedPtr<Component>& a_Component) = 0;				 // Visit
		virtual void Render(CommonSharedPtr<GraphicComponent>& a_Component) = 0;		 // Visit
		inline void Init(CommonSharedPtr<AOutput>& a_OwnOutput);

		inline bool IsInit() { return m_IsInit; }

		void RegisterFBOCallback(std::function<void(CommonSharedPtr<FBO>&)>& a_Delegate) { m_Delegate = a_Delegate; }


        inline void SetViewPoint(CommonSharedPtr<ViewMatrix>& aViewPosition) { m_CurrentCamera = aViewPosition; }


	protected:
		virtual void InternalInit();
		CommonSharedPtr<AOutput> m_OwnOutput;
		bool m_IsInit;
		std::function<void(CommonSharedPtr<FBO>&)> m_Delegate;
        CommonSharedPtr<ViewMatrix> m_CurrentCamera;
	};
	
	inline RendererVisitor::~RendererVisitor() {}

	inline void RendererVisitor::Init(CommonSharedPtr<AOutput>& a_OwnOutput) { m_OwnOutput = a_OwnOutput; InternalInit(); m_IsInit = true; }

	inline void RendererVisitor::InternalInit() {}



}

#endif
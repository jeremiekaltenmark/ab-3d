/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 09-07-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */


#ifndef Graphic_Device_H
#define Graphic_Device_H

#include "Common/Singleton.h"
#include "Engine/Engine.h"
#include "Common/CommonMap.h"

namespace Engine
{

	class Program;
	class Material;
	class Texture;

	class GraphicDevice : public Singleton<GraphicDevice>
	{
		friend class Engine::Singleton<GraphicDevice>;

	public:
		enum PROGRAMS : ui8
		{
			PROGRAM_SIMPLE_3D,
			PROGRAM_TEXTURE_3D,
			PROGRAM_LIT_TEXTURE_3D,
			PROGRAM_SHADOW_MAP,

			PROGRAM_COUNT
		};

		void InitShaders();

        CommonSharedPtr<Material>& GetMaterial(int a_index);
        CommonSharedPtr<Material>& GetMaterial(PROGRAMS a_ProgID, const CommonSharedPtr<Texture>& a_DiffuseTex);
        CommonSharedPtr<Material>& GetMaterial(PROGRAMS a_ProgID,
                                               const CommonSharedPtr<Texture>& a_DiffuseTex,
                                               const CommonSharedPtr<Texture>& a_ShadowTex);
        CommonSharedPtr<Material>& GetMaterial(PROGRAMS a_ProgID,
                                               const CommonSharedPtr<Texture>& a_DiffuseTex,
                                               const CommonSharedPtr<Texture>& a_ShadowTex,
                                               const CommonSharedPtr<Texture>& a_NormalTex);

		i32 GetMaterialIndex(const CommonSharedPtr<Material>& a_Mat) const;

		CommonSharedPtr<Program>& GetShaderProgram(PROGRAMS a_ProgID)
		{
			return m_ShadersProgram[a_ProgID];
		}
		CommonSharedPtr<Texture>& GetTexture(const CommonString& a_TextureName, bool a_IsModelTexture = false);
        CommonSharedPtr<Texture>& GetDefaultTexture();
        CommonSharedPtr<Texture>& GetDefaultNormalMap();


	private:
		typedef CommonList< CommonSharedPtr<Material> > MatContainer;
		typedef CommonMap<CommonString, CommonSharedPtr<Texture> > TexContainer;


		CommonSharedPtr<Program> m_ShadersProgram[PROGRAM_COUNT];
		MatContainer m_Materials;
        TexContainer m_Textures;
        CommonSharedPtr<Texture> m_DefaultTexture;
        CommonSharedPtr<Texture> m_DefaultNormalMap;

	private:
		GraphicDevice();
		//GraphicDevice(const GraphicDevice& aCopy); // disable copy of application
		virtual ~GraphicDevice();

		CommonSharedPtr<Texture>& GetModelTexture(const CommonString& a_ModelTextureName);

	};
}
#endif // GRaphic_device_H
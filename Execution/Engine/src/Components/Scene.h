/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 10-03-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#ifndef Engine_Scene_H
#define Engine_Scene_H

#include "Components/Component.h"
#include "Components/GraphicComponent.h"
#include "Rendering/Light.h"

namespace Engine
{
    class Application;

class Scene : public Component
{
    friend class Application;

public:
	Scene();
	Scene(const Scene& aCopy);

    CommonSharedPtr<Light>& GetLight() { return mLight;}
	void AddRepereToScene();


    // this is the method to implement to get a
    virtual void Update(float aElapsedTime) = 0;
	virtual ~Scene();

    ArrayGraphicComponents& GetSceneGraph() { return m3DObjects; }

protected:
	void SceneUpdate(float aElapsedTime);

protected:
	CommonSharedPtr<Engine::GraphicComponent> mRepereX;
	CommonSharedPtr<Engine::GraphicComponent> mRepereY;
	CommonSharedPtr<Engine::GraphicComponent> mRepereZ;

	ArrayGraphicComponents m3DObjects;
	CommonSharedPtr<Light> mLight;


};

}
#endif // Engine_Scene_H

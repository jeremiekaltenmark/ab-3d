/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 21-02-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */


#ifndef Engine_Component_H
#define Engine_Component_H

#include "Common/CommonList.h"
#include "Engine/Engine.h"

namespace Engine
{

	class Component
	{
	public:
		Component();
		Component(const Component& aCopy);

		virtual void Update(float aElapsedTime);
		virtual ~Component();

		inline ArrayComponents& GetComponents() { return mComponents; }
		inline bool IsGraphic() { return m_IsGraphic; }

        template<class T>
        CommonSharedPtr<T> GetComponent() { return CommonDynamicSharedCast<T> ( mComponents[&typeid(T)]); }

	protected:
		ArrayComponents mComponents;
		bool m_IsGraphic;
	};

} // namespace Engine


#endif // Engine_Component_H

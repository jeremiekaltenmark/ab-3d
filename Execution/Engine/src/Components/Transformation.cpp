/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 03-04-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#include "Engine/Engine.h"
#include "Transformation.h"

using namespace Engine;


////////////////////////////////////////////////////////////////////////
Transformation::Transformation()
:
mPos(0.f,0.f,0.f),
mScale(1.f,1.f,1.f),
mRot(),
mTransformMat(),
mDirty(false)
{
//    m_type = TRANSFORMATION;
}

////////////////////////////////////////////////////////////////////////
Transformation::Transformation(const Transformation& aCopy)
:
mPos(aCopy.mPos),
mScale(aCopy.mScale),
mRot(aCopy.mRot),
mTransformMat(aCopy.mTransformMat),
mDirty(aCopy.mDirty)
{
//    m_type = aCopy.m_type;
}

////////////////////////////////////////////////////////////////////////
Transformation::~Transformation()
{
	//Logger::LogDebugSoft("~Transformation", DEBUG_SOFT_INFO);
}

const Matrix& Transformation::GetConstMatrixTransform ()
{
	if(mDirty)
	{
		Matrix tRotMat;
		mRot.GetTransfMat(tRotMat.mLineMat);

		mTransformMat = Matrix::NewTranslationMatrix(mPos) * tRotMat * Matrix::NewScaleMatrix(mScale);
		mDirty = false;
	}

	return mTransformMat;
}

Matrix Transformation::GetCopyMatrixTransform ()
{
	return GetConstMatrixTransform(); // will returns a copy
}
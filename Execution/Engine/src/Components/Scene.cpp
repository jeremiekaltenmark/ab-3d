/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 10-03-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */



#include "Engine/Engine.h"
#include "Scene.h"
#include "Common/Logger.h"
#include "Components/GraphicPrimitives/Cube.h"
#include "Components/GraphicComponent.h"

using namespace Engine;


////////////////////////////////////////////////////////////////////////
Scene::Scene()
{
//    m_type = SCENE;
    mLight = CommonDynamicSharedCast<Light>(GraphicDevice::GetSingleton()->GetShaderProgram(Engine::GraphicDevice::PROGRAMS::PROGRAM_LIT_TEXTURE_3D));
    mComponents[&typeid(Light)] = mLight;
}

////////////////////////////////////////////////////////////////////////
Scene::Scene(const Scene& aCopy)
{
    Logger::LogDebug("implement me", DEBUG_INFO);
	(void) aCopy;
}

////////////////////////////////////////////////////////////////////////
Scene::~Scene()
{
    //Engine::Logger::LogDebugSoft("destroy Scene", DEBUG_SOFT_INFO);
    //m3DObjects.deletePtrAndClear(); // no more need to delete them as they are sharedPtr now
}

////////////////////////////////////////////////////////////////////////
void Scene::SceneUpdate(float aElapsedTime)
{
	(void) aElapsedTime;
    //mLight->Update(aElapsedTime);
    Update(aElapsedTime);
}

////////////////////////////////////////////////////////////////////////
void Scene::Update(float aElapsedTime)
{
	(void) aElapsedTime;
}

void Engine::Scene::AddRepereToScene()
{
	CommonSharedPtr<Engine::GraphicComponent> tGraph;
	GraphicDevice* t_Device = GraphicDevice::GetSingleton();
    CommonSharedPtr<Texture> t_SimpleTexture = t_Device->GetTexture("textures/damier.png");
	CommonSharedPtr<Material> t_SimpleMat = t_Device->GetMaterial(GraphicDevice::PROGRAM_LIT_TEXTURE_3D, t_SimpleTexture);

	Vec3 tForward(0.0f, 0.0f, 1.0f);
	Vec3 tUp(0.0f, 1.0f, 0.0f);

	Vec3 tRight = tUp.Cross(tForward);  tRight.Normalize();
	tUp = tForward.Cross(tRight);       tUp.Normalize();
	tForward = tRight.Cross(tUp);       tForward.Normalize();

	Vec3 tScale(0.01f, 0.01f, 0.01f);
	tRight += tScale;
	tForward += tScale;
	tUp += tScale;


	// x
	tGraph = CommonSharedPtr<Engine::GraphicComponent>(new Engine::Cube());
	tGraph->GetTransform()->Move(tRight);
	tGraph->GetTransform()->SetScale(tRight);
	tGraph->SetMaterial(t_SimpleMat);
	tGraph->SetColor(-1.0f);
	//tGraph->SetParent(mBase.get());
	//tGraph->GetMaterial()->SetShader(Renderer::PROGRAMS::PROGRAM_SIMPLE_3D );
	Scene::m3DObjects.AddFirst(tGraph);
	mRepereX = tGraph;

	// y
	tGraph = CommonSharedPtr<Engine::GraphicComponent>(new Engine::Cube());
	tGraph->GetTransform()->Move(tUp);
	tGraph->GetTransform()->SetScale(tUp);
	tGraph->SetMaterial(t_SimpleMat);
	tGraph->SetColor(1.0f);
	Scene::m3DObjects.AddFirst(tGraph);
	//tGraph->SetParent(mBase.get());
	//tGraph->GetMaterial()->SetShader(Renderer::PROGRAMS::PROGRAM_SIMPLE_3D );
	mRepereY = tGraph;



	// z
	tGraph = CommonSharedPtr<Engine::GraphicComponent>(new Engine::Cube());
	tGraph->GetTransform()->Move(tForward);
	tGraph->GetTransform()->SetScale(tForward);
	tGraph->SetMaterial(t_SimpleMat);
	tGraph->SetColor(0.5f);
	Scene::m3DObjects.AddFirst(tGraph);
	//tGraph->SetParent(mBase.get());
	//tGraph->GetMaterial()->SetShader(Renderer::PROGRAMS::PROGRAM_SIMPLE_3D );
	mRepereZ = tGraph;

}


/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 08-04-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */


#include "Engine/Engine.h"
#include "Components/GraphicPrimitives/Plan.h"

using namespace Engine;

////////////////////////////////////////////////////////////////////////
Plan::Plan(void)
{
//    m_type = PLAN;
	unsigned int Indices[] = {
		0, 1, 2,    0, 2, 3
	};

	m_NbIBOIndices = GetLength(Indices);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW);

	static Vertex g_vertex_buffer_data[] = {
		Vertex(Vec3(-1.0f, 0.0f, -1.0f), Vec2(0.0f, 0.0f), Vec3(0.0f, 1.0f, 0.0f)), // 0
		Vertex(Vec3(+1.0f, 0.0f, -1.0f), Vec2(1.0f, 0.0f), Vec3(0.0f, 1.0f, 0.0f)), // 1
		Vertex(Vec3(+1.0f, 0.0f, +1.0f), Vec2(1.0f, 1.0f), Vec3(0.0f, 1.0f, 0.0f)), // 3
		Vertex(Vec3(-1.0f, 0.0f, +1.0f), Vec2(0.0f, 1.0f), Vec3(0.0f, 1.0f, 0.0f)), // 3
	};

    ComputeTangent(g_vertex_buffer_data, Indices);
    
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);
}

////////////////////////////////////////////////////////////////////////
Plan::~Plan(void)
{
    //Logger::LogDebugSoft("Delete plan", DEBUG_SOFT_INFO);
}

//////////////////////////////////////////////////////////////////////////
//void Plan::Draw()
//{
//    glDisable(GL_CULL_FACE);
//
//	// 1rst attribute buffer : vertices
//	glEnableVertexAttribArray(0);
//	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
////glVertexAttribPointer (Shaderlayout, size, type, normalized?, stride, arrayOffset (void*));
//	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, 0);
//
//	// Draw the triangle !
//	glDrawArrays(GL_TRIANGLES, 0, 6);
//
//	glDisableVertexAttribArray(0);
//
//    glEnable(GL_CULL_FACE);
//}

/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 23-05-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */


#include "Engine/Engine.h"
#include "Components/GraphicPrimitives/Cube.h"
#include "Rendering/Renderer.h"
//#include "Components/Material/TextureMgr.h"

using namespace Engine;

/*

	float xyzData[] = {
	// top
	+0.5, 0.5, -0.5,	 0.f, 1.f, 0.f,		1.f, 0.f,
	-0.5, 0.5, +0.5,	 0.f, 1.f, 0.f,		0.f, 1.f,
	+0.5, 0.5, +0.5,	 0.f, 1.f, 0.f,		1.f, 1.f,
	+0.5, 0.5, -0.5,	 0.f, 1.f, 0.f,		1.f, 0.f,
	-0.5, 0.5, -0.5,	 0.f, 1.f, 0.f,		0.f, 0.f,
	-0.5, 0.5, +0.5,	 0.f, 1.f, 0.f,		0.f, 1.f,
	// bottom
	-0.5, -0.5, +0.5,	 0.f, -1.f, 0.f,	 0.f, 0.f,
	+0.5, -0.5, -0.5,	 0.f, -1.f, 0.f,	 1., 1.,
	+0.5, -0.5, +0.5,	 0.f, -1.f, 0.f,	 1.f, 0.,
	-0.5, -0.5, -0.5,	 0.f, -1.f, 0.f,	 0.f, 1.f,
	+0.5, -0.5, -0.5,	 0.f, -1.f, 0.f,	 1.f, 1.f,
	-0.5, -0.5, +0.5,	 0.f, -1.f, 0.f,	 0.f, 0.f,
	// front
	+0.5, -0.5, -0.5,	 0.f, 0.f, -1.f,	 0.f, 1.f,
	-0.5, +0.5, -0.5,	 0.f, 0.f, -1.f,	 1.f, 0.f,
	+0.5, +0.5, -0.5,	 0.f, 0.f, -1.f,	 0.f, 0.f,
	-0.5, +0.5, -0.5,	 0.f, 0.f, -1.f,	 1.f, 0.f,
	+0.5, -0.5, -0.5,	 0.f, 0.f, -1.f,	 0.f, 1.f,
	-0.5, -0.5, -0.5,	 0.f, 0.f, -1.f,	 1.f, 1.f,
	// back
	-0.5, +0.5, +0.5,	 0.f, 0.f, 1.f,		 0.f, 0.f,
	-0.5, -0.5, +0.5,	 0.f, 0.f, 1.f,		 0.f, 1.f,
	+0.5, +0.5, +0.5,	 0.f, 0.f, 1.f,		 1.f, 0.f,
	-0.5, -0.5, +0.5,	 0.f, 0.f, 1.f,		 0.f, 1.f,
	+0.5, -0.5, +0.5,	 0.f, 0.f, 1.f,		 1.f, 1.f,
	+0.5, +0.5, +0.5,	 0.f, 0.f, 1.f,		 1.f, 0.f,
	// left
	-0.5, -0.5, +0.5,	-1.f, 0.f, 0.f,		 1.f, 1.f,
	-0.5, +0.5, -0.5,	-1.f, 0.f, 0.f,		 0.f, 0.f,
	-0.5, -0.5, -0.5,	-1.f, 0.f, 0.f,		 0.f, 1.f,
	-0.5, +0.5, -0.5,	-1.f, 0.f, 0.f,		 0.f, 0.f,
	-0.5, -0.5, +0.5,	-1.f, 0.f, 0.f,		 1.f, 1.f,
	-0.5, +0.5, +0.5,	-1.f, 0.f, 0.f,		 1.f, 0.f,
	// right
	+0.5, -0.5, -0.5,	1.f, 0.f, 0.f,		1.f, 1.f,
	+0.5, +0.5, -0.5,	1.f, 0.f, 0.f,		1.f, 0.f,
	+0.5, +0.5, +0.5,	1.f, 0.f, 0.f,		0.f, 0.f,
	+0.5, +0.5, +0.5,	1.f, 0.f, 0.f,		0.f, 0.f,
	+0.5, -0.5, +0.5,	1.f, 0.f, 0.f,		0.f, 1.f,
	+0.5, -0.5, -0.5,	1.f, 0.f, 0.f,		1.f, 1.f
	};

	unsigned short i[] = {
	0, 1, 2, 3, 4, 5,
	6, 7, 8, 9, 10, 11,
	12, 13, 14, 15, 16, 17,
	18, 19, 20, 21, 22, 23,
	24, 25, 26, 27, 28, 29,
	30, 31, 32, 33, 34, 35
	};
	*/
////////////////////////////////////////////////////////////////////////
Cube::Cube()
{
//    m_type = CUBE;
	// 6 faces * 2 tri by face = 12 tri * 3 vertice = 36 points
	unsigned int Indices[] = {
        //*
        0 ,1 ,2,   0 ,2 ,3   // front
		,0,3 ,4,  4,3 ,7   // left
		,7 ,3 ,2,   7 ,2 ,6   // top
		,6 ,2 ,1,   6 ,1 ,5   // right
		,5 ,1 ,0,   5 ,0 ,4   // bottom
		,4,7 ,6,   4,6 ,5  // back
        /*/
        8 ,9 ,2,   8 ,2 ,3   // front
		,12,3 ,13,  13,3 ,7   // left
		,7 ,3 ,2,   7 ,2 ,6   // top
		,6 ,2 ,1,   6 ,1 ,5   // right
		,5 ,1 ,0,   5 ,0 ,4   // bottom
		,10,7 ,6,   10,6 ,11  // back
         //*/
	};

    m_NbIBOIndices = GetLength(Indices);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW);

	static Vertex g_vertex_buffer_data[] = {
		Vertex(Vec3(-1.0f, -1.0f, -1.0f), Vec2(0.00f, 0.50f)), // 0
		Vertex(Vec3(+1.0f, -1.0f, -1.0f), Vec2(0.25f, 0.50f)), // 1
		Vertex(Vec3(+1.0f, +1.0f, -1.0f), Vec2(0.50f, 0.50f)), // 2
		Vertex(Vec3(-1.0f, +1.0f, -1.0f), Vec2(0.75f, 0.50f)), // 3
		Vertex(Vec3(-1.0f, -1.0f, +1.0f), Vec2(0.00f, 0.75f)), // 4
		Vertex(Vec3(+1.0f, -1.0f, +1.0f), Vec2(0.25f, 0.75f)), // 5
		Vertex(Vec3(+1.0f, +1.0f, +1.0f), Vec2(0.50f, 0.75f)), // 6
		Vertex(Vec3(-1.0f, +1.0f, +1.0f), Vec2(0.75f, 0.75f)), // 7

		Vertex(Vec3(-1.0f, -1.0f, -1.0f), Vec2(0.75f, 0.25f)), // 0 as 8
		Vertex(Vec3(+1.0f, -1.0f, -1.0f), Vec2(0.50f, 0.25f)), // 1 as 9

		Vertex(Vec3(-1.0f, -1.0f, +1.0f), Vec2(0.75f, 1.00f)), // 4 as 10
		Vertex(Vec3(+1.0f, -1.0f, +1.0f), Vec2(0.50f, 1.00f)), // 5 as 11

		Vertex(Vec3(-1.0f, -1.0f, -1.0f), Vec2(1.00f, 0.50f)), // 0 as 12
		Vertex(Vec3(-1.0f, -1.0f, +1.0f), Vec2(1.00f, 0.75f))  // 4 as 13
    };
    ComputeNormals(g_vertex_buffer_data, Indices);
    ComputeTangent(g_vertex_buffer_data, Indices);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);
}

////////////////////////////////////////////////////////////////////////
Cube::Cube(const Cube& /*aCopy*/)
{
//    m_type = aCopy.m_type;
}

////////////////////////////////////////////////////////////////////////
Cube::~Cube()
{
	//Logger::LogDebugSoft("Destroy cube", DEBUG_SOFT_INFO);
}


/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - Internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 11-07-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */
#ifndef Mesh_H
#define Mesh_H

#include "Engine/Engine.h"
#include "IO/FileSystem.h"
#include "Common/CommonString.h"
#include "Common/CommonVector.h"
#include "Components/Material/Texture.h"
#include "Components/GraphicComponent.h"

#include "assimp/Importer.hpp"
#include "assimp/postprocess.h"
#include "assimp/scene.h"

namespace Engine
{
	class Mesh : public GraphicComponent
	{
	public:
		Mesh();
		Mesh(const char* a_FileName, bool a_SetDefaultTexture = false);
		Mesh(const CommonString& a_FileName, bool a_SetDefaultTexture = false);
		~Mesh();

		bool LoadMesh(const char* a_FileName);
		bool LoadMesh(const CommonString& a_FileName);

		//void Draw();

	private:
		bool InitFromScene(const aiScene* a_Scene);
		bool InitMesh(const aiMesh* a_Mesh, const aiScene* a_pScene); // return true if no error occurs
//		bool InitMaterials(const aiScene* pScene, const CommonString& Filename);
		void Clear();

		bool m_SetDefaultTexture = false;

	};
}

#endif
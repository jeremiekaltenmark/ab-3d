/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 23-05-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */


#include "Engine/Engine.h"
#include "Components/GraphicPrimitives/TriPyramid.h"

using namespace Engine;


////////////////////////////////////////////////////////////////////////
TriPyramid::TriPyramid()
{

    unsigned int Indices[] = {
        3, 0, 1,   // left
        3, 1, 2,   // right
        2, 0, 3,   // back
        1, 0, 2 }; // bottom


    m_NbIBOIndices = GetLength(Indices);
    //	glGenBuffers(1, &mIndexedBufferObject);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW);

	static const GLfloat g_vertex_buffer_data[] = {
		-1.0f, -1.0f, 0.0f, // 0 left
		0.0f, -1.0f, 1.0f,  // 1 front
		1.0f, -1.0f, 0.0f,  // 2 right
		0.0f,  1.0f, 0.0f,  // 3 top
	};

//    ComputeTangent<sizeof(g_vertex_buffer_data), sizeof(Indices)>(g_vertex_buffer_data, Indices);


	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

}

////////////////////////////////////////////////////////////////////////
TriPyramid::TriPyramid(const TriPyramid& /*aCopy*/)
{
//    m_type = aCopy.m_type;
}

////////////////////////////////////////////////////////////////////////
TriPyramid::~TriPyramid()
{
	//Logger::LogDebugSoft("Destroy TriPyramid", DEBUG_SOFT_INFO);
}

////////////////////////////////////////////////////////////////////////
//void TriPyramid::Draw()
//{
//    glDisable(GL_CULL_FACE);
//
//    // 1rst attribute buffer : vertices
//	glEnableVertexAttribArray(0);
//	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
//    //glVertexAttribPointer (Shaderlayout, size, type, normalized?, stride, arrayOffset (void*));
//	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, 0);
//
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
//	// Draw the triangle !
//	glDrawElements(	GL_TRIANGLES, m_NbIBOIndices, GL_UNSIGNED_INT, 0); // 3 indices starting at 0 -> 1 triangle
//    
//	glDisableVertexAttribArray(0);
//
//    glEnable(GL_CULL_FACE);
//}

/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - Internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 11-07-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */
 
#include "Engine/Engine.h"

using namespace Engine;

#include "mesh.h"

//Mesh::MeshEntry::MeshEntry()
//{
//	VBO = INVALID_OGL_VALUE;
//	IBO = INVALID_OGL_VALUE;
//	NumIndices = 0;
//	MaterialIndex = INVALID_MATERIAL;
//};
//
//Mesh::MeshEntry::~MeshEntry()
//{
//	if (VBO != INVALID_OGL_VALUE)
//	{
//		glDeleteBuffers(1, &VBO);
//	}
//
//	if (IBO != INVALID_OGL_VALUE)
//	{
//		glDeleteBuffers(1, &IBO);
//	}
//}
//
//void Mesh::MeshEntry::Init(const CommonVector<Vertex>& Vertices,
//	const CommonVector<ui32>& Indices)
//{
//	NumIndices = Indices.GetLength();
//
//	glGenBuffers(1, &VBO);
//	glBindBuffer(GL_ARRAY_BUFFER, VBO);
//	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * Vertices.GetLength(), &Vertices[0], GL_STATIC_DRAW);
//
//	glGenBuffers(1, &IBO);
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
//	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(ui32) * NumIndices, &Indices[0], GL_STATIC_DRAW);
//}

Mesh::Mesh()
{
//    m_type = MESH;
 //   glDeleteBuffers(1, &m_VBO);
	//glDeleteBuffers(1, &m_IBO);
	//glDeleteVertexArrays(1, &m_VAO);
 //   m_VBO = INVALID_OGL_VALUE;
 //   m_IBO = INVALID_OGL_VALUE;
 //   m_VAO = INVALID_OGL_VALUE;

}
Mesh::Mesh(const char* a_Filename, bool a_SetDefaultTexture/* = false*/)
{
//    m_type = MESH;
	m_SetDefaultTexture = a_SetDefaultTexture;
	glDeleteBuffers(1, &m_VBO);
	glDeleteBuffers(1, &m_IBO);
	glDeleteVertexArrays(1, &m_VAO);
	m_VBO = INVALID_OGL_VALUE;
	m_IBO = INVALID_OGL_VALUE;
	m_VAO = INVALID_OGL_VALUE;

    if(! LoadMesh(a_Filename))
    {
        Logger::LogDebug("Error while creating the mesh", DEBUG_INFO);
    }
}
Mesh::Mesh(const CommonString& a_Filename, bool a_SetDefaultTexture/* = false*/)
{
//    m_type = MESH;
	m_SetDefaultTexture = a_SetDefaultTexture;
	if (!LoadMesh(a_Filename))
    {
        Logger::LogDebug("Error while creating the mesh", DEBUG_INFO);
    }
}


Mesh::~Mesh()
{
	Clear();
}


void Mesh::Clear()
{
	CommonList<CommonSharedPtr<GraphicComponent> >& t_Children = GetChildren();
	for (CommonList<CommonSharedPtr<GraphicComponent> >::iterator it = t_Children.begin(); it != t_Children.end(); ++it)
	{
		CommonSharedPtr<Mesh> tSubMesh = CommonDynamicSharedCast<Mesh>(*it);
		if (tSubMesh)
			tSubMesh->Clear();
	}

	ArrayComponents& t_comps = GetComponents();
	for (ArrayComponents::iterator it = t_comps.begin(); it != t_comps.end(); ++it)
	{
		CommonSharedPtr<Mesh> tSubMesh = CommonDynamicSharedCast<Mesh>(it->second);
		if (tSubMesh)
			tSubMesh->Clear();
	}

	m_Material.reset();
}


bool Mesh::LoadMesh(const char* a_Filename)
{
	return LoadMesh(CommonString(a_Filename));
}

bool Mesh::LoadMesh(const CommonString& a_Filename)
{
	// Release the previously loaded mesh (if it exists)
	Clear();

	bool t_Ret = false;
	Assimp::Importer t_Importer;

	const aiScene* t_pScene = t_Importer.ReadFile(a_Filename.GetChars(),
                                                  aiProcess_Triangulate |
                                                  aiProcess_GenSmoothNormals |
//                                                  aiProcess_FlipUVs |
                                                  aiProcess_CalcTangentSpace
                                                  );

	if (t_pScene)
	{
		t_Ret = InitFromScene(t_pScene);
	}
	else
	{
		Logger::LogDebugSoft("Error", DEBUG_SOFT_INFO);
		Logger::Printf("Error parsing '%s': '%s'\n", a_Filename.GetChars(), t_Importer.GetErrorString());
		//printf("Error parsing '%s': '%s'\n", a_Filename.GetChars(), Importer.GetErrorString());
	}

	return t_Ret;
}

bool Engine::Mesh::InitFromScene(const aiScene* a_Scene)
{
    mComponents.clear();

    bool t_ret = true;
	// Initialize the meshes in the scene one by one
	for (unsigned int i = 0; i < a_Scene->mNumMeshes; i++)
	{
		const aiMesh* t_Mesh = a_Scene->mMeshes[i];
		t_ret = t_ret && InitMesh(t_Mesh, a_Scene);
	}

	return t_ret;//InitMaterials(a_Scene, a_Filename);
}

bool Engine::Mesh::InitMesh(const aiMesh* a_Mesh, const aiScene* a_pScene)
{
	CommonVector<Vertex> Vertices;
	CommonVector<unsigned int> Indices;

	const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);

	for (unsigned int i = 0; i < a_Mesh->mNumVertices; i++)
	{
		const aiVector3D* t_Pos = &(a_Mesh->mVertices[i]);
        const aiVector3D* t_Normal = &(a_Mesh->mNormals[i]);
        const aiVector3D* t_TexCoord = a_Mesh->HasTextureCoords(0) ? &(a_Mesh->mTextureCoords[0][i]) : &Zero3D;
        const aiVector3D* t_Tangent = &(a_Mesh->mTangents[i]);

        Vertex v(Vec3(t_Pos->x, t_Pos->y, t_Pos->z),
//             Vec2(t_TexCoord->x, 1.0f - t_TexCoord->y),
             Vec2(t_TexCoord->x, t_TexCoord->y),
			Vec3(t_Normal->x, t_Normal->y, t_Normal->z),
            Vec3(t_Tangent->x,t_Tangent->y,t_Tangent->z));

		Vertices.AddLast(v);
	}

	for (unsigned int i = 0; i < a_Mesh->mNumFaces; i++)
	{
		const aiFace& Face = a_Mesh->mFaces[i];
		assert(Face.mNumIndices == 3);
		Indices.AddLast(Face.mIndices[0]);
		Indices.AddLast(Face.mIndices[1]);
		Indices.AddLast(Face.mIndices[2]);
	}

    CommonSharedPtr<Mesh> t_SubMesh(new Mesh());
	t_SubMesh->Init(Vertices, Indices);
//    t_SubMesh->ComputeTangent(Vertices, Indices);

    GraphicDevice* t_Device = GraphicDevice::GetSingleton();
    CommonSharedPtr<Texture> t_Texture = NULL;
    CommonSharedPtr<Texture> t_NormalMap = NULL;

    const aiMaterial* t_pMaterial = a_pScene->mMaterials[a_Mesh->mMaterialIndex];
    aiString t_Path;

    if (t_pMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &t_Path, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS)
    {
        CommonString t_TextureName = t_Path.data;
		t_Texture = t_Device->GetTexture(t_TextureName, true);
		if (t_Texture != NULL && t_Texture->IsDefault())
		{
			t_Texture.reset();
			t_Texture = NULL;
		}
    }

    if (t_pMaterial->GetTexture(aiTextureType_NORMALS, 0, &t_Path, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS)
    {
        CommonString t_TextureName = t_Path.data;
        t_NormalMap = t_Device->GetTexture(t_TextureName, true);
        if (t_NormalMap != NULL && t_NormalMap->IsDefault())
        {
            t_NormalMap.reset();
            t_NormalMap = t_Device->GetDefaultNormalMap();
        }
    }


    if (t_Texture == NULL)
	{
		if (m_SetDefaultTexture)
			t_SubMesh->m_Material = t_Device->GetMaterial(GraphicDevice::PROGRAM_LIT_TEXTURE_3D, t_Device->GetDefaultTexture());
		else
			t_SubMesh->m_Material = t_Device->GetMaterial(GraphicDevice::PROGRAM_LIT_TEXTURE_3D, t_Texture);
	}
	else
	{
        t_SubMesh->m_Material = t_Device->GetMaterial(GraphicDevice::PROGRAM_LIT_TEXTURE_3D, t_Texture, NULL, t_NormalMap);
	}

	t_SubMesh->GetTransform()->SetScale(Vec3(0.05f, 0.05f, 0.05f));
	t_SubMesh->SetParent(this);

	return true;
}

 
/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 03-04-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */


#ifndef Engine_GraphicComponent_H
#define Engine_GraphicComponent_H

//#include <iostream>
#include "Components/NodeComponent.h"
#include "Components/Transformation.h"
#include "Components/Material/Material.h"
#include "Maths/Vec2.h"
#include "Components/Material/Program.h"


namespace Engine
{
    class Camera;
    class Viewport;
    struct Vertex;
	class RendererVisitor;


    class GraphicComponent : public NodeComponent, public CommonEnableSharedFromThis<GraphicComponent>
    {
    public:
	friend class ShadowMapRender;
        GraphicComponent();
//        GraphicComponent(const GraphicComponent& aCopy);

        virtual ~GraphicComponent();

		virtual void Draw(CommonSharedPtr<Material> a_Mat = CommonSharedPtr<Material>(NULL));
		//virtual void Draw(CommonSharedPtr<Material>& a_PreviousMat, CommonSharedPtr<Camera>& aCam, CommonSharedPtr<Viewport>& aViewport);
		virtual void Draw(RendererVisitor* a_Visitor);
        void Init(CommonVector<Vertex>& Vertices, const CommonVector<ui32>& Indices);

		inline Matrix GetModelMatrix() { return mTransform->GetConstMatrixTransform(); }
		inline const Matrix& GetModelMatrix() const { return mTransform->GetConstMatrixTransform(); }

		Matrix GetGlobalModelMatrix() const ;

		inline const CommonSharedPtr<Transformation>& GetTransform() const { return mTransform; }
		inline CommonSharedPtr<Transformation>& GetTransform() { return mTransform; }
		inline CommonSharedPtr<Material>& GetMaterial() { return m_Material; }


        inline void SetTransform(const CommonSharedPtr<Transformation>& aTransf) { mTransform = aTransf; }
		
		inline void SetMaterial(CommonSharedPtr<Material>& a_Material) {  m_Material = a_Material; mComponents[&typeid(Material)] = m_Material;}
        inline GLuint GetProgramShader() {return m_Material->GetShader()->GetHandle(); }

		inline void SetColor(float a) { mCol = a; }
		inline float GetColor() { return mCol ; }

        inline GraphicComponent* GetParent() {return m_Parent;}
        inline void SetParent(GraphicComponent* a_Parent) 
		{
			m_Parent = a_Parent;
			m_Parent->m_Children.AddLast(shared_from_this());
		}

		inline CommonList<CommonSharedPtr<GraphicComponent> >& GetChildren() { return m_Children; }

    protected:
        template<size_t VCOUNT, size_t ICOUNT>
        void ComputeNormals(Vertex (&Vertices)[VCOUNT], const uint (&Indices)[ICOUNT] );
        
        template<size_t VCOUNT, size_t ICOUNT>
        void ComputeTangent(Vertex (&Vertices)[VCOUNT], const uint (&Indices)[ICOUNT] );

        inline void ComputeTangent(CommonVector<Vertex>& Vertices, const CommonVector<unsigned int>& Indices);

    protected:
        GLuint m_VBO;		// vertex buffer object
        GLuint m_VAO;		// vertex array object
        GLuint m_IBO;		// index buffer object

        int m_NbIBOIndices;
        float  mCol;
        
		CommonSharedPtr<Transformation> mTransform;
		CommonSharedPtr<Material> m_Material;
        GraphicComponent* m_Parent;
		CommonList<CommonSharedPtr<GraphicComponent> > m_Children;
    };

	struct Vertex
	{
		Vec3 m_Pos;			// must be first of members in struct to automatically use it when call draw
		Vec2 m_TexCoord;	// must be second of members in struct to automatically use it when call draw
        Vec3 m_Normal;      // must be third of members in struct to automatically use it when call draw
        Vec3 m_Tangent;     // must be fourth of members in struct to automatically use it when call draw

		Vertex();
		Vertex(const Vec3& a_Pos, const Vec2& a_TexCoord) :
			m_Pos(a_Pos), m_TexCoord(a_TexCoord), m_Normal(0.0f, 0.0f, 0.0f), m_Tangent(0.0f, 0.0f, 0.0f)
		{
        };
        Vertex(const Vec3& a_Pos, const Vec2& a_TexCoord, const Vec3& a_Normal) :
        m_Pos(a_Pos), m_TexCoord(a_TexCoord), m_Normal(a_Normal), m_Tangent(0.0f, 0.0f, 0.0f)
        {
        };
        Vertex(const Vec3& a_Pos, const Vec2& a_TexCoord, const Vec3& a_Normal, const Vec3& a_Tangent) :
        m_Pos(a_Pos), m_TexCoord(a_TexCoord), m_Normal(a_Normal), m_Tangent(a_Tangent)
        {
        };
	};

    ////////////////////////////////////////////////////////////////////////
    template<size_t VCOUNT, size_t ICOUNT>
    void GraphicComponent::ComputeTangent(Vertex (&Vertices)[VCOUNT], const uint (&Indices)[ICOUNT])
    {
        for (int i = 0; i < ICOUNT; i += 3)
        {
            uint Index0 = Indices[i];
            uint Index1 = Indices[i+1];
            uint Index2 = Indices[i+2];

            Vertex& v0 = Vertices[Index0];
            Vertex& v1 = Vertices[Index1];
            Vertex& v2 = Vertices[Index2];

            Vec3 Edge1 = v1.m_Pos - v0.m_Pos;
            Vec3 Edge2 = v2.m_Pos - v0.m_Pos;


            // compute tangent in same loop
            float DeltaU1 = v1.m_TexCoord.x - v0.m_TexCoord.x;
            float DeltaV1 = v1.m_TexCoord.y - v0.m_TexCoord.y;
            float DeltaU2 = v2.m_TexCoord.x - v0.m_TexCoord.x;
            float DeltaV2 = v2.m_TexCoord.y - v0.m_TexCoord.y;

            float f = 1.0f / (DeltaU1 * DeltaV2 - DeltaU2 * DeltaV1);

            Vec3 Tangent, Bitangent;

            Tangent.x = f * (DeltaV2 * Edge1.x - DeltaV1 * Edge2.x);
            Tangent.y = f * (DeltaV2 * Edge1.y - DeltaV1 * Edge2.y);
            Tangent.z = f * (DeltaV2 * Edge1.z - DeltaV1 * Edge2.z);

            Bitangent.x = f * (-DeltaU2 * Edge1.x - DeltaU1 * Edge2.x);
            Bitangent.y = f * (-DeltaU2 * Edge1.y - DeltaU1 * Edge2.y);
            Bitangent.z = f * (-DeltaU2 * Edge1.z - DeltaU1 * Edge2.z);

            v0.m_Tangent += Tangent;
            v1.m_Tangent += Tangent;
            v2.m_Tangent += Tangent;

        }
        
        for (int i = 0; i < VCOUNT; ++i)
        {
            Vertices[i].m_Tangent.Normalize();
        }
    }

    ////////////////////////////////////////////////////////////////////////
    void GraphicComponent::ComputeTangent(CommonVector<Vertex>& Vertices, const CommonVector<unsigned int>& Indices)
    {
        size_t ICOUNT = Indices.GetLength();
        size_t VCOUNT = Vertices.GetLength();

		for (size_t i = 0; i < ICOUNT; i += 3)
        {
            uint Index0 = Indices[i];
            uint Index1 = Indices[i+1];
            uint Index2 = Indices[i+2];

            Vertex& v0 = Vertices[Index0];
            Vertex& v1 = Vertices[Index1];
            Vertex& v2 = Vertices[Index2];

            Vec3 Edge1 = v1.m_Pos - v0.m_Pos;
            Vec3 Edge2 = v2.m_Pos - v0.m_Pos;


            // compute tangent in same loop
            float DeltaU1 = v1.m_TexCoord.x - v0.m_TexCoord.x;
            float DeltaV1 = v1.m_TexCoord.y - v0.m_TexCoord.y;
            float DeltaU2 = v2.m_TexCoord.x - v0.m_TexCoord.x;
            float DeltaV2 = v2.m_TexCoord.y - v0.m_TexCoord.y;

            float f = 1.0f / (DeltaU1 * DeltaV2 - DeltaU2 * DeltaV1);

            Vec3 Tangent, Bitangent;

            Tangent.x = f * (DeltaV2 * Edge1.x - DeltaV1 * Edge2.x);
            Tangent.y = f * (DeltaV2 * Edge1.y - DeltaV1 * Edge2.y);
            Tangent.z = f * (DeltaV2 * Edge1.z - DeltaV1 * Edge2.z);

            Bitangent.x = f * (-DeltaU2 * Edge1.x - DeltaU1 * Edge2.x);
            Bitangent.y = f * (-DeltaU2 * Edge1.y - DeltaU1 * Edge2.y);
            Bitangent.z = f * (-DeltaU2 * Edge1.z - DeltaU1 * Edge2.z);

            v0.m_Tangent += Tangent;
            v1.m_Tangent += Tangent;
            v2.m_Tangent += Tangent;

        }

		for (size_t i = 0; i < VCOUNT; ++i)
        {
            Vertices[i].m_Tangent.Normalize();
        }
    }
    ////////////////////////////////////////////////////////////////////////
    template<size_t VCOUNT, size_t ICOUNT>
    void GraphicComponent::ComputeNormals(Vertex (&Vertices)[VCOUNT], const uint (&Indices)[ICOUNT])
    {
		for (size_t i = 0; i < ICOUNT; i += 3)
        {
            uint Index0 = Indices[i];
            uint Index1 = Indices[i+1];
            uint Index2 = Indices[i+2];

            Vertex& v0 = Vertices[Index0];
            Vertex& v1 = Vertices[Index1];
            Vertex& v2 = Vertices[Index2];

            Vec3 Edge1 = v1.m_Pos - v0.m_Pos;
            Vec3 Edge2 = v2.m_Pos - v0.m_Pos;

            Vec3 t_Normal = Edge1.Cross(Edge2);
            t_Normal.Normalize();

            v0.m_Normal += t_Normal;
            v1.m_Normal += t_Normal;
            v2.m_Normal += t_Normal;
        }
        
		for (size_t i = 0; i < VCOUNT; ++i)
        {
            Vertices[i].m_Normal.Normalize();
        }
    }
}
#endif // Engine_GraphicComponent_H

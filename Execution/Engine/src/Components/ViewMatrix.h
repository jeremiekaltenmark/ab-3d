//
//  ViewMatrix.h
//  Engine
//
//  Created by Jeremie Kaltenmark on 23-06-2015.
//  Copyright (c) 2015 Jeremie Kaltenmark. All rights reserved.
//

#ifndef ENGINE_VIEW_MATRIX_H
#define ENGINE_VIEW_MATRIX_H

#include "Maths/Maths.h"
#include "Maths/Matrix.h"
#include "Maths/Quaternion.h"
#include "Components/Component.h"
#include "Engine/Engine.h"
#include "Common/Logger.h"

namespace Engine
{

    class ViewMatrix : public Component
    {
    public:
        inline ViewMatrix(void);
        inline virtual ~ViewMatrix(void);
        inline void Init();
        inline void Update();
        inline void LookAt(const Vec3& aTargetPosition);
        inline void SetForward(const Vec3& aDirection);
        inline void SetPosition(const Vec3& aPos);
        inline void SetUpVector(const Vec3& aPos);

        inline void MoveLeft(float aDist);
        inline void MoveRigth(float aDist);
        inline void MoveUp(float aDist);
        inline void MoveForward(float aDist);

        inline Matrix& GetViewMatrix();
        inline const Matrix& GetViewMatrix() const;
        inline const Vec3& GetPosition() const;
        inline Vec3 CopyPosition() const;
        inline const Vec3& GetForward();
        inline const Vec3& GetUp();

        inline void Rotate(float aDeltaX, float aDeltaY);

    private:
        inline void ComputeViewMat();

    private:

        Vec3 mPos;
        Vec3 mForward;
        Vec3 mUp;

        bool mDirty;

        Matrix mViewMatrix;
    };


    inline Matrix& ViewMatrix::GetViewMatrix()
    {
        Update();
        return mViewMatrix;
    }
    inline const Matrix& ViewMatrix::GetViewMatrix() const
    {
        return mViewMatrix;
    }
    inline void ViewMatrix::LookAt(const Vec3& aTargetPosition)
    {
        mForward = aTargetPosition - mPos;
        mForward.Normalize();

        mDirty = true;
    }
    inline void ViewMatrix::SetForward(const Vec3& aDirection)
    {
        mForward = aDirection;
        mForward.Normalize();

        mDirty = true;
    }
    inline void ViewMatrix::SetPosition(const Vec3& aPos)
    {
        mDirty = true;
        mPos.Set(aPos.X(), aPos.Y(), aPos.Z());
    }
    inline void ViewMatrix::SetUpVector(const Vec3& aUpVector)
    {
        mDirty = true;
        mUp.Set(aUpVector.X(), aUpVector.Y(), aUpVector.Z());
        mUp.Normalize();
    }

    inline void ViewMatrix::MoveForward(float aDist)
    {
        Update();

        Vec3 N = mForward;
        N.Normalize();
        N.SelfScale(aDist);
        mPos += N ;

        mDirty = true;
    }


    inline void ViewMatrix::MoveRigth(float aDist)
    {
        Update();

        Vec3 tTargetDir = mForward;
        tTargetDir.Normalize();

        Vec3 tRight = mUp.Cross(tTargetDir);
        tRight.Normalize();
        tRight.SelfScale(aDist);
        mPos += tRight ;

        mDirty= true;
        //        Logger::LogDebugSoft("Implement Me", DEBUG_FILE_LINE);
    }

    inline void ViewMatrix::MoveLeft(float aDist)
    {
        Update();
        MoveRigth(-aDist);
    }

    inline void ViewMatrix::MoveUp(float aDist)
    {
        Update();
        mPos += mUp * aDist;
        mDirty= true;
    }
    
    
    
    inline void ViewMatrix::Update()
    {
        if (mDirty)
        {
            ComputeViewMat();
        }
    }
    
    inline Vec3 ViewMatrix::CopyPosition() const
    {
        return mPos;
    }
    inline const Vec3& ViewMatrix::GetPosition() const
    {
        return mPos;
    }
    inline const Vec3& ViewMatrix::GetForward()
    {
        Update();
        return mForward;
    }
    inline const Vec3& ViewMatrix::GetUp()
    {
        Update();
        return mUp;
    }

    inline ViewMatrix::ViewMatrix(void) : mPos(0.f,0.f,0.f), mForward(0.f,0.f,1.f), mUp(0.f,1.f,0.f), mDirty(true)
    {
//        m_type = VIEW_MATRIX;
    }

    inline ViewMatrix::~ViewMatrix(void)
    {
        //	Logger::LogDebugSoft("~ViewMatrix", DEBUG_SOFT_INFO);
    }

    inline void ViewMatrix::Init()
    {
        mPos.Set(0,0, -6);
        mUp.Set(0,1,0);
        LookAt(Vec3(0,0, -5));
        mDirty = true;
    }

    inline void ViewMatrix::ComputeViewMat()
    {
        Vec3 tForward = mForward;
        tForward.Normalize();

        mUp.Normalize();
        Vec3 tUp = Vec3(0.0f,1.0f,0.0f);
        Vec3 tRight = tUp.Cross(tForward);
        tRight.Normalize();

        //    tUp = tRight.Cross(tForward);//mUp;
        tUp = tForward.Cross(tRight);//mUp;
        tUp.Normalize();

        mForward = tForward;
        mUp = tUp;

        mViewMatrix[0][0] = tRight.X();     mViewMatrix[0][1] = tRight.Y();     mViewMatrix[0][2] = tRight.Z();     mViewMatrix[0][3] = 0.0f;
        mViewMatrix[1][0] = tUp.X();        mViewMatrix[1][1] = tUp.Y();        mViewMatrix[1][2] = tUp.Z();        mViewMatrix[1][3] = 0.0f;
        mViewMatrix[2][0] = tForward.X();   mViewMatrix[2][1] = tForward.Y();   mViewMatrix[2][2] = tForward.Z();   mViewMatrix[2][3] = 0.0f;
        mViewMatrix[3][0] = 0.0f;           mViewMatrix[3][1] = 0.0f;           mViewMatrix[3][2] = 0.0f;           mViewMatrix[3][3] = 1.0f;

        mViewMatrix.SelfTranslate(-mPos.X(), -mPos.Y(), -mPos.Z());
        mDirty = false;
    }

    inline void ViewMatrix::Rotate(float aDeltaX, float aDeltaY)
    {
        float tAngleX = aDeltaX/ 40.0f;
        float tAngleY = aDeltaY/ 40.0f;
        //    const Vec3 t_Up = mUp;//(0.0f, 1.0f, 0.0f);

        // Rotate the view vector by the horizontal angle around the vertical axis
        mUp.Normalize();
        Vec3 t_Forward = mForward; //(0.0f, 0.0f, 1.0f);
        t_Forward.SelfRotate(tAngleX, mUp);
        t_Forward.Normalize();

        // Rotate the view vector by the vertical angle around the horizontal axis
        Vec3 Haxis = mUp.Cross(t_Forward);
        Haxis.Normalize();
        t_Forward.SelfRotate(tAngleY, Haxis);
        t_Forward.Normalize();
        
        mForward = t_Forward;
        mForward.Normalize();

        mDirty = true;
    }

}

#endif

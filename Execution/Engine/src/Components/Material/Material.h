/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 08-04-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#ifndef Engine_Material_H
#define Engine_Material_H

#include "Engine/Engine.h"
#include "Rendering/GraphicDevice.h"
#include "Components/Component.h"
#include "Components/Material/Texture.h"

namespace Engine
{
    class Material;
}
extern CommonSharedPtr<const Engine::Material> NullMaterial;

namespace Engine
{
	class Program;
	union Handle;

    class Material : public Component
	{

    public:
        enum TEXTURES {DIFFUSE = 0, SHADOW_MAP = 1, NORMAL_MAP = 2, COUNT};

	private:
		CommonSharedPtr<Program> m_ProgramShader;
        CommonSharedPtr<Texture> m_Texture[TEXTURES::COUNT];

		float m_MatSpecIntens;
		float m_SpecPower;

	protected:
	public:
		Material();
		Material(const Material& aCopy);

		virtual ~Material();
				
		Handle SetShader(GraphicDevice::PROGRAMS);
		void SetShader(CommonSharedPtr<Program>& a_ValidProgram );
		CommonSharedPtr<Program>& GetShader() { return m_ProgramShader; }
		const CommonSharedPtr<Program>& GetShader() const { return m_ProgramShader; }

		CommonSharedPtr<Texture>& GetTexture(TEXTURES a_TextureType) { return m_Texture[a_TextureType]; }
		void SetTexture(const CommonSharedPtr<Texture>& aTex, TEXTURES a_TextureType) { m_Texture[a_TextureType] = aTex; }

		float GetSpecularIntens() { return m_MatSpecIntens;}
		float GetSpecularPower() { return m_SpecPower;	 }

		void Bind(CommonSharedPtr<const Material>& a_PreviousMaterial = NullMaterial)const;
		void BindTextures(CommonSharedPtr<const Material>& a_PreviousMaterial = NullMaterial)const;
		void BindTexture(TEXTURES a_texType, CommonSharedPtr<const Material>& a_PreviousMaterial = NullMaterial)const;
		void BindProgram(CommonSharedPtr<const Material>& a_PreviousMaterial = NullMaterial)const;

		void Bind(CommonSharedPtr<Material>& a_PreviousMaterial)const;
		void BindTextures(CommonSharedPtr<Material>& a_PreviousMaterial)const;
		void BindTexture(TEXTURES a_texType, CommonSharedPtr<Material>& a_PreviousMaterial)const;
		void BindProgram(CommonSharedPtr<Material>& a_PreviousMaterial)const;

	};
	
}

#endif // Engine_Material_H

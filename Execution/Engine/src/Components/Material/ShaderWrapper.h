/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
*	Project PER - internal Game Engine
*	Organisation: www.jeremkalt.xyz
*
*	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
*	Created on: 03-09-2014
*
*	Copyright (c) 2014 CDRIN. All rights reserved.
*/


#ifndef Engine_ShaderWrapper_H
#define Engine_ShaderWrapper_H

#include "Engine/Engine.h"

namespace Engine
{
	enum ShaderType
	{
		VERTEX,
		GEOMETRY,
		FRAGMENT,
		TESSELATION,
		COMPUTE,
		COUNT
	};


	inline ShaderType ConvertFromGL(GLenum aShaderType)
	{
		switch (aShaderType)
		{
		case GL_VERTEX_SHADER:
			return ShaderType::VERTEX;
		case GL_GEOMETRY_SHADER:
			return ShaderType::GEOMETRY;
		case GL_FRAGMENT_SHADER:
			return ShaderType::FRAGMENT;
		case GL_TESS_CONTROL_SHADER:
			return ShaderType::TESSELATION;
//		case GL_COMPUTE_SHADER:
//			return ShaderType::COMPUTE;
		default:
			return ShaderType::COUNT;
		}
	}
	inline GLenum ConvertFromEngine(ShaderType aShaderType)
	{
		switch (aShaderType)
		{
		case ShaderType::VERTEX:
			return GL_VERTEX_SHADER;
		case ShaderType::GEOMETRY:
			return GL_GEOMETRY_SHADER;
		case ShaderType::FRAGMENT:
			return GL_FRAGMENT_SHADER;
		case ShaderType::TESSELATION:
			return GL_TESS_CONTROL_SHADER;
//		case ShaderType::COMPUTE:
//			return GL_COMPUTE_SHADER;
		default:
			return GL_INVALID_ENUM;
		}
	}

	class Shader;
	class VertexShader;
	class FragmentShader;
	class GraphicComponent;

	struct Uniform
	{
		CommonString mName;
		int mLocation;
		GLenum mType;
		Uniform(const char* aName, int aLoc, GLenum aType) : mName(aName), mLocation(aLoc), mType(aType) {}
		Uniform() {};
	};


	// Warning, currently we only support one type of Subroutine by shader (can be one in fragment, and one in vertex for example)
	struct Subroutine
	{
		CommonString mName;
		GLenum mGLShadertype;
		ShaderType mShadertype; // EASIER DEBUG
		uint32_t mIndex;
		Subroutine(const char* aName, int aLoc, GLenum aShadertype) : mName(aName), mIndex(aLoc), mGLShadertype(aShadertype), mShadertype(ConvertFromGL(aShadertype)) {}
		Subroutine() {};
	};

	struct Attribute
	{
		CommonString mName;
		int mLocation;
		GLenum mType;
		Attribute(const char* aName, int aLoc, GLenum aType) : mName(aName), mLocation(aLoc), mType(aType) {}
		Attribute() {};
	};

	union Handle
	{
		void* ptr;
		ui32  id;

		Handle() : id(0)
		{
		}

		operator ui32 () const { return id; }
		operator const void* () const { return ptr; }
	};


}

#endif // Engine_ShaderWrapper_H
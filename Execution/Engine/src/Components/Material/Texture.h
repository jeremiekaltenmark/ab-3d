/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 10-06-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#ifndef Engine_Texture_H
#define Engine_Texture_H


namespace Engine
{

class Texture
{
private:
	friend class ShadowMapRender;
	CommonString m_fileName;
	GLenum m_textureTarget;
	GLuint m_glTextureId;
	bool m_IsLoaded;
    bool m_IsDefault;

protected:
	bool LoadFile(const char* a_filename, GLenum a_TextureTarget, GLint a_MipMapLevel, GLint a_Border, FIBITMAP* &t_LoadedImagePtr, FIBITMAP* &t_Image32Ptr, BYTE* &t_Pixels, unsigned int &t_Width, unsigned int &t_Height);
	virtual void GenerateGLTexture(GLenum a_TextureTarget, GLint a_MipMapLevel, unsigned int t_Width, unsigned int t_Height, GLint a_Border, BYTE* t_Pixels);

public:
	Texture();
	Texture(const Texture& aCopy);

	static CommonSharedPtr<Texture> Wrap(const CommonString& m_fileName, GLenum m_textureTarget, GLuint m_glTextureId);

	virtual ~Texture();

	//load a texture and make it the current texture
	//if texID is already in use, it will be unloaded and replaced with this texture
	bool LoadTexture(const char* filename,		//where to load the file from
		GLenum a_TextureTarget = GL_TEXTURE_2D,	// texture target as called by ogldev tutos
		GLint a_MipMapLevel = 0,				//mipmapping level
		GLint a_Border = 0);					//border size


	void Bind(GLenum TextureUnit);
    void SetIsDefault(bool a_Isdefault) { m_IsDefault = a_Isdefault;}
    bool IsDefault() { return m_IsDefault;}
    CommonString& GetName() {return m_fileName; }
    CommonString GetName() const {return m_fileName; }

	operator bool() { return m_glTextureId != 0; }
};

}
#endif // Engine_Texture_H

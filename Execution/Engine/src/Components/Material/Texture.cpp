/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 10-06-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */
#include "Engine/Engine.h"
#include "Components/Material/Texture.h"

using namespace Engine;


////////////////////////////////////////////////////////////////////////
Texture::Texture() : m_IsLoaded(false), m_IsDefault(false)
{

}

////////////////////////////////////////////////////////////////////////
Texture::Texture(const Texture& aCopy)
{
	(void) aCopy;
}

////////////////////////////////////////////////////////////////////////
Texture::~Texture()
{
    //Logger::LogDebugSoft("~Texture", DEBUG_SOFT_INFO);
	glDeleteTextures(1, &m_glTextureId);
}


////////////////////////////////////////////////////////////////////////
void Texture::Bind(GLenum TextureUnit)
{
    if(m_IsLoaded)
    {
        glActiveTexture(TextureUnit);
        glBindTexture(m_textureTarget, m_glTextureId);
    }
}

////////////////////////////////////////////////////////////////////////
bool Texture::LoadTexture(const char* a_filename, GLenum a_TextureTarget, GLint a_MipMapLevel, GLint a_Border)
{
	FIBITMAP *t_LoadedImagePtr(0);							//pointer to the image, once loaded
	FIBITMAP *t_Image32Ptr(0);								//pointer to the 32bits image, once loaded
	BYTE* t_Pixels(0);										//pointer to the image data
	unsigned int t_Width(0), t_Height(0);					//image width and height

	if (!LoadFile(a_filename, a_TextureTarget, a_MipMapLevel, a_Border, t_LoadedImagePtr, t_Image32Ptr, t_Pixels, t_Width, t_Height))
		return false;

	GenerateGLTexture(a_TextureTarget, a_MipMapLevel, t_Width, t_Height, a_Border, t_Pixels);

	//Free FreeImage's copy of the data
	FreeImage_Unload(t_Image32Ptr);
	FreeImage_Unload(t_LoadedImagePtr);

	//return success
	m_IsLoaded = true;
	return m_IsLoaded;
}

CommonSharedPtr<Texture> Engine::Texture::Wrap(const CommonString& m_fileName, GLenum m_textureTarget, GLuint m_glTextureId)
{
	CommonSharedPtr<Texture> t_ret(new Texture);

	t_ret->m_fileName = m_fileName;
	t_ret->m_textureTarget = m_textureTarget;
	t_ret->m_glTextureId = m_glTextureId;
	t_ret->m_IsLoaded = true;

	return t_ret;
}

bool Engine::Texture::LoadFile(const char* a_filename, GLenum a_TextureTarget, GLint a_MipMapLevel, GLint a_Border, FIBITMAP* &t_LoadedImagePtr, FIBITMAP* &t_Image32Ptr, BYTE* &t_Pixels, unsigned int &t_Width, unsigned int &t_Height)
{
	FREE_IMAGE_FORMAT t_FileImageFormat = FIF_UNKNOWN;		//image format


	//check the file signature and deduce its format
	t_FileImageFormat = FreeImage_GetFileType(a_filename, 0);
	if (t_FileImageFormat == FIF_UNKNOWN)							//if still unknown, try to guess the file format from the file extension
	{
		t_FileImageFormat = FreeImage_GetFIFFromFilename(a_filename);
	}
	if (t_FileImageFormat == FIF_UNKNOWN)							//if still unkown, return failure
	{
		Logger::LogDebugSoft(CommonString("cannot find image type: ") + a_filename, DEBUG_SOFT_INFO);
		m_IsLoaded = false;
		return m_IsLoaded;
	}

	//check that the plugin has reading capabilities and load the file
	if (FreeImage_FIFSupportsReading(t_FileImageFormat))
	{
		t_LoadedImagePtr = FreeImage_Load(t_FileImageFormat, a_filename);
	}

	if (!t_LoadedImagePtr)											//if the image failed to load, return failure
	{
		CommonString t_Message = "--\nCan't load image ";
		t_Message += a_filename;
		CommonString tName(a_filename);
		CommonString tExt = tName.GetExtension();

		if (tExt != "jpg")
		{
			tName = tName.ExcludeExtension();
			tName.ConvertSlash();
			tName += ".jpg";
			if (!LoadTexture(tName.GetChars(), a_TextureTarget, a_MipMapLevel, a_Border))
			{
				Logger::LogDebugSoft(t_Message, DEBUG_SOFT_INFO);
				m_IsLoaded = false;
				return m_IsLoaded;
			}
			else
			{
				t_Message += " but jpg version worked !";
				Logger::LogDebugSoft(t_Message, DEBUG_SOFT_INFO);
			}
		}
		else
		{
			Logger::LogDebugSoft(t_Message, DEBUG_SOFT_INFO);
			m_IsLoaded = false;
			return m_IsLoaded;
		}
	}
	else
	{
		CommonString t_Message = "--\nimage ";
		t_Message += a_filename;
		t_Message += " has been loaded";
		Logger::LogLine(t_Message);
		t_Image32Ptr = FreeImage_ConvertTo32Bits(t_LoadedImagePtr);

		//FREE_IMAGE_TYPE t_Type = FreeImage_GetImageType(t_ImagePtr);
		int t_size = FreeImage_GetBPP(t_Image32Ptr);
		ASSERT(t_size == 32, "wrong pixel size... it should be 8 bits for each RGBA channels");
	}

	t_Pixels = FreeImage_GetBits(t_Image32Ptr);						//retrieve the image pixels
	t_Width = FreeImage_GetWidth(t_Image32Ptr);
	t_Height = FreeImage_GetHeight(t_Image32Ptr);
	if ((t_Pixels == 0) || (t_Width == 0) || (t_Height == 0))	//if this somehow one of these failed (they shouldn't), return failure
	{
		CommonString t_Message = "image ";
		t_Message += a_filename;
		t_Message += " is weird";
		Logger::LogDebugSoft(t_Message, DEBUG_SOFT_INFO);
		m_IsLoaded = false;
		return m_IsLoaded;
	}
	m_fileName = a_filename;

	return true;
}

void Engine::Texture::GenerateGLTexture(GLenum a_TextureTarget, GLint a_MipMapLevel, unsigned int t_Width, unsigned int t_Height, GLint a_Border, BYTE* t_Pixels)
{
	glGenTextures(1, &m_glTextureId);						//generate an OpenGL texture ID for this texture
	m_textureTarget = a_TextureTarget;
	glBindTexture(m_textureTarget, m_glTextureId);			//bind to the new texture ID
	glTexImage2D(m_textureTarget, a_MipMapLevel, GL_RGBA8, t_Width, t_Height, a_Border, GL_BGRA, GL_UNSIGNED_BYTE, (GLvoid*)t_Pixels); //store the texture data for OpenGL use

	glTexParameterf(m_textureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(m_textureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

// 	glTexParameteri(m_textureTarget, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
// 	glTexParameteri(m_textureTarget, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
// 	glTexParameteri(m_textureTarget, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}

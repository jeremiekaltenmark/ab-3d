/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Projet Personnel - Moteur de jeu interne
 *	Organisation: www.jeremkalt.xyz pour CDRIN.
 *
 *	Auteur: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Date cr�ation: 03-03-2014
 *	Refactor: 08-07-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#include "Engine/Engine.h"
#include "Components/Material/Shader.h"
#include "IO/IOManager.h"
#include "Engine/Application.h"

#include "Common/CommonString.h"
#include "Common/CommonFile.h"
#include "Common/CommonVector.h"
#include "IO/FileSystem.h"
#include "Common/Logger.h"
#include "Maths/Maths.h"

using namespace Engine;

////////////////////////////////////////////////////////////////////////
Shader::Shader()
{
//    m_type = SHADER;
	m_ShaderID = HandleError;
	m_Name = "";
}

////////////////////////////////////////////////////////////////////////
Shader::Shader(const Shader& /*aCopy*/)
{
    Logger::LogDebug("implement me", DEBUG_INFO);
//    m_type = aCopy.m_type;
}

////////////////////////////////////////////////////////////////////////
Shader::~Shader()
{
	//Logger::LogDebugSoft("~Shader", DEBUG_SOFT_INFO);
}

////////////////////////////////////////////////////////////////////////
bool Engine::Shader::Build()
{
	if (m_ShaderCode == NULL)
		return false;

	if (m_Type == VERTEX)
		m_ShaderID.id = glCreateShader(GL_VERTEX_SHADER);
	else if (m_Type == FRAGMENT)
		m_ShaderID.id = glCreateShader(GL_FRAGMENT_SHADER);

	// Compile Vertex Shader
	Logger::Printf("Compiling shader : %s\n", m_Name.GetChars());
	char const * VertexSourcePointer = m_ShaderCode->GetChars();
	glShaderSource(m_ShaderID.id, 1, &VertexSourcePointer, NULL);
	glCompileShader(m_ShaderID.id);

	GLint Result = GL_FALSE;
	int InfoLogLength;
	// Check Shader
	glGetShaderiv(m_ShaderID.id, GL_COMPILE_STATUS, &Result);
	if (Result == GL_FALSE)
	{
		Logger::LogDebug("error while setting vertex shader sources", DEBUG_INFO);
		glGetShaderiv(m_ShaderID.id, GL_INFO_LOG_LENGTH, &InfoLogLength);

		if (InfoLogLength > 1)
		{
			CommonVector<char> VertexShaderErrorMessage(InfoLogLength);
			glGetShaderInfoLog(m_ShaderID.id, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
			Logger::LogDebug(CommonString(VertexShaderErrorMessage), DEBUG_INFO);

			glDeleteShader(m_ShaderID.id);
			m_ShaderID = HandleError;
			return false;
		}
	}
	Logger::LogLine("\tSuccessfully build shader");

	return true;
}

////////////////////////////////////////////////////////////////////////
bool Shader::InternalLoadShader(const CommonString& a_ShaderPath/*, const CommonString& aFragmentShaderPath, bool aForceReload*/)
{
	m_Name = a_ShaderPath;

	// Read the Shader code from the file
	m_ShaderCode = FileSystem::LoadAsString(a_ShaderPath);
	if (m_ShaderCode == NULL)
	{
		Logger::LogDebug(a_ShaderPath, DEBUG_INFO);
		Logger::Printf("\n!! Warning cannot open %s\n", a_ShaderPath.GetChars());
		m_ShaderID = HandleError;

		return false;
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////
template<>
CommonSharedPtr<VertexShader> Shader::LoadShader<VertexShader>(const CommonString& a_ShaderPath)
{
	CommonSharedPtr<VertexShader>  t_Shader = CommonSharedPtr<VertexShader>(new VertexShader());
	t_Shader->InternalLoadShader(a_ShaderPath);
	return t_Shader;
}
template<>
CommonSharedPtr<FragmentShader> Shader::LoadShader<FragmentShader>(const CommonString& a_ShaderPath)
{
	CommonSharedPtr<FragmentShader>  t_Shader = CommonSharedPtr<FragmentShader>(new FragmentShader());

	t_Shader->InternalLoadShader(a_ShaderPath);
	return t_Shader;
}

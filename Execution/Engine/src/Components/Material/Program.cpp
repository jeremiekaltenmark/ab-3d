/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 08-07-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#include "Engine/Engine.h"
#include "Components/Material/Program.h"
#include "Components/Material/Shader.h"
#include "IO/IOManager.h"
#include "Engine/Application.h"

#include "Common/CommonString.h"
#include "Common/CommonFile.h"
#include "Common/CommonVector.h"
#include "IO/FileSystem.h"
#include "Common/Logger.h"
#include "Maths/Maths.h"
#include "Components/GraphicComponent.h"
#include "Engine/Application.h"
#include "Components/Scene.h"

////////////////////////////////////////////////////////////////////////
using namespace Engine;

////////////////////////////////////////////////////////////////////////
Engine::Handle HandleError;
Engine::Uniform NullUniform("EMPTY_UNIFORM", 0xFFFFFFFF, 0);
Engine::Attribute NullAttribute("EMPTY_ATTRIBUTE", 0xFFFFFFFF, 0);
Engine::Subroutine NullSubroutine("EMPTY_SUBROUTINE", 0xFFFFFFFF, 0);

////////////////////////////////////////////////////////////////////////
void Program::Attach(const CommonSharedPtr<VertexShader>& aVertex, const CommonSharedPtr<FragmentShader>& aFragment)
{
	if (aVertex && aVertex->GetHandle() != 0)
	{
		mVertex = aVertex;
		m_Name += mVertex->GetName();

        mComponents[&typeid(VertexShader)] = mVertex;
    }

	if (aFragment && aFragment->GetHandle() != 0)
	{
		mFragment = aFragment;
		m_Name += " + ";
		m_Name += mFragment->GetName();

        mComponents[&typeid(FragmentShader)] = mFragment;

	}

}

////////////////////////////////////////////////////////////////////////
void Program::Detach()
{
	mVertex.reset();
	mFragment.reset();
}

////////////////////////////////////////////////////////////////////////
bool Program::Link()
{
	if (mVertex && mFragment)
	{
		const ui32 hVertex = mVertex->GetHandle();
		const ui32 hFragment = mFragment->GetHandle();

		if (hVertex && hFragment)
		{
			glAttachShader(m_ProgramID, hVertex);
			glAttachShader(m_ProgramID, hFragment);
			glLinkProgram(m_ProgramID);

			// Check if linking succeeded
			int tLinked = 0;
			int InfoLogLength = 0;
			glGetProgramiv(m_ProgramID, GL_LINK_STATUS, &tLinked);
			if (tLinked == GL_FALSE)
			{
				Logger::LogDebug("error while linking shaders", DEBUG_INFO);
				glGetProgramiv(m_ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
				if (InfoLogLength > 1)
				{
					CommonVector<char> ProgramErrorMessage(InfoLogLength);
					glGetProgramInfoLog(m_ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
					Logger::LogDebug(CommonString(ProgramErrorMessage), DEBUG_INFO);

					glDeleteShader(hVertex);
					glDeleteShader(hFragment);
					glDeleteProgram(m_ProgramID);
					m_ProgramID = HandleError;
					return false;
				}
			}


            RegisterUniforms();
            SetTextureImageUnit();

			// validate
			int tValidated = 0;
			glValidateProgram(m_ProgramID);
			glGetProgramiv(m_ProgramID, GL_VALIDATE_STATUS, &tValidated);

//			ASSERT(tValidated == GL_TRUE, "mShaderProgram: GL_VALIDATE_STATUS returned GL_FALSE");
			//ASSERT_GL_ERROR();

			if (tValidated == GL_TRUE)
			{

                RegisterAttributes();
				RegisterSubroutines(GL_VERTEX_SHADER);
				RegisterSubroutines(GL_GEOMETRY_SHADER);
				RegisterSubroutines(GL_FRAGMENT_SHADER);
//				RegisterSubroutines(GL_COMPUTE_SHADER);
				RegisterSubroutines(GL_TESS_CONTROL_SHADER);

				InternalInit();

				return true;
			}
            else
            {
                glGetProgramiv(m_ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
                if (InfoLogLength > 1)
                {
                    CommonVector<char> ProgramErrorMessage(InfoLogLength);
                    glGetProgramInfoLog(m_ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
                    Logger::LogDebug(CommonString(ProgramErrorMessage), DEBUG_INFO);

                    glDeleteShader(hVertex);
                    glDeleteShader(hFragment);
                    glDeleteProgram(m_ProgramID);
                    m_ProgramID = HandleError;
                    return false;
                }
            }
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////
void Program::Validate() const
{
    int InfoLogLength = 0;
    int tValidated = 0;
    glValidateProgram(m_ProgramID);
    glGetProgramiv(m_ProgramID, GL_VALIDATE_STATUS, &tValidated);

    //			ASSERT(tValidated == GL_TRUE, "mShaderProgram: GL_VALIDATE_STATUS returned GL_FALSE");
    //ASSERT_GL_ERROR();

    if (tValidated != GL_TRUE)
    {
        glGetProgramiv(m_ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
        if (InfoLogLength > 1)
        {
            CommonVector<char> ProgramErrorMessage(InfoLogLength);
            glGetProgramInfoLog(m_ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
            Logger::LogDebug(CommonString(ProgramErrorMessage), DEBUG_INFO);
//            m_ProgramID = HandleError;
            return;
        }
    }
}

////////////////////////////////////////////////////////////////////////
void Program::SetTextureImageUnit()
{
    Bind();

    if (m_HasTexture)
    {
        glUniform1i(m_Uniforms[m_TextureIndex].mLocation, 0);
    }
    if (m_HasShadowTexture)
    {
        glUniform1i(m_Uniforms[m_ShadowTextureIndex].mLocation, 1);
    }
    if (m_HasNormalTexture)
    {
        glUniform1i(m_Uniforms[m_NormalTextureIndex].mLocation, 2);
    }
    Unbind();
}

////////////////////////////////////////////////////////////////////////
void Program::InternalInit()
{
	//NullUniform.mLocation = 0xFFFFFFFF; 
	HandleError.id = 0;
}

////////////////////////////////////////////////////////////////////////
void Program::Bind()const
{
	glUseProgram(m_ProgramID);

    if (m_HasPositionAttribute)
    {
        glEnableVertexAttribArray(m_Attributes[m_PositionIndex].mLocation);
    }
    if (m_HasTexCoordAttribute)
    {
        glEnableVertexAttribArray(m_Attributes[m_TexCoordIndex].mLocation);
    }
    if(m_HasNormalAttribute)
    {
        glEnableVertexAttribArray(m_Attributes[m_NormalIndex].mLocation);
    }
    if(m_HasTangentAttribute)
    {
        glEnableVertexAttribArray(m_Attributes[m_TangentIndex].mLocation);
    }


//    if(m_Subroutines.GetLength() > 0 )
//        SelectSubroutine(0);
}

////////////////////////////////////////////////////////////////////////
void Program::Unbind()const
{
	glUseProgram(0);
}

void Program::UpdateUniform(uint32_t a_UniLocation, const Matrix& aMatUniform)const
{
	glUniformMatrix4fv(a_UniLocation, 1, GL_TRUE, aMatUniform.mLineMat);
}

////////////////////////////////////////////////////////////////////////
void Program::Update(const CommonSharedPtr<GraphicComponent>& a_Renderable, const Matrix& a_ProjectionMatrix, const ViewMatrix& a_ViewMatrix)const
{
//#if defined _DEBUG || DEBUG
//    Validate();
//#endif


    if (m_HasMVPUniform)
    {
        //Matrix t_MVP = a_ProjectionMatrix * a_ViewMatrix * a_Renderable->GetTransform()->GetConstMatrixTransform();
        Matrix t_MVP = a_ProjectionMatrix * a_ViewMatrix.GetViewMatrix() * a_Renderable->GetGlobalModelMatrix();
        glUniformMatrix4fv(m_Uniforms[m_MVPIndex].mLocation, 1, GL_TRUE, t_MVP.mLineMat);
    }


	if (m_HasWorldUniform)
	{
		glUniformMatrix4fv(m_Uniforms[m_WorldIndex].mLocation, 1, GL_TRUE, a_Renderable->GetGlobalModelMatrix().mLineMat);
	}

	if (m_HasUniform1)
	{
		glUniform1f(m_Uniforms[m_Uniform1Index].mLocation, a_Renderable->GetColor());
	}

	if (m_HasSpecIntens)
	{
		glUniform1f(m_Uniforms[m_SpecIntensIndex].mLocation, a_Renderable->GetMaterial()->GetSpecularIntens());
	}
	if (m_HasSpecPower)
	{
		glUniform1f(m_Uniforms[m_SpecPowerIndex].mLocation, a_Renderable->GetMaterial()->GetSpecularPower());
	}

}

////////////////////////////////////////////////////////////////////////
void Engine::Program::PostUpdate()const
{
	if (m_HasPositionAttribute)
	{
		glDisableVertexAttribArray(m_Attributes[m_PositionIndex].mLocation);
	}
	if (m_HasTexCoordAttribute)
	{
		glDisableVertexAttribArray(m_Attributes[m_TexCoordIndex].mLocation);
    }
    if (m_HasNormalAttribute)
    {
        glDisableVertexAttribArray(m_Attributes[m_NormalIndex].mLocation);
    }
    if (m_HasTangentAttribute)
    {
        glDisableVertexAttribArray(m_Attributes[m_TangentIndex].mLocation);
    }

	Unbind();
}

////////////////////////////////////////////////////////////////////////
void Engine::Program::RegisterSubroutines(GLenum ShaderType)
{
	CheckGlError("before RegisterSubroutines", DEBUG_INFO);

	GLint t_SubroutinesNbr, t_MaxLenUniform;

	glGetProgramStageiv(m_ProgramID, ShaderType, GL_ACTIVE_SUBROUTINES, &t_SubroutinesNbr);
	CheckGlError("number of active subroutines", DEBUG_INFO);

	if (t_SubroutinesNbr == 0)
		return;


	glGetProgramStageiv(m_ProgramID, ShaderType, GL_ACTIVE_SUBROUTINE_MAX_LENGTH, &t_MaxLenUniform);
	CheckGlError("maximum length of subroutine's names", DEBUG_INFO);

	if (t_MaxLenUniform == 0)
	{
		Logger::LogDebug("subroutine with no NAME ??", DEBUG_INFO);
		return;
	}

	GLchar * t_Name = (GLchar *)malloc(t_MaxLenUniform);

	for (int i = 0; i < t_SubroutinesNbr; ++i)
	{
		glGetActiveSubroutineName(m_ProgramID, ShaderType, i, t_MaxLenUniform, NULL, t_Name);
		CheckGlError("glGetActiveSubroutineName", DEBUG_INFO);
		Subroutine&& t_SbR = Subroutine(t_Name, i, ShaderType);
		m_Subroutines.AddLast(t_SbR);
	}
	free(t_Name);
}

void Engine::Program::FrameUpdate(const Matrix& a_ProjectionMatrix, const ViewMatrix& a_ViewMatrix) const
{

    if (m_ProgramID.id == Application::GetSingleton()->GetScene()->GetLight()->m_ProgramID.id)
        Application::GetSingleton()->GetScene()->GetLight()->Update();
    
    if (m_HasProjectionUniform)
    {
        glUniformMatrix4fv(m_Uniforms[m_ProjIndex].mLocation, 1, GL_TRUE, a_ProjectionMatrix.mLineMat);
    }

    if (m_HasViewUniform)
    {
        glUniformMatrix4fv(m_Uniforms[m_ViewIndex].mLocation, 1, GL_TRUE, a_ViewMatrix.GetViewMatrix().mLineMat);
    }

    if(m_HasCamPos)
    {
        glUniform3fv(m_Uniforms[m_CamPosIndex].mLocation, 1, a_ViewMatrix.GetPosition().t);
    }


}

void Engine::Program::ObjectUpdate() const
{

}

void Engine::Program::InitUpdate() const
{

}

////////////////////////////////////////////////////////////////////////
void Program::RegisterUniforms()
{
	GLint t_UniformsNbr, t_MaxLenUniform, t_Size, t_Location;
	GLsizei t_Written;
	GLenum t_Type;
	glGetProgramiv(m_ProgramID, GL_ACTIVE_UNIFORMS, &t_UniformsNbr);
	glGetProgramiv(m_ProgramID, GL_ACTIVE_UNIFORM_MAX_LENGTH, &t_MaxLenUniform);
	GLchar * t_Name = (GLchar *) malloc(t_MaxLenUniform);


	for (int i = 0; i < t_UniformsNbr; ++i)
	{
		glGetActiveUniform(m_ProgramID, i, t_MaxLenUniform, &t_Written, &t_Size, &t_Type, t_Name);
		t_Location = glGetUniformLocation(m_ProgramID, t_Name);
		Uniform&& t_Uniform = Uniform(t_Name, t_Location, t_Type);
		if (t_Uniform.mName == "MVP")
		{
			m_HasMVPUniform = true;
			m_MVPIndex = m_Uniforms.GetLength();
		}
		else if (t_Uniform.mName == "ProjectionMat")
        {
            m_HasProjectionUniform = true;
            m_ProjIndex = m_Uniforms.GetLength();
        }
        else if (t_Uniform.mName == "ViewMat")
        {
            m_HasViewUniform = true;
            m_ViewIndex = m_Uniforms.GetLength();
        }
        else if (t_Uniform.mName == "World")
		{
			m_HasWorldUniform = true;
			m_WorldIndex = m_Uniforms.GetLength();
		}
		else if (t_Uniform.mName == "mUniform1")
		{
			m_HasUniform1 = true;
			m_Uniform1Index = m_Uniforms.GetLength();
		}
		else if (t_Uniform.mName == "CameraPosition")
		{
			m_HasCamPos = true;
			m_CamPosIndex = m_Uniforms.GetLength();
		}
		else if (t_Uniform.mName == "matSpecIntens")
		{
			m_HasSpecIntens = true;
			m_SpecIntensIndex = m_Uniforms.GetLength();
		}
		else if (t_Uniform.mName == "SpecPower")
		{
			m_HasSpecPower = true;
			m_SpecPowerIndex = m_Uniforms.GetLength();
		}
		else if (t_Uniform.mType == GL_SAMPLER_2D)
		{
            int t_Unit ;
            if(t_Uniform.mName == "gShadowMap")
            {
                m_HasShadowTexture = true;
                m_ShadowTextureIndex = m_Uniforms.GetLength();
                t_Unit = 1;
            }
            else if(t_Uniform.mName == "gNormalMap")
            {
                m_HasNormalTexture = true;
                m_NormalTextureIndex = m_Uniforms.GetLength();
                t_Unit = 2;
            }
            else
            {
                m_HasTexture = true;
                m_TextureIndex = m_Uniforms.GetLength();
                t_Unit = 0;
            }
            Logger::Printf("\tSampler \'%s\' at location %d will get unit %d\n", t_Uniform.mName.GetChars(), t_Uniform.mLocation, t_Unit);
		}
        else if(t_Uniform.mType == GL_SAMPLER_2D_SHADOW)
        {
            m_HasShadowTexture = true;
            m_ShadowTextureIndex = m_Uniforms.GetLength();
            int t_Unit = 1;
            Logger::Printf("\tSampler \'%s\' at location %d will get unit %d\n", t_Uniform.mName.GetChars(), t_Uniform.mLocation, t_Unit);
        }

		else if(!CustomRegister(t_Uniform))
		{
			Logger::Printf("other uniforms you need to specifically update: \n");
			Logger::Printf(" - name: %s\n", t_Uniform.mName.GetChars());
			Logger::Printf(" - location: %d\n", t_Uniform.mLocation);
			Logger::Printf(" - type: %d\n\n", t_Uniform.mType);

		}
		m_Uniforms.AddLast(t_Uniform);
	}

	free(t_Name);
}

////////////////////////////////////////////////////////////////////////
void Program::RegisterAttributes()
{
    GLint t_AttrNbr, t_maxLengthAttrName;
    GLint t_Written, t_Size, t_Location;
    GLenum t_Type;

    glGetProgramiv(m_ProgramID, GL_ACTIVE_ATTRIBUTES, &t_AttrNbr);
    glGetProgramiv(m_ProgramID, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &t_maxLengthAttrName);
    GLchar * t_Name = (GLchar *) malloc(t_maxLengthAttrName);

    for (int i = 0; i < t_AttrNbr; ++i)
    {
        glGetActiveAttrib(m_ProgramID, i, t_maxLengthAttrName, &t_Written, &t_Size, &t_Type, t_Name);
        t_Location = glGetAttribLocation(m_ProgramID, t_Name);
        Attribute t_Attr = Attribute(t_Name, t_Location, t_Type);

        if (t_Attr.mName == "Position")
        {
            m_HasPositionAttribute = true;
			m_PositionIndex = m_Attributes.GetLength();
        }
        else if (t_Attr.mName == "TexCoord")
        {
            m_HasTexCoordAttribute = true;
			m_TexCoordIndex = m_Attributes.GetLength();
        }
        else if (t_Attr.mName == "Normal")
        {
            m_HasNormalAttribute = true;
            m_NormalIndex = m_Attributes.GetLength();
        }
        else if (t_Attr.mName == "Tangent")
        {
            m_HasTangentAttribute = true;
            m_TangentIndex = m_Attributes.GetLength();
        }

        m_Attributes.AddLast(t_Attr);
    }

	free(t_Name);

}


////////////////////////////////////////////////////////////////////////



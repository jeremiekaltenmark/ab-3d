/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Projet Personnel - Moteur de jeu interne
 *	Organisation: www.jeremkalt.xyz pour CDRIN.
 *
 *	Auteur: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Date cr�ation: 03-03-2014
 *	Refactor: 08-07-2014
 *	
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#ifndef Engine_Shader_H
#define Engine_Shader_H


#include "Common/Logger.h"
#include "Engine/Engine.h"
#include "Components/Component.h"
#include "Common/CommonVector.h"
#include "Components/Material/Program.h"
#include "Components/Material/ShaderWrapper.h"

namespace Engine
{
	class Shader : public Component, public CommonEnableSharedFromThis<Shader>
	{
	public:
		

		Shader();
		Shader(const Shader& aCopy);

		virtual ~Shader();

		template<typename T>
		static CommonSharedPtr<T> LoadShader(const char* a_ShaderPath);
		template<typename T>
		static CommonSharedPtr<T> LoadShader(const CommonString& a_ShaderPath);
		
		bool Build();

		inline Handle GetHandle() const { return m_ShaderID; }
		inline const CommonString& GetName() const { return m_Name; }


	protected:
		bool InternalLoadShader(const CommonString& a_ShaderPath);
		int m_Type;
	private:
		CommonSharedPtr<CommonString> m_ShaderCode;
		CommonString m_Name;
		Handle m_ShaderID;
	};

	class VertexShader : public Shader
	{
	public:
		VertexShader()
		{
			m_Type = VERTEX;
		}
	};

	class FragmentShader : public Shader
	{
	public:
		FragmentShader()
		{
			m_Type = FRAGMENT;
		}
	};

	class GeometryShader : public Shader
	{
	public:
		GeometryShader()
		{
			m_Type = GEOMETRY;
		}
	};

	class TesselationShader : public Shader
	{
	public:
		TesselationShader()
		{
			m_Type = TESSELATION;
		}
	};

	class ComputeShader : public Shader
	{
	public:
		ComputeShader()
		{
			m_Type = COMPUTE;
		}
	};

	////////////////////////////////////////////////////////////////////////
	template<typename T>
	inline CommonSharedPtr<T> Shader::LoadShader(const char* a_ShaderPath)
	{
		return Shader::LoadShader<T>(CommonString(a_ShaderPath));
	}


	////////////////////////////////////////////////////////////////////////
	template<typename T>
	inline CommonSharedPtr<T> Shader::LoadShader(const CommonString& a_ShaderPath)
	{
		CommonSharedPtr<T>  t_Shader = CommonSharedPtr<T>(new T());
		typedef char TemplateNeedsToInheritFromShader[CommonDynamicSharedCast<Shader>(t_Shader) != NULL ? 1 : -1];

		CommonString t_Ext = a_ShaderPath.GetExtension();
		if (t_Ext == "vert")
		 	t_Shader->m_Type = VERTEX;
		else if (t_Ext == "frag")
			t_Shader->m_Type = FRAGMENT;
		else if (t_Ext == "geom")
			t_Shader->m_Type = GEOMETRY;
		else if (t_Ext == "ts")
			t_Shader->m_Type = TESSELATION;
		else if (t_Ext == "cp")
			t_Shader->m_Type = COMPUTE;
		else
		{
			Logger::LogDebugSoft("shader Extension not valide", DEBUG_SOFT_INFO);
			ASSERT(false, "shader Extension not valide");
		}

		t_Shader->InternalLoadShader(a_ShaderPath);
		return t_Shader;

	}

	template<>
	CommonSharedPtr<VertexShader> Shader::LoadShader<VertexShader>(const CommonString& a_ShaderPath);
	template<>
	CommonSharedPtr<FragmentShader> Shader::LoadShader<FragmentShader>(const CommonString& a_ShaderPath);



} // namespace Engine

#endif // Engine_Shader_H


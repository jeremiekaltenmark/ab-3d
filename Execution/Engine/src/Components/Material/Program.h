/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 08-07-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */


#ifndef Engine_Program_H
#define Engine_Program_H


#include "Common/Logger.h"
#include "Engine/Engine.h"
#include "Components/Component.h"
#include "Common/CommonVector.h"
//#include "Components/Material/Shader.h"
#include "Maths/Matrix.h"
#include "ShaderWrapper.h"
#include "Components/ViewMatrix.h"

namespace Engine
{

	class Program : public Component, public CommonEnableSharedFromThis<Program>
	{
	public:
		inline Program();
		inline ~Program();

		void Attach(const CommonSharedPtr<VertexShader>& aVertex, const CommonSharedPtr<FragmentShader>& aFragment);
		void Detach();

		bool Link();

		void RegisterUniforms();
        virtual bool CustomRegister(const Uniform& /*a_Uniform*/) {return false;}
		void RegisterSubroutines(GLenum ShaderType);
        void RegisterAttributes();
        void SetTextureImageUnit();
        void Validate()const;

		void Bind()const;
		void Unbind()const;

		virtual void Update(const CommonSharedPtr<GraphicComponent>& a_Renderable, const Matrix& a_ProjectionMatrix, const ViewMatrix& a_ViewMatrix)const;
		virtual void FrameUpdate(const Matrix& a_ProjectionMatrix, const ViewMatrix& a_ViewMatrix) const;
		virtual void ObjectUpdate() const;
		virtual void InitUpdate() const;
		virtual void PostUpdate()const;
		virtual void UpdateUniform(uint32_t a_UniLocation, const Matrix& aMatUniform)const;
		inline const CommonSharedPtr<Shader>& GetVertexShader()	const { return mVertex; }
		inline const CommonSharedPtr<Shader>& GetFragmentShader()	const { return mFragment; }


		inline GLuint GetGLUniform(const char * aUniform)const;
		inline GLuint GetGLUniform(int aIndexID)const;
		inline const Uniform& GetUniform(const char * aUniform)const;
		inline const Uniform& GetUniform(int aIndexID)const;

		inline GLuint GetGLAttribute(const char * aAttribute)const;
		inline GLuint GetGLAttribute(int aIndexID)const;
		inline const Attribute& GetAttribute(const char * aAttribute)const;
		inline const Attribute& GetAttribute(int aIndexID)const;

		// Warning, currently we only support one type of Subroutine by shader (can be one in fragment, and one in vertex for example)
		inline GLuint GetGLSubroutine(const char * aSubroutine)const ;
		inline GLuint GetGLSubroutine(int aEngineIndexID)const;
		inline const Subroutine& GetSubroutine(const char * aSubroutine)const;
		inline const Subroutine& GetSubroutine(int aEngineIndexID)const;
		inline uint GetSubroutineNbr()const;
		inline void SelectSubroutine(int aEngineIndexID)const;
		inline void SelectSubroutine(const char * aSubroutine)const;
		inline void SelectSubroutine(const Subroutine& aSubroutine)const;

		inline bool HasPositionAttribute()const{ return m_HasPositionAttribute; }
        inline bool HasTexCoordAttribute()const{ return m_HasTexCoordAttribute; }
        inline bool HasNormalAttribute() const{ return m_HasNormalAttribute; }
        inline bool HasTangentAttribute() const{ return m_HasTangentAttribute; }

        inline GLuint GetLocationPositionAttribute()const { return m_Attributes[m_PositionIndex].mLocation; }
        inline GLuint GetLocationTexCoordAttribute()const { return m_Attributes[m_TexCoordIndex].mLocation; }
        inline GLuint GetLocationTangentAttribute()const { return m_Attributes[m_TangentIndex].mLocation; }
		inline GLuint GetLocationNormalAttribute()const { return m_Attributes[m_NormalIndex].mLocation; }

		inline Handle GetHandle() const { return m_ProgramID; }

	protected:
		virtual void InternalInit();
	private:
		CommonSharedPtr<Shader> mVertex;
		CommonSharedPtr<Shader> mFragment;
		CommonVector<Uniform> m_Uniforms;
		CommonVector<Attribute> m_Attributes;
		CommonVector<Subroutine> m_Subroutines;

        bool m_HasMVPUniform;           ui32 m_MVPIndex;
        bool m_HasProjectionUniform;    ui32 m_ProjIndex;
        bool m_HasViewUniform;          ui32 m_ViewIndex;
		bool m_HasWorldUniform;         ui32 m_WorldIndex;
		bool m_HasUniform1;             ui32 m_Uniform1Index;
		bool m_HasCamPos;               ui32 m_CamPosIndex;
		bool m_HasSpecIntens;           ui32 m_SpecIntensIndex;
		bool m_HasSpecPower;            ui32 m_SpecPowerIndex;
        bool m_HasTexture;              ui32 m_TextureIndex;
        bool m_HasShadowTexture;        ui32 m_ShadowTextureIndex;
        bool m_HasNormalTexture;        ui32 m_NormalTextureIndex;

        bool m_HasPositionAttribute;	ui32 m_PositionIndex;
        bool m_HasTexCoordAttribute;	ui32 m_TexCoordIndex;
        bool m_HasNormalAttribute;		ui32 m_NormalIndex	;
        bool m_HasTangentAttribute;		ui32 m_TangentIndex	;

		CommonString m_Name;

		Handle m_ProgramID;
	};
}

extern union Engine::Handle HandleError;
extern struct Engine::Uniform NullUniform;
extern struct Engine::Attribute NullAttribute;
extern struct Engine::Subroutine NullSubroutine;

namespace Engine
{
	inline Program::Program() : m_HasMVPUniform(false), m_HasProjectionUniform(false), m_HasViewUniform(false), m_HasWorldUniform(false), m_HasUniform1(false), m_HasCamPos(false), m_HasSpecIntens(false), m_HasSpecPower(false), m_HasPositionAttribute(false), m_HasNormalAttribute(false), m_HasTangentAttribute(false), m_HasTexCoordAttribute(false), m_HasTexture(false), m_HasShadowTexture(false), m_HasNormalTexture(false), m_Name("")
	{
		m_ProgramID.id = glCreateProgram();
	}
	inline Program::~Program()
	{
		//Logger::LogDebugSoft("~Program", DEBUG_SOFT_INFO);
		glDeleteShader(m_ProgramID);
	}

	inline GLuint Program::GetGLUniform(const char *aUniform)const
	{
		for (CommonVector<Uniform>::const_iterator it = m_Uniforms.Begin(); it != m_Uniforms.End(); ++it)
		{
			if (it->mName == aUniform)
				return it->mLocation;
		}

		Logger::LogDebug("Uniform Error !!", DEBUG_INFO);
		return NullUniform.mLocation;
	}
	inline GLuint Program::GetGLUniform(int aIndexID)const
	{
		return m_Uniforms[aIndexID].mLocation;
	}
	inline const Uniform& Program::GetUniform(int aIndexID)const
	{
		return m_Uniforms[aIndexID];
	}
	inline const Uniform& Program::GetUniform(const char *aUniform)const
	{
		for (uint i = 0; i < m_Uniforms.GetLength(); ++i)
		{
			if (m_Uniforms[i].mName == aUniform)
			{
				return m_Uniforms[i];
			}
		}

        Logger::LogDebug("Uniform Error !!", DEBUG_INFO);
        Logger::Log("\tTrying to get ");
        Logger::Log(aUniform);
        Logger::LogLine(" but there is only : ");
        for (uint i = 0; i < m_Uniforms.GetLength(); ++i)
        {
            Logger::Log("\t\t- ");
            Logger::LogLine(m_Uniforms[i].mName);
        }

		return NullUniform;
	}



	inline GLuint Program::GetGLAttribute(const char *aAttribute)const
	{
		for (CommonVector<Attribute>::const_iterator it = m_Attributes.Begin(); it != m_Attributes.End(); ++it)
		{
			if (it->mName == aAttribute)
				return it->mLocation;
		}

		Logger::LogDebug("Attribute Error !!", DEBUG_INFO);
		return NullAttribute.mLocation;
	}
	inline GLuint Program::GetGLAttribute(int aIndexID)const
	{
		return m_Attributes[aIndexID].mLocation;
	}
	inline const Attribute& Program::GetAttribute(int aIndexID)const
	{
		return m_Attributes[aIndexID];
	}
	inline const Attribute& Program::GetAttribute(const char *aAttribute)const
	{
		for (uint i = 0; i < m_Attributes.GetLength(); ++i)
		{
			if (m_Attributes[i].mName == aAttribute)
			{
				return m_Attributes[i];
			}
		}

		Logger::LogDebug("Attribute Error !!", DEBUG_INFO);
		return NullAttribute;
	}




	inline GLuint Program::GetGLSubroutine(const char *aSubroutine)const 
	{
		for (CommonVector<Subroutine>::const_iterator it = m_Subroutines.Begin(); it != m_Subroutines.End(); ++it)
		{
			if (it->mName == aSubroutine)
				return it->mIndex;
		}

		Logger::LogDebug("Subroutine Error !!", DEBUG_INFO);
		return NullSubroutine.mIndex;
	}
	inline GLuint Program::GetGLSubroutine(int aEngineIndexID)const
	{
		return m_Subroutines[aEngineIndexID].mIndex;
	}
	inline const Subroutine& Program::GetSubroutine(int aEngineIndexID)const
	{
		return m_Subroutines[aEngineIndexID];
	}
	inline const Subroutine& Program::GetSubroutine(const char *aSubroutine)const 
	{
		for (uint i = 0; i < m_Attributes.GetLength(); ++i)
		{
			if (m_Subroutines[i].mName == aSubroutine)
			{
				return m_Subroutines[i];
			}
		}

		Logger::LogDebug("Subroutine Error !!", DEBUG_INFO);
		return NullSubroutine;
	}
	uint Engine::Program::GetSubroutineNbr()const
	{
		return m_Subroutines.GetLength();
	}

	inline void Engine::Program::SelectSubroutine(int aEngineIndexID)const
	{
		const Subroutine& tSub = m_Subroutines[aEngineIndexID];
		SelectSubroutine(tSub);
	}
	inline void Engine::Program::SelectSubroutine(const char * aSubroutine)const
	{
		const Subroutine& tSub = GetSubroutine(aSubroutine);
		SelectSubroutine(tSub);
	}

	// Warning, currently we only support one type of Subroutine by shader (can be one in fragment, and one in vertex for example)
	inline void Engine::Program::SelectSubroutine(const Subroutine& aSubroutine)const
	{
		glUniformSubroutinesuiv(aSubroutine.mGLShadertype, 1, &aSubroutine.mIndex);
	}
}

#endif // Engine_Program_H
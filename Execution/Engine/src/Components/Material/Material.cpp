/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 08-04-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#include "Engine/Engine.h"
#include "Components/Material/Material.h"
#include "Components/Material/Program.h"
#include "Components/Material/Texture.h"

using namespace Engine;


CommonSharedPtr<const Engine::Material> NullMaterial;


////////////////////////////////////////////////////////////////////////
Material::Material() : m_ProgramShader(NULL), m_Texture(), m_SpecPower(32), m_MatSpecIntens(1.0f)
{
    mComponents[&typeid(Program)] = m_ProgramShader;
}

////////////////////////////////////////////////////////////////////////
Material::Material(const Material& aCopy)
{
//    Logger::LogDebug("implement me", DEBUG_INFO);
//	(void) aCopy;
    m_ProgramShader = aCopy.m_ProgramShader;
    mComponents[&typeid(Program)] = m_ProgramShader;
	m_Texture[0] = aCopy.m_Texture[0];
	m_Texture[1] = aCopy.m_Texture[1];
	m_Texture[2] = aCopy.m_Texture[2];
    m_MatSpecIntens = aCopy.m_MatSpecIntens;
    m_SpecPower = aCopy.m_SpecPower;

}

////////////////////////////////////////////////////////////////////////
Material::~Material()
{
    //Logger::LogDebugSoft("~Material", DEBUG_SOFT_INFO);
}

Handle Material::SetShader(GraphicDevice::PROGRAMS a_ProgID)
{
    m_ProgramShader = GraphicDevice::GetSingleton()->GetShaderProgram(a_ProgID);
    mComponents[&typeid(Program)] = m_ProgramShader;
	return m_ProgramShader->GetHandle();
}

void Material::SetShader(CommonSharedPtr<Program>& a_ValidProgram)
{
    m_ProgramShader = a_ValidProgram;
    mComponents[&typeid(Program)] = m_ProgramShader;
}

void Engine::Material::Bind(CommonSharedPtr<const Material>& a_PreviousMaterial)const
{
	BindProgram(a_PreviousMaterial);
	BindTextures(a_PreviousMaterial);
}

void Engine::Material::BindProgram(CommonSharedPtr<const Material>& a_PreviousMaterial)const
{
	if (a_PreviousMaterial == NULL || a_PreviousMaterial->m_ProgramShader->GetHandle().id != m_ProgramShader->GetHandle().id)
	{
		if (m_ProgramShader != NULL)
		{
			if (a_PreviousMaterial != NULL)
				a_PreviousMaterial->m_ProgramShader->PostUpdate();

			m_ProgramShader->Bind();
		}
	}
}

void Engine::Material::BindTextures(CommonSharedPtr<const Material>& a_PreviousMaterial)const
{
	int i = 0;
	for (CommonSharedPtr<Texture> t_tex : m_Texture)
	{
		if (t_tex != NULL && (a_PreviousMaterial == NULL || (a_PreviousMaterial->m_Texture[TEXTURES(i)] != t_tex)))
		{
            t_tex->Bind(GL_TEXTURE0 + i);
		}
		++i;
	}
}

void Engine::Material::BindTexture(TEXTURES a_texType, CommonSharedPtr<const Material>& a_PreviousMaterial /*= NullMaterial*/)const
{
	CommonSharedPtr<Texture> t_tex = m_Texture[a_texType];
	if (t_tex != NULL && (a_PreviousMaterial == NULL || (a_PreviousMaterial->m_Texture[a_texType] != t_tex)))
	{
		t_tex->Bind(GL_TEXTURE0 + static_cast<int>(a_texType));
	}
}

void Engine::Material::Bind(CommonSharedPtr<Material>& a_PreviousMaterial)const
{
	BindProgram(a_PreviousMaterial);
	BindTextures(a_PreviousMaterial);
}

void Engine::Material::BindProgram(CommonSharedPtr<Material>& a_PreviousMaterial)const
{
	if (a_PreviousMaterial == NULL || a_PreviousMaterial->m_ProgramShader->GetHandle().id != m_ProgramShader->GetHandle().id)
	{
		if (m_ProgramShader != NULL)
		{
			if (a_PreviousMaterial != NULL)
				a_PreviousMaterial->m_ProgramShader->PostUpdate();

			m_ProgramShader->Bind();
		}
	}
}

void Engine::Material::BindTextures(CommonSharedPtr<Material>& a_PreviousMaterial)const
{
	int i = 0;
	for (CommonSharedPtr<Texture> t_tex : m_Texture)
	{
		if (a_PreviousMaterial == NULL || (t_tex != NULL && a_PreviousMaterial->m_Texture[TEXTURES(i)] != t_tex))
		{
			if (t_tex != NULL)
				t_tex->Bind(GL_TEXTURE0 + i);
		}
		++i;
	}
	//	if (a_PreviousMaterial == NULL || (m_Texture != NULL && a_PreviousMaterial->m_Texture != m_Texture))
	//	{
	//		if (m_Texture != NULL)
	//			m_Texture->Bind(GL_TEXTURE0);
	//	}
}

void Engine::Material::BindTexture(TEXTURES a_texType, CommonSharedPtr<Material>& a_PreviousMaterial /*= NullMaterial*/)const
{
	CommonSharedPtr<Texture> t_tex = m_Texture[a_texType];
	if (t_tex != NULL && (a_PreviousMaterial == NULL || (a_PreviousMaterial->m_Texture[a_texType] != t_tex)))
	{
		t_tex->Bind(GL_TEXTURE0 + static_cast<int>(a_texType));
	}
}

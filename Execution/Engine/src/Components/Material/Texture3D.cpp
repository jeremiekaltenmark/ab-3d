/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *    Project MoteurCDRIN - internal Game Engine
 *    Organisation: www.jeremkalt.xyz
 *
 *    Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *    Created on: 24-03-2016
 *
 *    Copyright (c) 2016 CDRIN. All rights reserved.
 */


#include "Engine/Engine.h"
#include "Components/Material/Texture3D.h"

void Engine::Texture3D::GenerateGLTexture(GLenum a_TextureTarget, GLint a_MipMapLevel, unsigned int t_Width, unsigned int t_Height, GLint a_Border, BYTE* t_Pixels)
{
	(void)a_TextureTarget;
	(void)a_MipMapLevel;
	(void)t_Width;
	(void)t_Height;
	(void)a_Border;
	(void)t_Pixels;
}

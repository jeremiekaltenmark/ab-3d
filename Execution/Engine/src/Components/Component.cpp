/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 21-02-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#include "Engine/Engine.h"
#include "Component.h"
#include "Common/Logger.h"

//namespace Engine
//{

using Engine::Component;

////////////////////////////////////////////////////////////////////////
Component::Component() : m_IsGraphic(false)
{

}

////////////////////////////////////////////////////////////////////////
Component::Component(const Component& aCopy)
{
    Logger::LogDebug("implement me", DEBUG_INFO);
	(void) aCopy;
}

////////////////////////////////////////////////////////////////////////
Component::~Component()
{
// mComponents is now from construct from shared_ptr
//     ArrayComponents::iterator it; 
//     for (it = mComponents.begin(); it != mComponents.end(); ++it)
//     {
//         DELETE_OBJECT( (*it) );
//     }
	//Logger::LogDebugSoft("~Component", DEBUG_SOFT_INFO);
    mComponents.clear();
}

////////////////////////////////////////////////////////////////////////
void Component::Update(float aElapsedTime)
{
	(void) aElapsedTime;
    Logger::Printf("Implement me ! \n\tmethod: %s \n\tline: %d \n\tfile: %s \n", __func__, __LINE__, __FILE__);
}
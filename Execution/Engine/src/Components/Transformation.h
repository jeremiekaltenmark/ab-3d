/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 03-04-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#ifndef Engine_Transformation_H
#define Engine_Transformation_H

#include "Components/NodeComponent.h"
#include "Maths/Vec3.h"
#include "Maths/Quaternion.h"

namespace Engine
{

    class Transformation : public NodeComponent
    {
    public:
        Transformation();
        Transformation(const Transformation& aCopy);

        virtual ~Transformation();

        inline void SetPosition (const Vec3& aVec)          { mPos = aVec;       mDirty = true; }
        inline void SetScale    (const Vec3& aVec)          { mScale = aVec;     mDirty = true; }
        inline void SetScale    (float a, float b, float c) { mScale.Set(a,b,c); mDirty = true; }
        inline void SetRotation (const Quaternion& aQuat)   { mRot = aQuat;      mDirty = true; }
        inline Transformation& operator=(const Transformation& aRef);

        inline Vec3&              GetPosition()             {return mPos;   }
        inline Vec3&              GetScale( )               {return mScale; }
        inline Quaternion&        GetRotation( )            {return mRot;   }
        inline const Vec3&        GetPosition() const       {return mPos;   }
        inline const Vec3&        GetScale( )   const       {return mScale; }
        inline const Quaternion&  GetRotation() const       {return mRot;   }

        inline void Move(const Vec3& aVec) {mPos += aVec; mDirty = true;}
        inline void Move(const float aX,const float aY, const float aZ) {mPos.SelfTranslate(aX,aY,aZ); mDirty = true;}
        inline void Rotate(const Quaternion& aQuad) { mRot.SelfMult(aQuad); mDirty = true;}


        const Matrix& GetConstMatrixTransform () ;
        Matrix GetCopyMatrixTransform () ;

    private:
        Vec3 mPos;
        Vec3 mScale;
        Quaternion mRot;
        Matrix mTransformMat;
        
        bool mDirty;
    };


    inline Transformation& Transformation::operator=(const Engine::Transformation &aRef)
    {
        mPos = aRef.mPos;
        mScale = aRef.mScale;
        mRot = aRef.mRot;
        mDirty = true;

        return *this;
    }

}
#endif // Engine_Transformation_H

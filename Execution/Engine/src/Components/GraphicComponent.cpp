/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 03-04-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */


#include "Engine/Engine.h"
#include "GraphicComponent.h"
#include "IO/Viewport.h"
#include "IO/Camera.h"
#include "Rendering/Renderer.h"
#include "Rendering/RenderVisitor.h"

using namespace Engine;


////////////////////////////////////////////////////////////////////////
GraphicComponent::GraphicComponent() : mTransform(new Transformation()), m_Material(), m_Parent(nullptr)
{
    //    m_type = GRAPHIC;
    mComponents[&typeid(Transformation)] = mTransform;
    mComponents[&typeid(Material)] = m_Material;
    m_NbIBOIndices = 0;
	//m_Material->SetShader(Renderer::PROGRAM_SIMPLE_3D);
	glGenBuffers(1, &m_VBO);
	glGenBuffers(1, &m_IBO);
	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);

	m_IsGraphic = true;
}

////////////////////////////////////////////////////////////////////////
GraphicComponent::~GraphicComponent()
{
	m_Material.reset(); // not required, but easier to debug if an error occurs in destructor
	mTransform.reset(); // not required, but easier to debug if an error occurs in destructor
	glDeleteBuffers(1, &m_VBO);
	glDeleteBuffers(1, &m_IBO);
	glDeleteVertexArrays(1, &m_VAO);
}

////////////////////////////////////////////////////////////////////////
void GraphicComponent::Draw(CommonSharedPtr<Material> a_Mat)
{
	//ASSERT(m_IBO != INVALID_OGL_VALUE, "if no indexed draw, reimplement this method")

	//glDisable(GL_CULL_FACE); // draw each face back and front


	if (m_VBO != INVALID_OGL_VALUE)
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);

		size_t TexOffset = sizeof(Vec3);
		ASSERT(TexOffset == 12, "wrong Vec3 size");

        size_t NormalOffset = TexOffset + sizeof(Vec2);
        ASSERT(NormalOffset == 20, "wrong normal offset");

        size_t TangentOffset = NormalOffset + sizeof(Vec3);
        ASSERT(TangentOffset == 32, "wrong tangent offset");

		if (a_Mat == NULL)
		{
			a_Mat = GetMaterial();
		}

		ASSERT(a_Mat != NULL, "no material assigned but VBO correct ?? ");
		//glVertexAttribPointer (Shaderlayout, size, type, normalized?, stride, arrayOffset (void*));
		CommonSharedPtr<Program>& tshader = a_Mat->GetShader();

		if (tshader->HasPositionAttribute())
		{
			glVertexAttribPointer(tshader->GetLocationPositionAttribute(), 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0); // vertices, with offset winthin each
		}
		else
		{
			TexOffset -= 12;
            NormalOffset -= 12;
            TangentOffset -= 12;
		}

		if (tshader->HasTexCoordAttribute())
		{
			glVertexAttribPointer(tshader->GetLocationTexCoordAttribute(), 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*) TexOffset); // tex coord
		}
		else
		{
            NormalOffset -= 8;
            TangentOffset -= 8;
		}

		if (tshader->HasNormalAttribute())
		{
			glVertexAttribPointer(tshader->GetLocationNormalAttribute(), 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*) NormalOffset); // normals coord
		}
        else
        {
            TangentOffset -= 12;
        }

        if (tshader->HasTangentAttribute())
        {
            glVertexAttribPointer(tshader->GetLocationTangentAttribute(), 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*) TangentOffset); // normals coord
        }
        

	}

	if (m_IBO != INVALID_OGL_VALUE)
	{
		// Draw the triangles !
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
		glDrawElements(GL_TRIANGLES, m_NbIBOIndices, GL_UNSIGNED_INT, 0); // 6 faces * 2 tri by face = 12 tri * 3 vertice = 36 points
	}
	
	//glEnable(GL_CULL_FACE);

}

////////////////////////////////////////////////////////////////////////
void Engine::GraphicComponent::Draw(RendererVisitor* a_Visitor)
{
	CommonSharedPtr<Engine::GraphicComponent> t = shared_from_this();
	a_Visitor->Render(t);
}

////////////////////////////////////////////////////////////////////////
void GraphicComponent::Init(CommonVector<Vertex>& Vertices, const CommonVector<ui32>& Indices)
{
	m_NbIBOIndices = Indices.GetLength();

    if(m_VAO == INVALID_OGL_VALUE)
    {
        glGenVertexArrays(1, &m_VAO);
        glBindVertexArray(m_VAO);
    }

    if(m_VBO == INVALID_OGL_VALUE)
        glGenBuffers(1, &m_VBO);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * Vertices.GetLength(), &Vertices[0], GL_STATIC_DRAW);

    if(m_IBO == INVALID_OGL_VALUE)
        glGenBuffers(1, &m_IBO);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(ui32) * m_NbIBOIndices, &Indices[0], GL_STATIC_DRAW);
}

Matrix Engine::GraphicComponent::GetGlobalModelMatrix() const
{
	if (m_Parent != nullptr)
	{
		return m_Parent->GetGlobalModelMatrix() * mTransform->GetConstMatrixTransform();
	}
	else
		return mTransform->GetConstMatrixTransform(); // will return a copy ;)

}



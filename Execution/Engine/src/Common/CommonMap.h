/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 18-06-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */


#ifndef Engine_Commonmap_h
#define Engine_Commonmap_h

#include "Engine/EngineType.h"
#include <map>

namespace Engine {



template <typename KEY_TYPE, typename VALUE_TYPE>
class CommonMap : private std::map<KEY_TYPE, VALUE_TYPE>
{
private:
	typedef std::map<KEY_TYPE, VALUE_TYPE> stlType;

public:
	typedef typename stlType::iterator iterator;
	typedef typename stlType::const_iterator const_iterator;
	typedef typename stlType::reverse_iterator reverse_iterator;
	typedef typename stlType::const_reverse_iterator const_reverse_iterator;
    typedef typename stlType::const_reference const_reference;

public:
    CommonMap                          ()									{}
    ~CommonMap                         ()									{}

	ulong					GetLength	()									{ return stlType::size(); }
	bool					IsEmpty		()									{ return stlType::empty(); }


	void					Add(const KEY_TYPE& a_Key, const VALUE_TYPE& a_Val) { stlType::operator[](a_Key) = a_Val; }
	void					Add(std::pair<KEY_TYPE, VALUE_TYPE>& a_kvp)	{ insert(a_kvp); }

	void					clear()											{ stlType::clear(); }

	void					deletePtrAndClear()
    {
        for (iterator it = stlType::begin(); it != stlType::end(); ++it)
        {
			DELETE_OBJECT((*it))
        }
        stlType::clear();
    }


	VALUE_TYPE&				operator[](const KEY_TYPE& a_Key) { return stlType::operator[](a_Key); }
	//const VALUE_TYPE&		operator[](int a_Key) const { return stlType:::operator[a_Key]; }

	VALUE_TYPE&				getFirst()										{ return stlType::begin()->first; }
	const VALUE_TYPE&		getFirst() const								{ return stlType::begin()->first; }
	VALUE_TYPE&				getLast()										{ return stlType::rbegin()->first; }
	const VALUE_TYPE&		getLast() const									{ return stlType::rbegin()->first; }

	void					remove		(const KEY_TYPE& a_Key)				{ stlType::erase(a_Key); }
 	void					remove		( const iterator& a_it )			{ stlType::erase( a_it ); }

	iterator				begin		()									{ return stlType::begin(); }
	const_iterator			begin		() const							{ return stlType::begin(); }
	iterator				end			()									{ return stlType::end(); }
	const_iterator			end			() const							{ return stlType::end(); }

	reverse_iterator		rbegin		()									{ return stlType::rbegin(); }
	const_reverse_iterator	rbegin		() const							{ return stlType::rbegin(); }
	reverse_iterator		rend		()									{ return stlType::rend(); }
	const_reverse_iterator	rend		() const							{ return stlType::rend(); }

};

}

#endif

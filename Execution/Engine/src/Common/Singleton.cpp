/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 27-02-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#include "Engine/Engine.h"
#include "Singleton.h"



template <typename T> T* Engine::Singleton<T>::mSingleton = NULL;

using Engine::Singleton;


////////////////////////////////////////////////////////////////////////
template <typename T>
Singleton<T>::Singleton()
{

}

////////////////////////////////////////////////////////////////////////
template <typename T>
Singleton<T>::Singleton(const Singleton<T>& aCopy)
{
    (void) aCopy;
}

////////////////////////////////////////////////////////////////////////
template <typename T>
Singleton<T>::~Singleton()
{
	//Logger::LogDebugSoft("Destroy Singleton", DEBUG_SOFT_INFO);
}

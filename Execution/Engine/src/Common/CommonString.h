/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 03-03-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#ifndef Engine_CommonString_H
#define Engine_CommonString_H

#include <iostream>
#include <string>
#include "Common/CommonVector.h"
#include "Maths/Vec3.h"


namespace Engine
{

class CommonString
{
#define STRING_ERROR -1 // std::npos = -1 but warning level 4 treat this as warning

public:
	CommonString() {}
	CommonString(const char* aStr) : mString(aStr) {}
	CommonString(const CommonVector<char>& aVec)
	{
        CommonVector<char>::const_iterator it;
		for (it = aVec.Begin(); it != aVec.End(); ++it)
		{
			mString += *it;
		}
	}

    CommonString(const std::string& aStr)
    {
        mString = aStr;
    }
    CommonString(const Vec3& aVec) : mString("{")
    {
        mString += std::to_string(aVec.X());
        mString += ", ";
        mString += std::to_string(aVec.Y());
        mString += ", ";
        mString += std::to_string(aVec.Z());
        mString += "}";
    }

	CommonString (const CommonString& str) : mString(str.mString) {}
	CommonString (const CommonString& str, size_t pos, size_t len = std::string::npos) : mString(str.mString, pos, len) {}
	CommonString (const char* s, size_t n) : mString(s, n) {}
	CommonString (size_t n, char c) : mString(n,c) {}

	CommonString& operator= (const CommonString& str) { mString = str.mString;  return *this; }
	CommonString& operator= (const char* s) {mString = s;  return *this; }
	CommonString& operator= (char c) { mString = c; return *this; }
	
	CommonString operator+ (const CommonString& str)	const { return CommonString(mString + str.mString); }
	CommonString operator+ (const char* str)			const { return CommonString(mString + str); }
	CommonString operator+ (const char str)				const { return CommonString(mString + str); }


	CommonString& operator+= (const CommonString& str) { mString += str.mString;  return *this; }
	CommonString& operator+= (const char* s) {mString += s;  return *this; }
	CommonString& operator+= (char c) { mString += c;  return *this; }
	CommonString& operator+= (int i) { mString += std::to_string(i);  return *this; }
	CommonString& operator+= (long i) { mString += std::to_string(i);  return *this; }
	CommonString& operator+= (float i) { mString += std::to_string(i);  return *this; }
	CommonString& operator+= (double i) { mString += std::to_string(i);  return *this; }

	bool operator!= (const CommonString& str) const { return mString.compare(str.mString) != 0; }
	bool operator!= (const char* s) const { return mString.compare(s) != 0; }
	bool operator!= (const std::string s) const { return mString.compare(s) != 0; }

	bool operator== (const CommonString& str) const { return mString.compare(str.mString) == 0; }
	bool operator== (const char* s) const { return mString.compare(s) == 0; }
	bool operator== (const std::string s) const { return mString.compare(s) == 0; }


	bool operator< (const CommonString& c) const { return mString < c.mString; }

	const std::string& GetStdString() const { return mString; }
	std::string& GetStdString() { return mString; }
	const char* GetChars() const { return mString.c_str(); }

	CommonString SubString(size_t pos = 0, size_t len = std::string::npos) const  { return CommonString(mString.substr(pos,len)); }
	uint FindLastOf(const char* a_str) const { return          static_cast<uint>(mString.find_last_of(a_str)); }
	uint FindLastOf(const CommonString& a_str) const{ return    static_cast<uint>(mString.find_last_of(a_str.mString)); }


	CommonString GetExtension() const
	{
		ulong pos = mString.find_last_of('.');
		if (pos != std::string::npos)
		{
			return CommonString(mString.substr(pos + 1));
		}
		else
			return CommonString();
	}
	CommonString ExcludeExtension() const
	{
		ulong pos = mString.find_last_of('.');
		if (pos != std::string::npos)
		{
			return CommonString(mString.substr(0, pos));
		}
		else
			return CommonString();
	}
	void ConvertSlash()
	{
		for (std::string::iterator it = mString.begin();
			it != mString.end();
			++it)
		{
			if (*it == '\\')
				*it = '/';
		}
	}

	int GetLength()
	{
		return static_cast<int>(mString.length());
	}

private:
	std::string mString;

};

}
#endif // Engine_CommonString_H

/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 27-02-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */


#ifndef EngineSingleton_H
#define EngineSingleton_H

namespace Engine
{

    template <class T>
    class Singleton
    {
    private:
        static T* mSingleton;

    protected:
        Singleton();
        Singleton(const Singleton<T>& aCopy);
        virtual ~Singleton();

    public:

        static inline T* GetSingleton();
        static inline void DeleteSingleton();
    };

} // end Engine


////////////////////////////////////////////////////////////////////////
template<typename T>
inline T* Engine::Singleton<T>::GetSingleton()
{
    if (mSingleton == NULL)
    {
        mSingleton = new T();
    }
    return mSingleton;
}


////////////////////////////////////////////////////////////////////////
template<typename T>
inline void Engine::Singleton<T>::DeleteSingleton()
{
    if (mSingleton != NULL)
    {
        delete mSingleton;
        mSingleton = NULL;
    }
}



#endif // Engine_Singleton_H


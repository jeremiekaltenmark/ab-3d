/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 03-03-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */


#include <vector>

#ifndef Engine_CommonVector_h
#define Engine_CommonVector_h

namespace Engine {



template <typename TYPE>
class CommonVector : private std::vector<TYPE>
{
private:
	typedef std::vector<TYPE> stlType;

public:
	typedef typename stlType::iterator				iterator;
	typedef typename stlType::const_iterator			const_iterator;
	typedef typename stlType::reverse_iterator		reverse_iterator;
	typedef typename stlType::const_reverse_iterator const_reverse_iterator;

public:
	CommonVector						()									{}
	CommonVector						(int a) : stlType(a)				{}
	~CommonVector						()									{}

	uint					GetLength	() const							{ return static_cast<uint>(stlType::size()); }
	bool					IsEmpty		() const							{ return stlType::empty(); }

	void					AddLast		(TYPE& aRef)						{ stlType::push_back(aRef); }
	void					Add			(const iterator& aIt, TYPE& aRef)	{ stlType::insert(aIt, aRef); }
	void					AddSort		(TYPE& aRef)
	{
		iterator i = Begin();
		while ( i < End() && i < aRef) {++i; }
		Add(i, aRef);
	}


	void					Clear		()									{ std::vector<TYPE>::clear(); }

	TYPE&					GetFirst	()									{ return stlType::front(); }
	const TYPE&				GetFirst	() const							{ return stlType::front(); }
	TYPE&					GetLast		()									{ return stlType::back(); }
	const TYPE&				GetLast		() const							{ return stlType::back(); }

	void					RemoveFirst	()									{ stlType::pop_front(); }
	void					RemoveLast	()									{ stlType::pop_back(); }
	void					Remove		( const TYPE& _elmt )				{ stlType::remove( _elmt ); }
	void					Remove		( const iterator& _it )				{ stlType::erase( _it ); }

	iterator				Begin() { return stlType::begin(); }
	const_iterator			Begin() const { return stlType::begin(); }

	iterator				End() { return stlType::end(); }
	const_iterator			End() const { return stlType::end(); }

	reverse_iterator		RBegin() { return stlType::rbegin(); }
	const_reverse_iterator	RBegin() const { return stlType::rbegin(); }

	reverse_iterator		REnd		()									{ return stlType::rend(); }
	const_reverse_iterator	REnd		() const							{ return stlType::rend(); }

	void					Sort		( bool (*_fn)( TYPE, TYPE ) )		{ return stlType::sort( _fn ); }

	void					Resize		(ui32 a_NewSize)					{ return stlType::resize(a_NewSize); }

	TYPE& operator[] (size_t n) { return stlType::operator [](n); }
	const TYPE& operator[] (size_t n) const { return stlType::operator [](n); }
};

}
#endif

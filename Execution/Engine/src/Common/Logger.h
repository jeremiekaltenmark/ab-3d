/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 07-03-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#ifndef Engine_Logger_h
#define Engine_Logger_h

#include <iostream>
#include <cstdio>
#include <cstdarg>
#include <string>

#include "Engine/EngineType.h"
#include "Common/CommonString.h"

namespace Engine
{
class Logger
{
public:
	static inline void Log(const char* aStr) { std::cout << aStr; }
	static inline void Log(CommonString aStr) { std::cout << aStr.GetChars(); }
	static inline void LogLine(const char* aStr) { std::cout << aStr << std::endl; }
	static inline void LogLine(CommonString aStr) { std::cout << aStr.GetChars() << std::endl; }


    static inline void LogDebug(const char* aStr, const char* aFileName, int aLine, const char* aMethod)
    {
        std::string t = aFileName;
        t = t.substr(t.find_last_of("/") +1);

		std::cout << "---" << std::endl;
		std::cout << "\tMessage: " << aStr << std::endl;
        std::cout << "\tin File: " << t << std::endl;
        std::cout << "\tat line: " << aLine << std::endl;
        std::cout << "\tin Method: " << aMethod << std::endl;
    }
    static inline void LogDebug(const CommonString& aStr, const char* aFileName, int aLine, const char* aMethod)
    {

        std::string t = aFileName;
        t = t.substr(t.find_last_of("/") +1 );

		std::cout << "---" << std::endl;
        std::cout << "\tMessage: " << aStr.GetChars() << std::endl;
		std::cout << "\tin File: " << t << std::endl;
        std::cout << "\tat line: " << aLine << std::endl;
        std::cout << "\tin Method: " << aMethod << std::endl;
    }

    static inline void LogDebugSoft(const char* aStr, const char* aFileName, int aLine)
    {
        std::string t = aFileName;
        t = t.substr(t.find_last_of("/") +1);

		std::cout << aStr << std::endl << " - " << t << ", " << aLine << std::endl;
    }
    static inline void LogDebugSoft(const CommonString& aStr, const char* aFileName, int aLine)
    {
        std::string t = aFileName;
        t = t.substr(t.find_last_of("/") +1);

		std::cout << aStr.GetChars() << std::endl << " - " << t << ", " << aLine << std::endl;
    }

	static inline void  Printf ( const char * format, ... )
	{
		va_list arglist;
		va_start(arglist, format);
		vprintf(format, arglist);
		va_end( arglist );
	}

private:
	Logger() {}
	~Logger() { Logger::LogDebugSoft("Destroy Logger", DEBUG_SOFT_INFO); }
};


#define LogDebugInfo(a) Logger::LogDebug((a), DEBUG_INFO)
}


#endif

/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: Jérémie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 03-03-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */


#ifndef Engine_CommonList_h
#define Engine_CommonList_h

#include "Engine/EngineType.h"
#include <list>

namespace Engine {



template <typename TYPE>
class CommonList : private std::list<TYPE>
{
private:
	typedef std::list<TYPE> stlType;

public:
	typedef typename stlType::iterator iterator;
	typedef typename stlType::const_iterator const_iterator;
	typedef typename stlType::reverse_iterator reverse_iterator;
	typedef typename stlType::const_reverse_iterator const_reverse_iterator;
    typedef typename stlType::const_reference const_reference;

public:
    CommonList                          ()									{}
    ~CommonList                         ()									{}

	ulong					GetLength	()									{ return stlType::size(); }
	bool					IsEmpty		()									{ return stlType::empty(); }

	void					AddLast		(TYPE& aRef)						{ stlType::push_back(aRef); }
	void					AddLast		(const TYPE& aRef)					{ stlType::push_back(aRef); }
	void					Add			(const iterator& aIt, TYPE& aRef)	{ stlType::insert(aIt, aRef); }
	void					AddFirst	(TYPE& aRef)						{ stlType::push_front(aRef); }
	void					AddSort		(TYPE& aRef)
	{
		iterator i = begin();
		while ( i < end() && i < aRef) {++i; }
		Add(i, aRef);
	}


	void					clear		()									{ std::list<TYPE>::clear(); }

	void					deletePtrAndClear()
    {
        for (iterator it = stlType::begin(); it != stlType::end(); ++it)
        {
			DELETE_OBJECT((*it))
//             delete *it;
//             *it = NULL;
        }
        stlType::clear();
    }

	TYPE&					getFirst	()									{ return stlType::front(); }
	const TYPE&				getFirst	() const							{ return stlType::front(); }
	TYPE&					getLast		()									{ return stlType::back(); }
	const TYPE&				getLast		() const							{ return stlType::back(); }

	void					removeFirst	()									{ stlType::pop_front(); }
	void					removeLast	()									{ stlType::pop_back(); }
	void					remove		( const TYPE& _elmt )				{ stlType::remove( _elmt ); }
	void					remove		( const iterator& _it )				{ stlType::erase( _it ); }

	iterator				begin		()									{ return stlType::begin(); }
	const_iterator			begin		() const							{ return stlType::begin(); }

	iterator				end			()									{ return stlType::end(); }
	const_iterator			end			() const							{ return stlType::end(); }

	reverse_iterator		rbegin		()									{ return stlType::rbegin(); }
	const_reverse_iterator	rbegin		() const							{ return stlType::rbegin(); }

	reverse_iterator		rend		()									{ return stlType::rend(); }
	const_reverse_iterator	rend		() const							{ return stlType::rend(); }

	void					sort		( bool (*_fn)( TYPE, TYPE ) )		{ return stlType::sort( _fn ); }

	TYPE&					operator[] (ui32 a_Index) 
	{
		ui32 cpt = 0;
		for (iterator it = begin(); it != end(); ++it, ++cpt)
		{
			if (cpt == a_Index)
				return *it;
		}

		return *end(); // generate error at runtime if trying to access element not in list
	}
};

}

#endif

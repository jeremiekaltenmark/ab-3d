/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: Jérémie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 03-03-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#ifndef Engine_CommonFile_H
#define Engine_CommonFile_H

#include <iostream>
#include <fstream>
#include "CommonString.h"

namespace Engine
{

    class CommonFile
    {
    public:
        CommonFile( const CommonString& _fileName )
        {
            mStream.open(_fileName.GetChars());
        }

        explicit CommonFile(CommonFile& aRef)
        {
            mStream.swap(aRef.mStream);
        }

        ~CommonFile()
        {
//			Logger::LogDebugSoft("Destroy file", DEBUG_SOFT_INFO);
            if (mStream.is_open())
                mStream.close();
        }

        bool IsOpen()
        {
            return mStream.is_open();
        }

        bool GetLine(CommonString& aStr)
        {
            getline(mStream, aStr.GetStdString());
            return !mStream.eof();
        }

		CommonFile& operator<<(CommonString& aIn)
		{
			this->mStream << aIn.GetStdString();
			return *this;
		}
		CommonFile& operator<<(const char* aIn)
		{
			this->mStream << aIn;
			return *this;
		}

		std::fstream& GetStream() { return mStream; }

    private:
        std::fstream mStream;
        
    };
    
}
#endif // Engine_CommonFile_H

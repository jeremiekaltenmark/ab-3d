/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 28-05-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#include "Engine/Engine.h"
#include "FileSystem.h"

using namespace Engine;


////////////////////////////////////////////////////////////////////////
FileSystem::FileSystem()
{

}

////////////////////////////////////////////////////////////////////////
FileSystem::FileSystem(const FileSystem& aCopy)
{
	(void) aCopy;
}

////////////////////////////////////////////////////////////////////////
FileSystem::~FileSystem()
{
	//Logger::LogDebugSoft("~FileSystem", DEBUG_SOFT_INFO);
}

///////////////////// static ///////////////////////////////////////////
CommonSharedPtr<CommonFile> FileSystem::Load(const CommonString a_Str)
{
	CommonSharedPtr<CommonFile> t (new CommonFile(a_Str));
	return t;
}

///////////////////// static ///////////////////////////////////////////
CommonSharedPtr<CommonString> FileSystem::LoadAsString(const CommonString a_Str)
{
	CommonFile t(a_Str);
	if (t.IsOpen())
	{
		CommonSharedPtr<CommonString> tRet (new CommonString());
		CommonString Line = "";
		while (t.GetLine(Line))
		{
			(*tRet) += "\n";
			(*tRet) += Line;
		}
		return tRet;
	}
	else
		return NULL;
}


///////////////////// static ///////////////////////////////////////////
//void FileSystem::LoadAsync(CommonString aPath, fastdelegate::FastDelegate<CommonFile>& aDelegate)
//{
//
//}
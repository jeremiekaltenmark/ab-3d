/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 06-03-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#include "Engine/Engine.h"
#include "Rendering/Renderer.h"

#ifdef USING_GLFW

#include "Engine/Application.h"
#include "GLFWOutput.h"
#include "IO/IOManager.h"

using namespace Engine;

void KeyCallback(GLFWwindow* aWindow, int key, int scancode, int action, int mods)
{
	CommonSharedPtr<GLFWOutput> tOut = CommonDynamicSharedCast<GLFWOutput>(Application::GetSingleton()->GetIOManager()->GetOutput(aWindow));

    tOut->ProcessInputs(aWindow, key, scancode, action, mods);
}

void ErrorCallback(int error, const char* description)
{
    Logger::Printf("GL Error %d: ", error);
    Logger::Log(description);
}

void MousePositionCallbak(GLFWwindow* aWindow, double aXPos, double aYPos)
{
	CommonSharedPtr<GLFWOutput> tOut = CommonDynamicSharedCast<GLFWOutput>(Application::GetSingleton()->GetIOManager()->GetOutput(aWindow));

    tOut->ProcessMouse(aXPos, aYPos);
}

void MouseButtonCallback(GLFWwindow* aWindow,int button, int action, int mode)
{
	CommonSharedPtr<GLFWOutput> tOut = CommonDynamicSharedCast<GLFWOutput>(Application::GetSingleton()->GetIOManager()->GetOutput(aWindow));

    tOut->MouseButtonPressed(aWindow, button, action, mode);
}

void ResizeCallback(GLFWwindow* aWindow, int aWidth, int aHeight)
{
    CommonSharedPtr<GLFWOutput> tOut = CommonDynamicSharedCast<GLFWOutput>(Application::GetSingleton()->GetIOManager()->GetOutput(aWindow));

    tOut->Resize(aWindow, aWidth, aHeight);
}

////////////////////////////////////////////////////////////////////////
GLFWOutput::GLFWOutput(const CommonString& a_Title) : mMainWindow(NULL),
m_Go_Up(false),
m_Go_Down(false),
m_Go_Right(false),
m_Go_Left(false)
{
    if (!glfwInit())
        exit(EXIT_FAILURE);

	glfwSetErrorCallback(ErrorCallback);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, MAJOR_VERSION); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, MINOR_VERSION);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#ifdef FULL_SCREEN
    mMainWindow = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, a_Title.GetChars(), glfwGetPrimaryMonitor(), NULL);
#else
	mMainWindow = glfwCreateWindow((int)WINDOW_WIDTH, (int)WINDOW_HEIGHT, a_Title.GetChars(), NULL, NULL);
#endif

    mViewport->InitProjectionMat(45.f, WINDOW_WIDTH, WINDOW_HEIGHT, .1f, 1000.f);

    mKey = mMainWindow;
}

////////////////////////////////////////////////////////////////////////
GLFWOutput::GLFWOutput(const GLFWOutput& aCopy)
{
    Logger::LogDebug("implement me", DEBUG_INFO);
	(void) aCopy;
}

////////////////////////////////////////////////////////////////////////
GLFWOutput::~GLFWOutput()
{
	//Logger::LogDebugSoft("destroy", DEBUG_SOFT_INFO);
    if (mMainWindow != NULL)
    {
        glfwDestroyWindow(mMainWindow);
    }
    glfwTerminate();
}

////////////////////////////////////////////////////////////////////////
int GLFWOutput::Init()
{
	if (!mMainWindow)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(mMainWindow);
	//CheckGlError("glfwMakeContextCurrent", DEBUG_INFO);

#ifdef _WIN32
	// Initialize GLEW
	glewExperimental=true; // Needed in core profile
	if (glewInit() != GLEW_OK)
	{
		return ErrorAndExit("Failed to initialize GLEW\n");
	}
#endif
    mShouldExit = false;
	CheckGlError("glewInit", DEBUG_INFO);


    glfwSetKeyCallback(mMainWindow, KeyCallback);
	CheckGlError("glfwSetKeyCallback", DEBUG_INFO);

    glfwSetCursorPosCallback(mMainWindow, MousePositionCallbak);
	CheckGlError("glfwSetCursorPosCallback", DEBUG_INFO);

    glfwSetMouseButtonCallback(mMainWindow, MouseButtonCallback);
	CheckGlError("glfwSetMouseButtonCallback", DEBUG_INFO);

    glfwSetFramebufferSizeCallback(mMainWindow, ResizeCallback);
    CheckGlError("glfwSetMouseButtonCallback", DEBUG_INFO);

    mCamera->Init();
	CheckGlError("mCamera->Init();", DEBUG_INFO);

	CommonSharedPtr<AOutput> t = CommonDynamicSharedCast<AOutput>(shared_from_this());
	CheckGlError("AOutput>(shared_from_this()", DEBUG_INFO);

	mRenderer->Init(t);
	CheckGlError("mRenderer->Init(t)", DEBUG_INFO);

    mMouseButtonPressed = -1;
    

    return ENGINE_SUCCESS;
}

////////////////////////////////////////////////////////////////////////
int GLFWOutput::ErrorAndExit()
{
    return ENGINE_SUCCESS;
}

////////////////////////////////////////////////////////////////////////
int GLFWOutput::ErrorAndExit(const char* aStr)
{
	(void) aStr;
    return ENGINE_SUCCESS;
}

////////////////////////////////////////////////////////////////////////
bool GLFWOutput::ShouldExit()
{
	if (!mShouldExit)
	{
		mShouldExit = glfwWindowShouldClose(mMainWindow) > 0;

		if (mShouldExit && mIOManagerRef)
		{
			mIOManagerRef->NotifyExit(mShouldExit);
			mIOManagerRef.reset();
		}
	}
	return mShouldExit ;

}


void GLFWOutput::Update(float aElapsedTime, ArrayGraphicComponents& aDrawables)
{
	if (!ShouldExit())
	{
		mRenderer->Update(aElapsedTime, aDrawables, mCamera, mViewport);

		glfwSwapBuffers(mMainWindow);
		glfwPollEvents();
		UpdateInputs();
	}
}


//////////////////////////////////////////////////////////////////////////
void GLFWOutput::ProcessInputs(GLFWwindow* /*window*/, int key, int /*scancode*/, int action, int /*mods*/)
{
//    LogDebugInfo("process input");
	//float tStep = 0.1f;

    // moving
    if ((key == GLFW_KEY_UP || key == GLFW_KEY_W) )//|| glfwGetKey( 'w' ) == GLFW_PRESS)
	{
		if (action == GLFW_PRESS || action == GLFW_REPEAT)
			m_Go_Forward = true;
		else
			m_Go_Forward = false;
	}
	else if ((key == GLFW_KEY_DOWN || key == GLFW_KEY_S) )//|| glfwGetKey( 's' ) == GLFW_PRESS)
	{
		if (action == GLFW_PRESS || action == GLFW_REPEAT)
			m_Go_Backward = true;
		else
			m_Go_Backward = false;
	}

	if ((key == GLFW_KEY_RIGHT || key == GLFW_KEY_D) )//|| glfwGetKey( 'd' ) == GLFW_PRESS)
	{
		if (action == GLFW_PRESS || action == GLFW_REPEAT)
			m_Go_Right = true;
		else
			m_Go_Right = false;
	}
	else if ((key == GLFW_KEY_LEFT || key == GLFW_KEY_A) )//|| glfwGetKey( 'a' ) == GLFW_PRESS)
	{
		if (action == GLFW_PRESS || action == GLFW_REPEAT)
			m_Go_Left = true;
		else
			m_Go_Left = false;
	}


	if (key == GLFW_KEY_E )
	{
		if (action == GLFW_PRESS || action == GLFW_REPEAT)
			m_Go_Up = true;
		else
			m_Go_Up = false;
	}
	else if (key == GLFW_KEY_Q )
	{
		if (action == GLFW_PRESS || action == GLFW_REPEAT)
			m_Go_Down = true;
		else
			m_Go_Down = false;
	}

	if (key == GLFW_KEY_1 && (action == GLFW_PRESS || action == GLFW_REPEAT)  )
	{
        Application::GetSingleton()->GetScene()->GetLight()->AddDiffuseIntensity(0.05f);
	}
	else if (key == GLFW_KEY_2 && (action == GLFW_PRESS || action == GLFW_REPEAT) )
	{
        Application::GetSingleton()->GetScene()->GetLight()->AddDiffuseIntensity(-0.05f);
	}

    bool setaction = false;
    for (int i = 0; i < 10 && !setaction; ++i)
    {
        if (m_Inputs[i][0] != 0 && m_Inputs[i][0] != key)
            continue;
        m_Inputs[i][0] = key;
        m_Inputs[i][1] = action;
        setaction = true;
    }

    if (!setaction)
        Logger::LogDebugSoft("warning, all inputs full", DEBUG_SOFT_INFO);

}

//////////////////////////////////////////////////////////////////////////
void GLFWOutput::ProcessMouse(double aXPos, double aYPos)
{
    if(mMouseButtonPressed >= 0)
    {
        mCamera->Rotate((float)(aXPos - mXPos), (float)(aYPos - mYPos));
    }

    mYPos = aYPos;
    mXPos = aXPos;
}

//////////////////////////////////////////////////////////////////////////
void GLFWOutput::MouseButtonPressed(GLFWwindow* /*window*/, int button, int action, int mods)
{
    if(action == GLFW_PRESS)
    {
        mMouseButtonPressed = button;
    }
    else
        mMouseButtonPressed = -1;
    mModifierKey = static_cast<short>(mods);
    
    
}

//////////////////////////////////////////////////////////////////////////
void Engine::GLFWOutput::UpdateInputs()
{
	float tStep = 0.1f;

	if (m_Go_Up)
		mCamera->MoveUp(tStep);
	else if (m_Go_Down)
		mCamera->MoveUp(-tStep);

	if (m_Go_Left)
		mCamera->MoveLeft(tStep);
	else if (m_Go_Right)
		mCamera->MoveRigth(tStep);

	if (m_Go_Forward)
		mCamera->MoveForward(tStep);
	else if (m_Go_Backward)
		mCamera->MoveForward(-tStep);


    m_InputDelegate(m_Inputs);
}

//////////////////////////////////////////////////////////////////////////
void Engine::GLFWOutput::Resize(GLFWwindow* /*window*/, int aW, int aH)
{
	mViewport->InitProjectionMat(45.f, (float)aW, (float)aH, .1f, 1000.f);
}
#endif // using_glfw


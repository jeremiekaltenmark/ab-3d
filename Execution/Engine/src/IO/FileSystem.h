/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 28-05-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#ifndef Engine_FileSystem_H
#define Engine_FileSystem_H

#include "Common/CommonFile.h"
//#include "Common/FastDelegate.h"

#ifdef _WIN32
#define SEPARATOR "\\"
#else // if defined __APPLE_CC__  // or other unix compiler
#define SEPARATOR "/"
#endif

namespace Engine
{

class FileSystem
{
private:
protected:
public:
	FileSystem();
	FileSystem(const FileSystem& aCopy);

	virtual ~FileSystem();

	static CommonSharedPtr<CommonFile> Load(const CommonString);
	static CommonSharedPtr<CommonString> LoadAsString(const CommonString);
//    static void LoadAsync(CommonString aPath, fastdelegate::FastDelegate<CommonFile>& aDelegate);

};

}
#endif // Engine_FileSystem_H

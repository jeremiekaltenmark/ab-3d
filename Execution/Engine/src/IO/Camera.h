/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 21-05-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

// This is a left handed coord system:
//   X match the right axe
//   y match the up axe
//   z match the forward axe (from viewer to the screen)
//

#ifndef Engine_Camera_H
#define Engine_Camera_H

#include "Maths/Maths.h"
#include "Maths/Matrix.h"
#include "Maths/Quaternion.h"
#include "Components/Component.h"
#include "Components/ViewMatrix.h"

namespace Engine
{

class Camera : public Component
{
public:
	Camera(void);
	virtual ~Camera(void);
	void Init();
	inline void Update();
	inline void LookAt(const Vec3& aTargetPosition);
	inline void SetPosition(const Vec3& aPos);
	inline void SetUpVector(const Vec3& aPos);

    inline void MoveLeft(float aDist);
	inline void MoveRigth(float aDist);
	inline void MoveUp(float aDist);
	inline void MoveForward(float aDist);

    inline Matrix& GetMatrix();
    inline CommonSharedPtr<ViewMatrix>& GetViewMatrix();
    inline const CommonSharedPtr<ViewMatrix>& GetViewMatrix() const;
	inline const Vec3& GetPosition() const;
	inline Vec3 CopyPosition() const;
	inline const Vec3& GetForward();
	inline const Vec3& GetUp();

    void Rotate(float aDeltaX, float aDeltaY);

private:

	CommonSharedPtr<ViewMatrix> mViewMatrix;
};


    inline Matrix& Camera::GetMatrix()
    {
        return mViewMatrix->GetViewMatrix();
    }
    inline CommonSharedPtr<ViewMatrix>& Camera::GetViewMatrix()
    {
        return mViewMatrix;
    }
    inline const CommonSharedPtr<ViewMatrix>& Camera::GetViewMatrix() const
    {
        return mViewMatrix;
    }
    inline void Camera::LookAt(const Vec3& aTargetPosition)
    {
        mViewMatrix->LookAt(aTargetPosition);
    }
    inline void Camera::SetPosition(const Vec3& aPos)
    {
        mViewMatrix->SetPosition(aPos);
    }
    inline void Camera::SetUpVector(const Vec3& aUpVector)
    {
        mViewMatrix->SetUpVector(aUpVector);
    }

    inline void Camera::MoveForward(float aDist)
    {
        mViewMatrix->MoveForward(aDist);
    }


    inline void Camera::MoveRigth(float aDist)
    {
        mViewMatrix->MoveRigth(aDist);
    }

    inline void Camera::MoveLeft(float aDist)
    {
        mViewMatrix->MoveLeft(aDist);
    }

    inline void Camera::MoveUp(float aDist)
    {
        mViewMatrix->MoveUp(aDist);
    }


    
    void Camera::Update()
    {
        mViewMatrix->Update();
    }

	Vec3 Camera::CopyPosition() const
	{
		return mViewMatrix->GetPosition();
	}
	const Vec3& Camera::GetPosition() const
	{
		return mViewMatrix->GetPosition();
	}
	const Vec3& Camera::GetForward() 
	{
        return mViewMatrix->GetForward();
	}
	const Vec3& Camera::GetUp()
	{
        return mViewMatrix->GetUp();
	}

}
#endif // Engine_Camera_H

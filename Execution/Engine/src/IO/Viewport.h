/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 09-04-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */


#ifndef Engine_Viewport_H
#define Engine_Viewport_H

#include "Maths/Matrix.h"

namespace Engine
{
    class Viewport
    {
    public:
        Viewport			(void);
        virtual			~Viewport			(void);
        void	Init				();
        void	InitProjectionMat	(float FOV, float aWidth, float aHeight, float zNear, float zFar);
        inline	void	Update				();
        inline	void	SetWidth			(float aWidth)	;
        inline	void	SetHeight			(float aHeight)	;
        inline	void	SetFOV				(float aFOV)	;
        inline	void	SetZNear			(float aZNear)	;
        inline	void	SetZFar				(float aZFar)	;

        inline  float   GetWidth            () const {return mWidth; }
        inline  float   GetHeight           () const {return mHeight; }

        inline	Matrix& GetProjectionMatrix();

    private:
        void	ComputeProjectionMat();

    private:

        bool mDirty;
        float mWidth;
        float mHeight;
        float mFOV;
        float mZNear;
        float mZFar;
        Matrix mProjectionMatrix;
    };
}
//////////////////////////////////////////////////////////////////////////
inline Engine::Matrix& Engine::Viewport::GetProjectionMatrix()
{
    Update();
    return mProjectionMatrix;
}

//////////////////////////////////////////////////////////////////////////
void Engine::Viewport::Update()
{
    if (mDirty)
    {
        ComputeProjectionMat();
    }
}

//////////////////////////////////////////////////////////////////////////
inline void Engine::Viewport::SetWidth(float aWidth)
{
    if (aWidth != mWidth)
    {
        mDirty = true;
        mWidth = aWidth;
    }
}
//////////////////////////////////////////////////////////////////////////
inline void Engine::Viewport::SetHeight(float aHeight)
{
    if (mHeight != aHeight)
    {
        mDirty = true;
        mHeight = aHeight;
    }

}
//////////////////////////////////////////////////////////////////////////
inline void Engine::Viewport::SetFOV(float aFOV)
{
    if (aFOV != mFOV)
    {
        mDirty = true;
        mFOV = aFOV;
    }
}
//////////////////////////////////////////////////////////////////////////
inline void Engine::Viewport::SetZNear(float aZNear)
{
    if (aZNear != mZNear)
    {
        mDirty = true;
        mZNear = aZNear;
    }
}
//////////////////////////////////////////////////////////////////////////
inline void Engine::Viewport::SetZFar(float aZFar)
{
    if (aZFar != mZFar)
    {
        mDirty = true;
        mZFar = aZFar;
    }
}


#endif // Engine_Viewport_H

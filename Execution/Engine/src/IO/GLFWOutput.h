/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 06-03-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#ifdef USING_GLFW

#ifndef Engine_GLFWOutput_H
#define Engine_GLFWOutput_H

#include "IO/AOutput.h"
#include "Engine/Engine.h"

namespace Engine
{

	class GLFWOutput : public AOutput, public CommonEnableSharedFromThis<GLFWOutput>
{
private:

    double mXPos;
    double mYPos;
    int mMouseButtonPressed;
    short mModifierKey; // shift, alt, contrl or super (windows or apple)

	bool m_Go_Up;
	bool m_Go_Down;
	bool m_Go_Right;
	bool m_Go_Left;
	bool m_Go_Forward;
	bool m_Go_Backward;


protected:
    GLFWwindow* mMainWindow;

public:
	GLFWOutput(const CommonString& a_Title);
	GLFWOutput(const GLFWOutput& aCopy);

	virtual ~GLFWOutput();

    virtual void Update(float aElapsedTime, ArrayGraphicComponents& aDrawables);
	virtual int Init() ;
	virtual int ErrorAndExit() ;
	virtual int ErrorAndExit(const char* aStr) ;
    virtual bool ShouldExit();

    void Resize(GLFWwindow* window, int aW, int aH);
    void ProcessInputs(GLFWwindow* window, int key, int scancode, int action, int mods);
	void UpdateInputs();
    void ProcessMouse(double aXPos, double aYPos);
	void MouseButtonPressed(GLFWwindow* window, int button, int action, int mods);

};

}
#endif // Engine_GLFWOutput_H

#endif // using_GLFW

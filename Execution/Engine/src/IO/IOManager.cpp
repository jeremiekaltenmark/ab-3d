/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 03-03-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */


#include "Engine/Engine.h"
#include "IO/IOManager.h"

#include "IO/GLFWOutput.h"

using namespace Engine;


////////////////////////////////////////////////////////////////////////
IOManager::IOManager() : mShouldExit(false)
{
	//CommonSharedPtr<GLFWOutput> t_StandardOutput (new GLFWOutput());
 //   t_StandardOutput->Init();
	//CommonSharedPtr<IOManager> t = shared_from_this();
	//t_StandardOutput->SetIOManager(t);

	//AddOutput(t_StandardOutput->GetKey(), CommonDynamicSharedCast<AOutput>(t_StandardOutput));
}

////////////////////////////////////////////////////////////////////////
IOManager::IOManager(const IOManager& aCopy)
{
    Logger::LogDebug("implement me", DEBUG_INFO);
	(void) aCopy;
}

////////////////////////////////////////////////////////////////////////
IOManager::~IOManager()
{
//     for (OutPutMap::iterator it = mOutPuts.begin()
//          ; it != mOutPuts.end();
//          ++it)
//     {
//         //delete it->second;
//         it->second = NULL;
//     }
	//Logger::LogDebugSoft("destroy", DEBUG_SOFT_INFO);

    mOutPuts.clear(); // clearing the map
}

////////////////////////////////////////////////////////////////////////
bool IOManager::ShouldExit()
{
    return mShouldExit;
}

////////////////////////////////////////////////////////////////////////
void IOManager::NotifyExit(bool aShouldExit)
{
    mShouldExit = aShouldExit;
}

////////////////////////////////////////////////////////////////////////
void IOManager::AddOutput(KeyType aKey, const CommonSharedPtr<AOutput>& aAOutput)
{
    mOutPuts[aKey] = aAOutput;
}

////////////////////////////////////////////////////////////////////////
CommonSharedPtr<AOutput> IOManager::GetOutput(KeyType aKey)
{
    OutPutMap::iterator it = mOutPuts.find(aKey);
    if(it != mOutPuts.end())
    {
        return it->second;
    }
    else
    {
        return NULL;
    }
}

////////////////////////////////////////////////////////////////////////
void IOManager::Update(float aElapsedTime, ArrayGraphicComponents& aDrawables)
{
    for (OutPutMap::iterator it = mOutPuts.begin()
         ; it != mOutPuts.end();
         ++it)
    {
        it->second->Update(aElapsedTime, aDrawables);
    }
}

////////////////////////////////////////////////////////////////////////
void Engine::IOManager::Init()
{
	CommonSharedPtr<IOManager> t = shared_from_this();

	CommonSharedPtr<GLFWOutput> t_StandardOutput(new GLFWOutput("Simple example"));
	t_StandardOutput->GetRenderer()->SetPass(/*Renderer::SHADOWPASS |*/ Renderer::FORWARDPASS);
	t_StandardOutput->Init();
		CheckGlError("t_StandardOutput->Init();", DEBUG_INFO);
	t_StandardOutput->SetIOManager(t);
	AddOutput(t_StandardOutput->GetKey(), CommonDynamicSharedCast<AOutput>(t_StandardOutput));
}

CommonSharedPtr<AOutput> Engine::IOManager::GetOutputFromIndex(uint a_index)
{
	OutPutMap::iterator it = mOutPuts.begin();
	for (uint i = 0; it != mOutPuts.end() && i != a_index; ++it, ++i)
	{
	}

	if (it != mOutPuts.end())
	{
		return it->second;
	}
	else
	{
		return NULL;
	}
}












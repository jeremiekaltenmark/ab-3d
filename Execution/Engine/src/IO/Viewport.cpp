/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 09-04-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#include "Engine/Engine.h"
#include "Viewport.h"

using namespace Engine;

//////////////////////////////////////////////////////////////////////////
Viewport::Viewport(void) : mDirty(true), mWidth(0), mHeight(0), mFOV(0), mZNear(0), mZFar(0)
{
	mProjectionMatrix.SetIdentity();
}


//////////////////////////////////////////////////////////////////////////
Viewport::~Viewport(void)
{
	//Logger::LogDebugSoft("destroy", DEBUG_SOFT_INFO);
}

//////////////////////////////////////////////////////////////////////////
void Viewport::Init()
{
	ComputeProjectionMat();
}

//////////////////////////////////////////////////////////////////////////
void Viewport::InitProjectionMat(float FOV, float aWidth, float aHeight, float zNear, float zFar)
{
	mFOV = FOV;
	mWidth = aWidth;
	mHeight = aHeight;
	mZFar = zFar;
	mZNear = zNear;
	ComputeProjectionMat();
}

//////////////////////////////////////////////////////////////////////////
void Viewport::ComputeProjectionMat()
{
	const float tAspectRatio = mWidth / mHeight;
	const float zRange       = mZNear - mZFar;
	const float tanHalfFOV   = Maths::tanf(Maths::DegreeToRadian(mFOV / 2.0f));

	mProjectionMatrix.mMat[0][0] = 1.0f/(tanHalfFOV * tAspectRatio);
    mProjectionMatrix.mMat[0][1] = 0.0f;
    mProjectionMatrix.mMat[0][2] = 0.0f;
    mProjectionMatrix.mMat[0][3] = 0.0 ;

    mProjectionMatrix.mMat[1][0] = 0.0f;
    mProjectionMatrix.mMat[1][1] = 1.0f/tanHalfFOV;
    mProjectionMatrix.mMat[1][2] = 0.0f;
    mProjectionMatrix.mMat[1][3] = 0.0;

    mProjectionMatrix.mMat[2][0] = 0.0f;
    mProjectionMatrix.mMat[2][1] = 0.0f;
    mProjectionMatrix.mMat[2][2] = (-mZNear -mZFar)/zRange ;
	mProjectionMatrix.mMat[2][3] = 2.0f * mZFar * mZNear/zRange;

	mProjectionMatrix.mMat[3][0] = 0.0f;
	mProjectionMatrix.mMat[3][1] = 0.0f;
	mProjectionMatrix.mMat[3][2] = 1.0f;
    mProjectionMatrix.mMat[3][3] = 0.0;

	mDirty = false;    
}

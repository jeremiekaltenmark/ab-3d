/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 03-03-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#ifndef Engine_IOManager_H
#define Engine_IOManager_H

#include <map>
#include "Engine/Engine.h"


namespace Engine
{
class AOutput;

class IOManager : public CommonEnableSharedFromThis<IOManager>
{
public:
    typedef void* KeyType;
	typedef std::map<KeyType, CommonSharedPtr<AOutput> > OutPutMap;
	typedef std::pair<KeyType, CommonSharedPtr<AOutput> > MapOutPutElement;

public:
	IOManager();
	IOManager(const IOManager& aCopy);

	virtual ~IOManager();

	/**
	 *	warning : to call this function, you need to store IOManager in a CommonSharedObject
	 */
	void Init();
    bool ShouldExit();
    void NotifyExit(bool aShouldExit = true);

	void AddOutput(KeyType aKey,const CommonSharedPtr<AOutput>& aOutput);
	CommonSharedPtr<AOutput> GetOutput(KeyType aOutputKey);
	CommonSharedPtr<AOutput> GetOutputFromIndex(uint a_index);

    void Update(float aElapsedTime, ArrayGraphicComponents& aDrawables);

private:
    OutPutMap mOutPuts;

    bool mShouldExit;

};

}
#endif // Engine_IOManager_H

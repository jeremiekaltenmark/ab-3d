/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 06-03-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#ifndef Engine_AOutput_H
#define Engine_AOutput_H

#include "IO/IOManager.h"

#include "Rendering/Renderer.h"
#include "IO/Viewport.h"
#include "IO/Camera.h"
#include <functional>


namespace Engine
{


class AOutput
{
private:
protected:
    bool mShouldExit;
    CommonSharedPtr<IOManager> mIOManagerRef;
    IOManager::KeyType mKey;
    
    CommonSharedPtr<Renderer> mRenderer;
	CommonSharedPtr<Viewport> mViewport;
	CommonSharedPtr<Camera> mCamera;

    int m_Inputs[10][2]; // chaque input process est ajout� dans le tableau (10 maximums en mm temps),
                         //la seconde valeur est l'action: press 1, release 0, repeat 2
    std::function<void(int(&)[10][2])> m_InputDelegate;

public:
	AOutput();
	AOutput(const AOutput& aCopy);

	virtual ~AOutput();

    virtual void Update(float aElapsedTime, ArrayGraphicComponents& aDrawables) = 0;

	virtual int Init() = 0;
	virtual int ErrorAndExit() = 0;
	virtual int ErrorAndExit(const char* aStr) = 0;
    virtual bool ShouldExit() = 0;

	CommonSharedPtr<Renderer>& GetRenderer()	{ return mRenderer;	  }
	CommonSharedPtr<Viewport>& GetViewport()	{ return mViewport;	  }
	CommonSharedPtr<Camera	>& GetCamera  ()	{ return mCamera;	  }

    void RegisterInputCallback(std::function<void(int(&m_Inputs)[10][2])>& a_Delegate) { m_InputDelegate = a_Delegate; }

	void SetIOManager(const CommonSharedPtr<IOManager>& aMgr) { mIOManagerRef = aMgr; }
    IOManager::KeyType GetKey()
    {
        return mKey;
    }

};

}
#endif // Engine_AOutput_H

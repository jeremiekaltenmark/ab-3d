/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 03-04-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#ifndef VEC3_H
#define VEC3_H

#include "Maths/Maths.h"

namespace Engine {


	class Vec3
	{
	public:
#pragma warning(disable : 4201)
		union
		{
			struct
			{
				float x, y, z;
			};
			float t[3];
		};

#pragma warning(default : 4201)

		Vec3(void)			{x = 0.0f; y = 0.0f; z = 0.0f; }
		Vec3(const Vec3 & V)		{x = V.x;  y = V.y;  z = V.z;}
		Vec3(float Xi, float Yi, float Zi)	{x = Xi;    y = Yi;    z = Zi;}


		inline float X() const {return x;}
		inline float Y() const {return y;}
		inline float Z() const {return z;}

		inline void Set(float X, float Y, float Z) {x = X;    y = Y;    z = Z;}
		inline void Reset()			{x = 0.0f; y = 0.0f; z = 0.0f;}

		inline void Reverse(void)                  {x *= -1.0f; y *= -1.0f; z *= -1.0f;}

		inline float Magnitude(void) const		{return(Maths::sqrtf(x*x + y*y + z*z));}
		inline float Dot(const Vec3 & V) const	{return(x*V.x + y*V.y + z*V.z);}

		inline Vec3 Clone(void)			{return Vec3(x, y, z);}

		inline bool IsUnit() const {return (Magnitude() == 1.0f);}
		inline bool IsNull() const {return (x == 0.0f && y == 0.0f && z == 0.0f);}

		inline bool IsPerpendicularTo(const Vec3 & V) const {return (Dot(V) == 0.0f);}

		inline void     operator =  (const Vec3 & V){x = V.x; y = V.y; z = V.z;}

		inline bool     operator == (const Vec3 & V) const {return(x == V.x && y == V.y && z == V.z);}
		inline bool     operator != (const Vec3 & V) const {return(x != V.x || y != V.y || z != V.z);}

		inline float    operator [] (int I) const { if (I >= 0 && I < 3) return t[I]; else return 0; }
		inline float&   operator [] (int I)       {	if (I >= 0 && I < 3) return t[I]; else return t[I % 3]; }

		inline Vec3   operator +  (const Vec3 & V) const {return(Vec3(x + V.x, y + V.y, z + V.z));}
		inline Vec3   operator -  (const Vec3 & V) const {return(Vec3(x - V.x, y - V.y, z - V.z));}
		inline Vec3   operator *  (const Vec3 & V) const {return(Vec3(x * V.x, y * V.y, z * V.z));}
		inline Vec3   operator /  (const Vec3 & V) const {return(Vec3(x / V.x, y / V.y, z / V.z));}

		inline Vec3   operator *  (float F)  const { return(Vec3(x * F, y * F, z * F)); }
		inline Vec3   operator /  (float F)  const { return(Vec3(x / F, y / F, z / F)); }

		inline void     operator += (const Vec3 & V) { x += V.x; y += V.y; z += V.z; }
		inline void     operator -= (const Vec3 & V) { x -= V.x; y -= V.y; z -= V.z; }
		inline void     operator *= (const Vec3 & V) { x *= V.x; y *= V.y; z *= V.z; }
		inline void     operator /= (const Vec3 & V) { x /= V.x; y /= V.y; z /= V.z; }

		inline void     operator *= (float F) { x *= F;   y *= F; z *= F; }
		inline void     operator /= (float F) { x /= F; y /= F; z /= F; }

		inline Vec3   operator -  () const {return(Vec3(-x, -y, -z));}

		inline Vec3 & operator ++ (void) {x++; y++; z++; return(*this);}
		inline Vec3 & operator -- (void) {x--; y--; z--; return(*this);}
		inline Vec3   operator ++ (int)  {Vec3 Temp = *this; x++; y++; z++; return(Temp);}
		inline Vec3   operator -- (int)  {Vec3 Temp = *this; x--; y--; z--; return(Temp);}

		// for insertion in map
		inline bool operator < (const Vec3 & V) const 
		{
			if (Maths::IsNearlyZero(x - V.x)) // x equals
			{
				if (Maths::IsNearlyZero(y - V.y)) // y equals
				{
					if (Maths::IsNearlyZero(z - V.z)) // z equals
					{
						return false;
					}
					else
					{
						return z < V.z;
					}
				}
				else
				{
					return y < V.y;
				}
			}
			else
			{
				return x < V.x;
			}
		}


		inline Vec3 Cross(const Vec3 & V) const
		{
			return Vec3(
						y * V.z - z * V.y,
						z * V.x - x * V.z,
						x * V.y - y * V.x);
		}

		inline float Distance(const Vec3 & V)
		{
			return (Maths::sqrtf(Maths::powf(x - V.x, 2) + Maths::powf(y - V.y, 2) + Maths::powf(z - V.z, 2)));
		}

		inline bool IsNearlyEqualsTo (const Vec3 & V) const
		{
			float f = 10000;

			Vec3 a = (*this);
			Vec3 b = V;

			a *= f;
			b *= f;

			a.x = Maths::floor(a.x);
			a.y = Maths::floor(a.y);
			a.z = Maths::floor(a.z);

			b.x = Maths::floor(b.x);
			b.y = Maths::floor(b.y);
			b.z = Maths::floor(b.z);

			return(a.x == b.x && a.y == b.y && a.z == b.z);
		}

		inline void Normalize(void)
		{
			if(IsNull())return;

			float tM = Magnitude();
			x /= tM;	y /= tM;	z /= tM;
		}

		inline Vec3 NormalizeVector(void) const
		{
			if(IsNull())return *this;

			float tM = Magnitude();
			return(Vec3(x / tM, y / tM, z / tM));
		}

		inline void Truncate(float lenght)
		{
			Normalize();
			SelfScale(lenght);
		}

		inline void LookAt(const Vec3 & v)
		{
			Vec3 delta = v;
			delta -= (*this);
			delta.Normalize();
			*this = delta;
		}

		inline void SelfScale(float f)
		{
			x *= f; y *= f; z *= f;
		}

		inline void SelfScale(float X, float Y, float Z)
		{
			x *= X; y *= Y; z *= Z;
		}

		inline void SelfDivide(float f)
		{
			x /= f; y /= f; z /= f;
		}

		inline void SelfDivide(float X, float Y, float Z)
		{
			x /= X; y /= Y; z /= Z;
		}

		inline void SelfTranslate(float X, float Y, float Z)
		{
			x += X; y += Y; z += Z;
		}

		inline void SelfRotateX(float Angle)
		{
			float Sin = Maths::sinf(Angle);
			float Cos = Maths::cosf(Angle);
			float NewY = y*Cos - z*Sin;
			float NewZ = y*Sin + z*Cos;
			y = NewY;
			z = NewZ;
		}

		inline void SelfRotateY(float Angle)
		{
			float Sin = Maths::sinf(Angle);
			float Cos = Maths::cosf(Angle);
			float NewX = x*Cos + z*Sin;
			float NewZ = -x*Sin + z*Cos;
			x = NewX;
			z = NewZ;
		}

		inline void SelfRotateZ(float Angle)
		{
			float Sin = Maths::sinf(Angle);
			float Cos = Maths::cosf(Angle);
			float NewX = x*Cos - y*Sin;
			float NewY = x*Sin + y*Cos;
			x = NewX;
			y = NewY;
		}

		inline Vec3 Scale(float f) const
		{
			return(Vec3(x * f, y * f, z * f));
		}
		
		
		inline Vec3 Scale(float X, float Y, float Z) const
		{
			return(Vec3(x * X, y * Y, z * Z));
		}
		
		inline Vec3 Divide(float f) const
		{
			return(Vec3(x / f, y / f, z / f));
		}
		
		
		inline Vec3 Divide(float X, float Y, float Z)
		{
			return(Vec3(x / X, y / Y, z / Z));
		}
		
		inline Vec3 Translate(float X, float Y, float Z)
		{
			return(Vec3(x + X, y + Y, z + Z));
		}
		
		void SelfRotate(float Angle, const Vec3& Axe);
		
		inline Vec3 RotateX(float aRadAngle)
		{
			float Sin = Maths::sinf(aRadAngle);
			float Cos = Maths::cosf(aRadAngle);
			
			return(Vec3(x, y*Cos - z*Sin, y*Sin + z*Cos));
		}
		
		inline Vec3 RotateY(float aRadAngle)
		{
			float Sin = Maths::sinf(aRadAngle);
			float Cos = Maths::cosf(aRadAngle);
			
			return(Vec3(x*Cos + z*Sin, y, -x*Sin + z*Cos));
		}
		
		inline Vec3 RotateZ(float aRadAngle)
		{
			float Sin = Maths::sinf(aRadAngle);
			float Cos = Maths::cosf(aRadAngle);
			
			return(Vec3(x*Cos - y*Sin, x*Sin + y*Cos, z));
		}
		
		inline Vec3 Projection(const Vec3 & V)
		{
			return ( V * (Dot(V) / Maths::powf(Magnitude(), 2)));
		}
		
		
		float AngleBetween(const Vec3 & V) const;
		Vec3 Noise(float amplitude);
		
		~Vec3(){}
		
		
		// 	friend ostream& operator << (ostream& o, Vec3& v)
		// 	{
		// 		o << "X=" << v.x << " Y=" << v.y << " Z=" << v.z << endl;
		// 		return o;
		// 	}

//		CommonString ToString() const
//		{
//			CommonString t("");
//			t += "{";
//			t += x;
//			t += ", ";
//			t += y;
//			t += ", ";
//			t += z;
//			t += "}";
//			return t;
//		}

		
	};
	
	const static Vec3 AxisX(1, 0, 0);
	const static Vec3 AxisY(0, 1, 0);
	const static Vec3 AxisZ(0, 0, 1);
	const static Vec3 ZeroVec3(0, 0, 0);
	const static Vec3 OneVec3(1, 1, 1);
}

#endif
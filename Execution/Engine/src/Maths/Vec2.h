/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 19-06-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#ifndef Vec2_H
#define Vec2_H

#include "Maths/Maths.h"

namespace Engine {


    class Vec2
    {
    public:
#pragma warning(disable : 4201)

        union
        {
			struct
			{
				float x, y;
			};
            float t[2];
        };
#pragma warning(default : 4201)

        Vec2(void)			{x = 0.0f; y = 0.0f;  }
		Vec2(const Vec2 & V) { x = V.x;  y = V.y; }
        Vec2(float Xi, float Yi)	{x = Xi;    y = Yi; }


        inline float X() const {return x;}
        inline float Y() const {return y;}

        inline void Set(float X, float Y) {x = X;    y = Y; }
        inline void Reset()			{x = 0.0f; y = 0.0f;}

        inline void Reverse(void)                  {x *= -1.0f; y *= -1.0f; }

        inline float Magnitude(void) const		{return(Maths::sqrtf(x*x + y*y));}
        inline float Dot(const Vec2 & V) const	{return(x*V.x + y*V.y);}

        inline Vec2 Clone(void)			{return Vec2(x, y);}

        inline bool IsUnit() const {return (Magnitude() == 1.0f);}
        inline bool IsNull() const {return (x == 0.0f && y == 0.0f);}

        inline bool IsPerpendicularTo(const Vec2 & V) const {return (Dot(V) == 0.0f);}

        inline void     operator =  (const Vec2 & V){x = V.x; y = V.y; }

        inline bool     operator == (const Vec2 & V) const {return(x == V.x && y == V.y);}
        inline bool     operator != (const Vec2 & V) const {return(x != V.x || y != V.y);}

		inline float    operator [] (int I) const { if (I >= 0 && I < 2) return t[I]; else return 0; }
		inline float&   operator [] (int I)       { {	if (I >= 0 && I < 2) return t[I]; else return t[I % 2]; } }

		inline Vec2   operator +  (const Vec2 & V) const { return(Vec2(x + V.x, y + V.y)); }
		inline Vec2   operator -  (const Vec2 & V) const { return(Vec2(x - V.x, y - V.y)); }
		inline Vec2   operator *  (const Vec2 & V) const { return(Vec2(x * V.x, y * V.y)); }
		inline Vec2   operator /  (const Vec2 & V) const { return(Vec2(x / V.x, y / V.y)); }

        inline Vec2   operator *  (float F)  const {return(Vec2(x * F,   y * F));}
        inline Vec2   operator /  (float F)  const {return(Vec2(x / F,   y / F));}

        inline void     operator += (const Vec2 & V) {x += V.x; y += V.y;}
        inline void     operator -= (const Vec2 & V) {x -= V.x; y -= V.y;}
        inline void     operator *= (const Vec2 & V) {x *= V.x; y *= V.y;}
        inline void     operator /= (const Vec2 & V) {x /= V.x; y /= V.y;}

		inline void     operator *= (float F) { x *= F;   y *= F; }
		inline void     operator /= (float F) { x /= F;   y /= F; }

        inline Vec2   operator -  () const {return(Vec2(-x, -y));}

        inline Vec2 & operator ++ (void) {x++; y++; return(*this);}
        inline Vec2 & operator -- (void) {x--; y--; return(*this);}
        inline Vec2   operator ++ (int)  {Vec2 Temp = *this; x++; y++; return(Temp);}
        inline Vec2   operator -- (int)  {Vec2 Temp = *this; x--; y--; return(Temp);}

		/**
		 *	returns a perpendicular vector in the 2D space
		 */
        inline Vec2 Cross(const Vec2 & V) const
        {
            return Vec2(V.Y(), -V.X());
        }

        inline float Distance(const Vec2 & V)
        {
            return (Maths::sqrtf(Maths::powf(x - V.x, 2) + Maths::powf(y - V.y, 2)));
        }

        inline bool IsNearlyEqualsTo (const Vec2 & V) const
        {
            float f = 10000;

            Vec2 a = (*this);
            Vec2 b = V;

            a *= f;
            b *= f;

            a.x = Maths::floor(a.x);
            a.y = Maths::floor(a.y);

            b.x = Maths::floor(b.x);
            b.y = Maths::floor(b.y);

            return(a.x == b.x && a.y == b.y);
        }

        inline void Normalize(void)
        {
            if(IsNull())return;

            float tM = Magnitude();
            x /= tM;	y /= tM;
        }

        inline Vec2 NormalizeVector(void) const
        {
            if(IsNull())return *this;

            float tM = Magnitude();
            return(Vec2(x / tM, y / tM));
        }

        inline void Truncate(float lenght)
        {
            Normalize();
            SelfScale(lenght);
        }

        inline void LookAt(const Vec2 & v)
        {
            Vec2 delta = v;
            delta -= (*this);
            delta.Normalize();
            *this = delta;
        }

        inline void SelfScale(float f)
        {
            x *= f; y *= f; ;
        }

		inline void SelfDivide   (float f)			{ x /= f; y /= f; }
		inline void SelfScale    (float X, float Y) { x *= X; y *= Y; }
		inline void SelfDivide	 (float X, float Y) { x /= X; y /= Y; }
		inline void SelfTranslate(float X, float Y) { x += X; y += Y; }

        inline void SelfRotate(float Angle)
        {
            float Sin = Maths::sinf(Angle);
            float Cos = Maths::cosf(Angle);
            float NewX = x*Cos - y*Sin;
            float NewY = x*Sin + y*Cos;
            x = NewX;
            y = NewY;
        }

		inline Vec2 Scale(float f) const { return(Vec2(x * f, y * f)); }
		inline Vec2 Divide(float f) { return(Vec2(x / f, y / f)); }

		inline Vec2 Scale(float X, float Y) const { return(Vec2(x * X, y * Y)); }
		inline Vec2 Divide(float X, float Y) const { return(Vec2(x / X, y / Y)); }
		inline Vec2 Translate(float X, float Y) { return(Vec2(x + X, y + Y)); }

        inline Vec2 Rotate(float aRadAngle)
        {
            float Sin = Maths::sinf(aRadAngle);
            float Cos = Maths::cosf(aRadAngle);
            
            return(Vec2(x*Cos - y*Sin, x*Sin + y*Cos));
        }
        
        inline Vec2 Projection(const Vec2 & V)
        {
            return ( V * (Dot(V) / Maths::powf(Magnitude(), 2)));
        }
        
        
        float AngleBetween(const Vec2 & V) const;
        Vec2 Noise(float amplitude);
        
        ~Vec2(){}
        
        
        // 	friend ostream& operator << (ostream& o, Vec2& v)
        // 	{
        // 		o << "X=" << v.x << " Y=" << v.y << " Z=" << v.z << endl;
        // 		return o;
        // 	}
        
        
	};
}

#endif
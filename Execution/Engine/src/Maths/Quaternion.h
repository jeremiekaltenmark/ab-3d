/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 03-04-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */


#ifndef QUATERNION_H
#define QUATERNION_H

#include "Maths/Maths.h"
#include "Maths/Matrix.h"
#include "Maths/Vec3.h"

namespace Engine {


    class Quaternion
    {
    public:
        Quaternion() : x(0), y(0), z(0), s(1) {}
        Quaternion(const Quaternion& aRef) : x(aRef.x), y(aRef.y), z(aRef.z), s(aRef.s) {}
        Quaternion(const Vec3& aAxe, float aAngle) { Set(aAxe, aAngle); }

        inline const Quaternion& operator=(const Quaternion& aRef);

        inline void Set(const Vec3& aAxe, float aAngle);
        inline void Reset() { s = 1; x = 0; y = 0; z = 0; }

        inline void Mult(const Quaternion& aQuatA, const Quaternion& aQuatB);
        inline void SelfMult(const Quaternion& aRef);
        //static inline Mult(const Quaternion& aQuatA, const Quaternion& aQuatB, Quaternion& aQuatOut);
        inline void Conjugate(Quaternion& aQuatConj) const;
        inline void Conjugate();

        inline void GetTransfMat(float aMatrix[16]) const;
        inline void GetTransfMat3(float aMatrix[9]) const;
        inline void FromTransfMat(const float aMat[16]);
        inline void FromTransfMat(const Matrix& aMat);

        inline void Transform(const Vec3& aIn, Vec3& aOut) const;

        inline void Slerp(const Quaternion& aFrom, const Quaternion& aTo, float aT);

        inline void GetAxeAndAngle(Vec3& aAxe, float& aAngle);

        inline Quaternion operator*(const Quaternion& aRef);
        inline Quaternion operator*(const Vec3& aRef);



    private:
        inline void Normalize();
        Quaternion(float aX, float aY, float aZ, float aS) : x(aX), y(aY), z(aZ), s(aS) {}

    public:
        float x;
        float y;
        float z;
        float s;
    };


    inline const Quaternion& Quaternion::operator=(const Quaternion& aRef)
    {
        x = aRef.x;
        y = aRef.y;
        z = aRef.z;
        s = aRef.s;

        return *this;
    }

    inline void Quaternion::Set(const Vec3& aAxe, float aAngle)
    {
        aAngle *= 0.5f;
        float tLength = aAxe.Magnitude();
        float tSinus = Maths::sinf(aAngle) / tLength;

        x = aAxe.X() * tSinus;
        y = aAxe.Y() * tSinus;
        z = aAxe.Z() * tSinus;
        s = Maths::cosf(aAngle);
    }

    inline void Quaternion::Mult(const Quaternion& aQuatA, const Quaternion& aQuatB)
    {
        x = aQuatA.s * aQuatB.x + aQuatA.x * aQuatB.s + aQuatA.y * aQuatB.z - aQuatA.z * aQuatB.y;
        y = aQuatA.s * aQuatB.y + aQuatA.y * aQuatB.s + aQuatA.z * aQuatB.x - aQuatA.x * aQuatB.z;
        z = aQuatA.s * aQuatB.z + aQuatA.z * aQuatB.s + aQuatA.x * aQuatB.y - aQuatA.y * aQuatB.x;
        s = aQuatA.s * aQuatB.s - aQuatA.x * aQuatB.x - aQuatA.y * aQuatB.y - aQuatA.z * aQuatB.z;

        Normalize();
    }

    inline void Quaternion::SelfMult(const Quaternion& aRef)
    {
        float tx = x, ty = y, tz = z, ts = s;
        x = ts * aRef.x + tx * aRef.s + ty * aRef.z - tz * aRef.y;
        y = ts * aRef.y + ty * aRef.s + tz * aRef.x - tx * aRef.z;
        z = ts * aRef.z + tz * aRef.s + tx * aRef.y - ty * aRef.x;
        s = ts * aRef.s - tx * aRef.x - ty * aRef.y - tz * aRef.z;

        Normalize();
    }

    inline void Quaternion::GetTransfMat(float aMatrix[16]) const
    {
        aMatrix[ 0] = 1 - 2 * y * y - 2 * z * z;    aMatrix[ 4] = 2 * x * y - 2 * s * z;        aMatrix[ 8] = 2 * x * z + 2 * s * y;        aMatrix[12] = 0;
        aMatrix[ 1] = 2 * x * y + 2 * s * z;        aMatrix[ 5] = 1 - 2 * x * x - 2 * z * z;    aMatrix[ 9] = 2 * y * z - 2 * s * x;        aMatrix[13] = 0;
        aMatrix[ 2] = 2 * x * z - 2 * s * y;        aMatrix[ 6] = 2 * y * z + 2 * s * x;        aMatrix[10] = 1 - 2 * x * x - 2 * y * y;    aMatrix[14] = 0;
        aMatrix[ 3] = 0;                            aMatrix[ 7] = 0;                            aMatrix[11] = 0;                            aMatrix[15] = 1;
    }

    inline void Quaternion::GetTransfMat3(float aMatrix[9]) const
    {
        aMatrix[0] = 1 - 2 * y * y - 2 * z * z;    aMatrix[3] = 2 * x * y - 2 * s * z;        aMatrix[6] = 2 * x * z + 2 * s * y;
        aMatrix[1] = 2 * x * y + 2 * s * z;        aMatrix[4] = 1 - 2 * x * x - 2 * z * z;    aMatrix[7] = 2 * y * z - 2 * s * x;
        aMatrix[2] = 2 * x * z - 2 * s * y;        aMatrix[5] = 2 * y * z + 2 * s * x;        aMatrix[8] = 1 - 2 * x * x - 2 * y * y;
    }

    inline void Quaternion::Conjugate(Quaternion& aQuatConj) const
    {
        aQuatConj.x = -x;
        aQuatConj.y = -y;
        aQuatConj.z = -z;
        aQuatConj.s =  s;
    }

    inline void Quaternion::Conjugate()
    {
        x = -x;
        y = -y;
        z = -z;
    }

    inline void Quaternion::Transform(const Vec3& aIn, Vec3& aOut) const
    {
        float tLen = aIn.Magnitude();
        float tLenInv = 1.0f / tLen;

        Quaternion tQuatVect(aIn.X() * tLenInv, aIn.Y() * tLenInv, aIn.Z() * tLenInv, 0.0f);
        Quaternion tQuatTmp;

        Quaternion tConjugate;
        Conjugate(tConjugate);

        tQuatTmp .Mult(*this, tQuatVect);
        tQuatVect.Mult(tQuatTmp, tConjugate);

        aOut.x = tQuatVect.x * tLen;
        aOut.y = tQuatVect.y * tLen;
        aOut.z = tQuatVect.z * tLen;
    }

    inline void Quaternion::Normalize()
    {
        float tLenghtInv = 1.0f / Maths::sqrtf(s * s + x * x + y * y + z * z);
        x *= tLenghtInv;
        y *= tLenghtInv;
        z *= tLenghtInv;
        s *= tLenghtInv;
    }

    inline void Quaternion::GetAxeAndAngle(Vec3& aAxe, float& aAngle)
    {
        aAxe.Set(x, y, z);
        float tSin = aAxe.Magnitude();
        aAxe /= tSin;
        aAngle = Maths::atan2f(tSin, s);
    }

    inline void Quaternion::Slerp(const Quaternion& aFrom, const Quaternion& aTo, float aT)
    {
        float tProduct = aFrom.s * aTo.s + aFrom.x * aTo.x + aFrom.y * aTo.y + aFrom.z * aTo.z;

        if(Maths::abs(tProduct) < 1.0f)
        {
            // Take care of long angle case see http://en.wikipedia.org/wiki/Slerp
            const float tSign = (tProduct < 0) ? -1.0f : 1.0f;

            const float tTheta =Maths::acosf(tSign * tProduct);
            const float s1 = Maths::sinf(tSign * aT * tTheta);
            const float d  = 1.0f / Maths::sinf(tTheta);
            const float s0 = Maths::sinf((1.0f - aT) * tTheta);

            x = (aFrom.x * s0 + aTo.x * s1) * d;
            y = (aFrom.y * s0 + aTo.y * s1) * d;
            z = (aFrom.z * s0 + aTo.z * s1) * d;
            s = (aFrom.s * s0 + aTo.s * s1) * d;
        }
        else
        {
            *this = aFrom;
        }
    }

    inline void Quaternion::FromTransfMat(const float aMat[16])
    {
        FromTransfMat(Matrix(aMat));
    }

    inline void Quaternion::FromTransfMat(const Matrix& aMat)
    {
        float tr = aMat.m_E11 + aMat.m_E22 + aMat.m_E33;

        if (tr > 0)
        {
            float Sum = Maths::sqrtf(tr+1.f) * 2.f; // S=4*qw
            s = 0.25f * Sum;
            x = (aMat.m_E32 - aMat.m_E23) / Sum;
            y = (aMat.m_E13 - aMat.m_E31) / Sum;
            z = (aMat.m_E21 - aMat.m_E12) / Sum;
        } else if ((aMat.m_E11 > aMat.m_E22)&(aMat.m_E11 > aMat.m_E33)) {
            float Sum = Maths::sqrtf(1.f + aMat.m_E11 - aMat.m_E22 - aMat.m_E33) * 2.f; // S=4*qx
            s = (aMat.m_E32 - aMat.m_E23) / Sum;
            x = 0.25f * Sum;
            y = (aMat.m_E12 + aMat.m_E21) / Sum;
            z = (aMat.m_E13 + aMat.m_E31) / Sum;
        } else if (aMat.m_E22 > aMat.m_E33) {
            float Sum = Maths::sqrtf(1.f + aMat.m_E22 - aMat.m_E11 - aMat.m_E33) * 2; // S=4*qy
            s = (aMat.m_E13 - aMat.m_E31) / Sum;
            x = (aMat.m_E12 + aMat.m_E21) / Sum;
            y = 0.25f * Sum;
            z = (aMat.m_E23 + aMat.m_E32) / Sum;
        } else { 
            float Sum = Maths::sqrtf(1.f + aMat.m_E33 - aMat.m_E11 - aMat.m_E22) * 2; // S=4*qz
            s = (aMat.m_E21 - aMat.m_E12) / Sum;
            x = (aMat.m_E13 + aMat.m_E31) / Sum;
            y = (aMat.m_E23 + aMat.m_E32) / Sum;
            z = 0.25f * Sum;
        }
    }


    inline Quaternion Quaternion::operator*(const Quaternion& aRef)
    {
        /*
         float tx = x;
         float ty = y;
         float tz = z;
         float ts = s;

         Quaternion ret;
         ret.s = (ts * aRef.s) - (tx * aRef.x) - (ty * aRef.y) - (tz * aRef.z);
         ret.x = (tx * aRef.s) + (ts * aRef.x) + (ty * aRef.z) - (tz * aRef.y);
         ret.y = (ty * aRef.s) + (ts * aRef.y) + (tz * aRef.x) - (tx * aRef.z);
         ret.z = (tz * aRef.s) + (ts * aRef.z) + (tx * aRef.y) - (ty * aRef.x);
         return ret;
         /*/
        const float tw = (s * aRef.s) - (x * aRef.x) - (y * aRef.y) - (z * aRef.z);
        const float tx = (x * aRef.s) + (s * aRef.x) + (y * aRef.z) - (z * aRef.y);
        const float ty = (y * aRef.s) + (s * aRef.y) + (z * aRef.x) - (x * aRef.z);
        const float tz = (z * aRef.s) + (s * aRef.z) + (x * aRef.y) - (y * aRef.x);

        Quaternion ret(tx, ty, tz, tw);

        return ret;
        //*/
    }

    inline Quaternion Quaternion::operator*(const Vec3& aRef)
    {
        /*
         float ts = s;
         float tx = x;
         float ty = y;
         float tz = z;

         Quaternion ret;
         ret.s = - (tx * aRef.X()) - (ty * aRef.Y()) - (tz * aRef.Z());
         ret.x =   (ts * aRef.X()) + (ty * aRef.Z()) - (tz * aRef.Y());
         ret.y =   (ts * aRef.Y()) + (tz * aRef.X()) - (tx * aRef.Z());
         ret.z =   (ts * aRef.Z()) + (tx * aRef.Y()) - (ty * aRef.X());
         return ret;
         /*/
        const float tw = - (x * aRef.x) - (y * aRef.y) - (z * aRef.z);
        const float tx =   (s * aRef.x) + (y * aRef.z) - (z * aRef.y);
        const float ty =   (s * aRef.y) + (z * aRef.x) - (x * aRef.z);
        const float tz =   (s * aRef.z) + (x * aRef.y) - (y * aRef.x);
        
        Quaternion ret(tx, ty, tz, tw);
        
        return ret;
        //*/
    }
    
    
    inline void Vec3::SelfRotate(float Angle, const Vec3& Axe)
    {
        //        const float SinHalfAngle = sinf(Maths::DegreeToRadian(Angle/2));
        //        const float CosHalfAngle = cosf(Maths::DegreeToRadian(Angle/2));
        //
        //        const float Rx = Axe.x * SinHalfAngle;
        //        const float Ry = Axe.y * SinHalfAngle;
        //        const float Rz = Axe.z * SinHalfAngle;
        //        const float Rw = CosHalfAngle;
        Quaternion RotationQ (Axe, Angle);
        
        Quaternion ConjugateQ;
        RotationQ.Conjugate(ConjugateQ);
        
        Quaternion W = RotationQ * (*this) * ConjugateQ;
        
        x = W.x;
        y = W.y;
        z = W.z;
    }
}

#endif
/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 03-03-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#ifndef Engine_PrefixHeader_pch
#define Engine_PrefixHeader_pch

// Include any system framework and library headers here that should be included in all compilation units.
// You will also need to set the Prefix Header build setting of one or more of your targets to reference this file.
#include "Engine/EngineType.h"
#include "Common/Logger.h"


/**
 * OpenGL defines
 */
#ifdef __APPLE_CC__         // mac
#   define MAJOR_VERSION 4
#   define MINOR_VERSION 1
#else                       // windows
#   define MAJOR_VERSION 4
#   define MINOR_VERSION 1
#endif

/**
 * using modules defines
 */
#define USING_GLFW
#define USING_FREEIMAGE_LIB

/**
 * includes files according to modules defines
 */
#ifdef USING_GLFW
#   ifdef __APPLE_CC__         // mac
#       define GLFW_INCLUDE_GLCOREARB
#   else
#       ifdef WIN32            // windows
//#			define GL_GLEXT_PROTOTYPES
#           include "GL/glew.h"
//#           define GLFW_INCLUDE_GLU
//#       define GLFW_INCLUDE_GLCOREARB
#       endif
#   endif
#   include "GL/glfw3.h"
#endif

#ifdef USING_FREEIMAGE_LIB		
#	define FREEIMAGE_LIB
#endif // USING_FREEIMAGE_LIB


#include <memory>
#define CommonSharedPtr std::shared_ptr
#define CommonEnableSharedFromThis std::enable_shared_from_this
#define CommonDynamicSharedCast std::dynamic_pointer_cast
#define CommonSharedCastToConst std::const_pointer_cast

template <typename T, int N>
int GetLength(const T (&/*a_Array*/) [N])
{
    return N;
}

#include "FreeImage.h"

#include "Common/CommonString.h"
#include "Common/CommonList.h"
#include "Common/CommonFile.h"
#include "Common/CommonVector.h"
#include "Common/CommonMap.h"

namespace Engine
{
    class Component;
    class GraphicComponent;
    // array of component
//    typedef CommonList<CommonSharedPtr<Component> > ArrayComponents;
    typedef CommonMap<const std::type_info* , CommonSharedPtr<Component> > ArrayComponents;
	typedef CommonList<CommonSharedPtr<GraphicComponent> > ArrayGraphicComponents;

}


#ifdef _WIN32
#   define ASSERT(x, y)  if(!(x)) {_RPTF0(_CRT_ASSERT, y);}

#elif defined __APPLE_CC__
#   define ASSERT(x,y)	 if(!(x)) {Logger::LogDebugSoft(y, DEBUG_SOFT_INFO);}
#endif


#if defined _DEBUG || DEBUG
	inline void ClearGlError()
	{
		glGetError();
	}

	inline GLenum CheckGlError(const char* a_msg, const char* aFileName, int aLine, const char* aMethod)
	{
		GLenum t_Error = glGetError();
		if (t_Error != GL_NO_ERROR)
		{
			Engine::Logger::LogDebug(a_msg, aFileName, aLine, aMethod);
			switch (t_Error)
			{
				case GL_INVALID_ENUM:
					Engine::Logger::Printf("\tGL_INVALID_ENUM\n");
					break;
				case GL_INVALID_VALUE:
					Engine::Logger::Printf("\tGL_INVALID_VALUE\n");
					break;
				case GL_INVALID_OPERATION:
					Engine::Logger::Printf("\tGL_INVALID_OPERATION\n");
					break;
				default:
					Engine::Logger::Printf("\t%d\n",t_Error);
					break;
			}
		}
		return t_Error;
	}
#else
    inline void ClearGlError() {}
	inline GLenum CheckGlError(const char* , const char* , int , const char* ) { return 0;}
#endif


#endif



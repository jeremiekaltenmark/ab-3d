/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 21-02-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#ifndef Engine_Application_H
#define Engine_Application_H

#include "IO/IOManager.h"

#include "Common/Singleton.h"
#include "Components/Scene.h"

#include "Engine/Engine.h"

namespace Engine
{
    class Application : public Engine::Singleton<Application>
    {
        friend class Engine::Singleton<Application>;
    public:
        virtual void Run() ;

		int AddScene(const CommonSharedPtr<Scene>& aScene);
		CommonSharedPtr<IOManager>& GetIOManager() { return mManager; }

        CommonSharedPtr<Scene>& GetScene() { return mCurrentScene; }
    private:
        Application();
        Application(const Application& aCopy); // disable copy of application
        virtual ~Application();

        typedef CommonList< CommonSharedPtr<Scene> > SceneArray;
		CommonSharedPtr<Scene> mCurrentScene;
        SceneArray mScenes;

		CommonSharedPtr<IOManager> mManager;
        
    };


}

#endif // Engine_Application_H

/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 10-03-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#ifndef Engine_EngineType_h
#define Engine_EngineType_h

#include <stdint.h>
typedef unsigned int uint;
typedef unsigned long ulong;
typedef unsigned char uchar;

typedef uint8_t					ui8;
typedef uint16_t				ui16;
typedef uint32_t				ui32;
typedef uint64_t				ui64;

typedef int8_t					i8;
typedef int16_t					i16;
typedef int32_t                 i32;
typedef int64_t					i64;

/**
 * usefull operators
 */
#define NOT   !
#define OR    ||
#define AND   &&

#define int_MAX  INT_MAX
#define int_MIN  INT_MIN

#define DELETE_OBJECT(o) {if((o) != NULL) {delete(o); (o) = NULL;}}

#define MAKE_COLOR(R, G, B) (R << 16 | G << 8 | B)

#if defined ( WIN32 )
#define __func__ __FUNCTION__
#endif

/**
 * Common defines
 */
#define WINDOW_WIDTH 600.0f
#define WINDOW_HEIGHT 600.0f
#define ENGINE_FAILURE -1
#define ENGINE_SUCCESS 1
#define INVALID_OGL_VALUE 0xFFFFFFFF
#define DEBUG_INFO __FILE__, __LINE__, __func__
#define DEBUG_SOFT_INFO __FILE__, __LINE__

//enum COMPONENT_TYPE {
//    GENERIC,
//    NODE,
//    GRAPHIC,
//    TRANSFORMATION,
//    VIEW_MATRIX,
//    CAMERA,
//    SCENE,
//
//    CUBE,
//    TRI_PYR,
//    PLAN,
//    MESH,
//
//    SHADER,
//    PROGRAM,
//    LIGHT,
//
//    COUNT};


#endif

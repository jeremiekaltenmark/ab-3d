/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 21-02-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */

#include "Engine/Engine.h"
#include "Engine/Application.h"
#include "Common/Singleton.cpp"
#include "IO/GLFWOutput.h"

template class Engine::Singleton<Engine::Application>;

using namespace Engine;


////////////////////////////////////////////////////////////////////////
Application::Application() : mManager(new IOManager())
{
#ifdef USING_FREEIMAGE_LIB
	FreeImage_Initialise();
#endif
	mManager->Init();
}

//////////////////////////////////////////////////////////////////////
Application::Application(const Application& aCopy)
{
    Logger::LogDebug("Ridiculous copy call on Application... --\' ", DEBUG_INFO);
	(void) aCopy;
    // TODO : implement me
}

////////////////////////////////////////////////////////////////////////
Application::~Application()
{
//    mScenes.clear();
    // TODO : implement me
    //Logger::LogDebugSoft("destroy Application", DEBUG_SOFT_INFO);

#ifdef USING_FREEIMAGE_LIB
	FreeImage_DeInitialise();
#endif

	mManager.reset();
}

////////////////////////////////////////////////////////////////////////
void Application::Run()
{
    // TODO : implement me
	std::cout << std::endl << " -- CDRIN Engine -- \n" << std::endl;

    while (!mManager->ShouldExit())
    {
        if(mCurrentScene == NULL)
        {
            if(mScenes.GetLength() == 0)
            {
                Logger::LogLine("No scene added");
                return;
            }
            mCurrentScene = (mScenes.getFirst());
        }

        mCurrentScene->SceneUpdate(0);
		mManager->Update(0, mCurrentScene->GetSceneGraph());
    }
}

int Application::AddScene(const CommonSharedPtr<Scene>& aScene)
{
    mScenes.AddLast(aScene);
    mCurrentScene = aScene;
    return (int) mScenes.GetLength()-1;
}

#version 410

in vec3 m_fragmentColor;
//in vec3 m_Normal;
//in vec3 m_WorldPos;
out vec4 FragColor;

void main()
{
    FragColor = vec4(m_fragmentColor, 1.0);
}


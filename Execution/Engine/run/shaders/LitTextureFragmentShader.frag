#version 410

struct BaseLight				
{
	float AmbientIntensity;		// 1*4 +
	float DiffuseIntensity;		// 1*4 + 
	vec3  Color;				// 3*4 
};								// pack of 5*4

struct DirectionalLight
{
	BaseLight Base;				// 5*4
	vec3  Direction;			// 3*4
};								// pack de 8*4

struct Attenuation
{
	float Constant;				// 1*4 +
	float Linear;				// 1*4 + 
	float Exp;					// 1*4 +
};								// pack de 3*4 

struct PointLight
{
	BaseLight Base;				// 5*4 + 
	Attenuation Att;			// 3*4 +
	vec3 Position;				// 3*4
};								// pack de 11*4


uniform DirectionalLight g_DirectionalLight;
uniform PointLight g_PointLight;
uniform sampler2D gSampler;
uniform sampler2D gShadowMap;
uniform sampler2D gNormalMap;
uniform vec3 CameraPosition;
uniform float matSpecIntens;
uniform float SpecPower;

subroutine void RenderPassType();
subroutine uniform RenderPassType RenderPass;

in vec2 m_TexCoord;
in vec3 m_Normal;
in vec3 m_Tangent;
in vec3 m_WorldPos;
in vec4 LightSpacePos;

out vec4 FragColor;


float CalcShadowFactor(vec4 aLightSpacePos);
vec3 CalcNormal();

subroutine(RenderPassType)
void shadeWithShadow()
{
    vec4 t_AmbientColor = vec4(g_DirectionalLight.Base.Color, 1.0f) * g_DirectionalLight.Base.AmbientIntensity;
    vec3 t_LightDirection = -g_DirectionalLight.Direction;
    vec3 t_Normal = CalcNormal(); //normalize(m_Normal);
    float t_DiffuseFactor = dot(t_Normal, t_LightDirection);
    float ShadowFactor = CalcShadowFactor(LightSpacePos);

    vec4 t_DiffuseColor = vec4(0,0,0,0);
    vec4 t_SpecularColor = vec4(0,0,0,0);

    if (t_DiffuseFactor > 0)
    {
        t_DiffuseColor = vec4(g_DirectionalLight.Base.Color, 1.0f) * g_DirectionalLight.Base.DiffuseIntensity * t_DiffuseFactor;

        vec3 t_VertToCam = normalize(CameraPosition - m_WorldPos);
        vec3 t_LightReflect = normalize(reflect(g_DirectionalLight.Direction, t_Normal));
        float t_SpecularFactor = dot(t_VertToCam, t_LightReflect);
        t_SpecularFactor = pow(t_SpecularFactor, SpecPower);

        if(t_SpecularFactor > 0)
        {
            t_SpecularColor = vec4(g_DirectionalLight.Base.Color, 1.0f) * matSpecIntens * t_SpecularFactor;
        }
    }

    FragColor = texture(gSampler, m_TexCoord.st) * (t_AmbientColor +  ShadowFactor * (t_DiffuseColor + t_SpecularColor));
}

subroutine(RenderPassType)
void RecordDepth()
{
    FragColor = texture(gSampler, m_TexCoord.st);
}

float CalcShadowFactor(vec4 aLightSpacePos)
{
	// perspective devide
	vec3 ProjCoords = aLightSpacePos.xyz / aLightSpacePos.w; 
	
	// screen space
	vec2 UVCoords;
	UVCoords.x = 0.5 * ProjCoords.x + 0.5;					
	UVCoords.y = 0.5 * ProjCoords.y + 0.5;
	float z = 0.5 * ProjCoords.z + 0.5;

	// depth test
	float Depth = texture(gShadowMap, UVCoords).x;

	if (Depth < (z - 0.00001))
		return 0.5;
	else
		return 1.0;
}

vec3 CalcNormal()
{
    vec3 t_Normal = normalize(m_Normal);
    vec3 t_Tangent = normalize(m_Tangent);
    t_Tangent = normalize(t_Tangent - dot(t_Tangent, t_Normal) * t_Normal); //Gramm-Schmidt process
    vec3 t_BiTangent = cross(t_Tangent, t_Normal);
    vec3 t_BumpMapNormal = texture(gNormalMap, m_TexCoord).xyz; // [0, 1]
    t_BumpMapNormal = 2.0 * t_BumpMapNormal - vec3(1.0, 1.0, 1.0); // [-1, 1]

    vec3 returnNormal;
    mat3 TBN = mat3(t_Tangent, t_BiTangent, t_Normal);
    returnNormal = TBN * t_BumpMapNormal;
    returnNormal = normalize(returnNormal);
    return returnNormal;

}

void main()
{
    RenderPass();
}

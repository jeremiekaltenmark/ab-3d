#version 410

in vec2 TexCoord0;
out vec4 FragColor;

uniform sampler2D gSampler;

void main()
{
//    FragColor = vec4(0.5,0.5,0.5, 1.0);
//    FragColor.y = texture2D(gSampler, TexCoord0.xy).y;
    FragColor = texture(gSampler, TexCoord0.st);
}

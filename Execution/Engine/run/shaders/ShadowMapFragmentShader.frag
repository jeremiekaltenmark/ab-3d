#version 410

in vec2 TexCoord0;
out vec4 FragColor;

uniform sampler2D gReadShadowMap;
//uniform sampler2DShadow gShadowMap;

void main()
{
    float Depth = texture(gReadShadowMap, TexCoord0).x;
    Depth = 1.0 - (1.0 - Depth) * 25.0;
//    if (Depth < 1.0)
//    {
//        FragColor = vec4(Depth, Depth, Depth, 1);
//    }
//    else
//    {
        FragColor = vec4(Depth);
//    }


}

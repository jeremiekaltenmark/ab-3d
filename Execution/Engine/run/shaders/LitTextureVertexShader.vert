#version 410                                                                  

layout (location = 0) in vec3 Position;
layout (location = 1) in vec2 TexCoord;
layout (location = 2) in vec3 Normal;
layout (location = 3) in vec3 Tangent;

// check if this 2 uniforms can be optimized: MVP contains World,
// is it worth doing MVP computation in shader ?
//uniform mat4 MVP;
uniform mat4 World;
uniform mat4 ViewMat;
uniform mat4 ProjectionMat;
uniform mat4 LightMat;

out vec2 m_TexCoord;
out vec3 m_Normal;
out vec3 m_Tangent;
out vec3 m_WorldPos;
out vec4 LightSpacePos;

void main()                                                                    
{
    mat4 MVP = ProjectionMat * ViewMat * World;
	gl_Position = MVP * vec4(Position, 1.0);
    mat4 LightMVP = ProjectionMat * LightMat * World;
	LightSpacePos = LightMVP * vec4(Position, 1.0);
	m_TexCoord = TexCoord;
// 0.0 in vec4 of the next line is to nullify the translation component of World matrix
// (as normals don't have an origin)
	m_Normal = (World * vec4(Normal, 0.0)).xyz;
    m_Tangent = (World * vec4(Tangent, 0.0)).xyz;
	m_WorldPos = (World * vec4(Position, 1.0) ).xyz;

}

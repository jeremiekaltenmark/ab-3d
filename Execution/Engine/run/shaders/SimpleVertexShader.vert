#version 410                                                                  

layout (location = 0) in vec3 Position;                                       

uniform mat4 MVP;
uniform float mUniform1;

out vec3 m_fragmentColor;
//out vec3 m_Normal;
//out vec3 m_WorldPos;

void main()                                                                    
{                                                                             
    gl_Position = MVP * vec4(Position, 1.0);
    m_fragmentColor = clamp(Position * mUniform1, 0.0, 1.0f);
//	m_Normal = vec3(0.f,1.f,0.f);
}


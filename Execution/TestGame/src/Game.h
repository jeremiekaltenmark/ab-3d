/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 21-02-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */


#ifndef TestGameGame_H
#define TestGameGame_H

#include "Components/Scene.h"

namespace Engine
{
    class FBO;
	class Material;
    class Renderer;
}

namespace TestGame
{

    class Game : public Engine::Scene
    {
    public:
        Game();
        virtual ~Game();
        void NewSilhouette(CommonSharedPtr<Engine::FBO>& );
        void Inputs(int(&a_inputs)[10][2]);

    protected:
        Game(const Game& aCopy);

		void AddGround(CommonSharedPtr<Engine::Material>& a_Material);
		void AddVisualLight(CommonSharedPtr<Engine::Material>& t_SimpleMat);
		void AddCubesCylinder(CommonSharedPtr<Engine::Material>& t_BoxLitMat);
		void AddMesh();
		void ManageRenderPass();

		void Update(float aElapsedTime);


		CommonSharedPtr<Engine::GraphicComponent> mBase;
		CommonSharedPtr<Engine::GraphicComponent> mLightPhys;
        CommonSharedPtr<Engine::GraphicComponent> mX;
        CommonSharedPtr<Engine::GraphicComponent> mY;
        CommonSharedPtr<Engine::GraphicComponent> mZ;

    private:
        CommonSharedPtr<Engine::Renderer> m_Renderer;
    };
}


#endif // TestGame_Game_H

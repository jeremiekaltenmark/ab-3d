/**
 *  This file is part of Engine AB-3D.
 *
 *  Engine AB-3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Engine AB-3D is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Engine AB-3D.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	Project PER - internal Game Engine
 *	Organisation: www.jeremkalt.xyz
 *
 *	Author: J�r�mie Kaltenmark (contact@jeremkalt.xyz)
 *	Created on: 21-02-2014
 *
 *	Copyright (c) 2014 CDRIN. All rights reserved.
 */



#include "Game.h"
#include "Engine/Engine.h"
#include "Components/GraphicPrimitives/Plan.h"
#include "Components/GraphicPrimitives/Cube.h"
#include "Components/GraphicPrimitives/TriPyramid.h"
#include "Components/GraphicPrimitives/Mesh.h"
#include "Maths/Vec3.h"
#include "Common/Logger.h"

#include "Rendering/GraphicDevice.h"
#include "Components/Material/Texture.h"
#include "Components/Material/Material.h"

#include "IO/IOManager.h"
#include "IO/Camera.h"
#include "IO/AOutput.h"
#include "Engine/Application.h"

#include "Rendering/FBO.h"
#include "Rendering/ShadowMapFBO.h"
#include "Rendering/ShadowMapRender.h"


using namespace Engine;
using namespace TestGame;

//namespace TestGame
//{

////////////////////////////////////////////////////////////////////////
Game::Game()
{
    GraphicDevice* t_Device = GraphicDevice::GetSingleton();

	CommonSharedPtr<Texture> t_SimpleTexture = t_Device->GetTexture("textures/damier.png");
	CommonSharedPtr<Material> t_SimpleMat = t_Device->GetMaterial(GraphicDevice::PROGRAM_LIT_TEXTURE_3D, t_SimpleTexture);

	//CommonSharedPtr<Texture> t_BricksTexture = t_Device->GetTexture("textures/bricks.jpg");
	//CommonSharedPtr<Texture> t_BricksNormalMap = t_Device->GetTexture("textures/normal_map.jpg");
	//CommonSharedPtr<Material> t_BricksMat = t_Device->GetMaterial(GraphicDevice::PROGRAM_LIT_TEXTURE_3D, t_BricksTexture, NULL, t_BricksNormalMap);
	CommonSharedPtr<Texture> t_MetalTexture = t_Device->GetTexture("textures/metal.jpg");
	CommonSharedPtr<Texture> t_MetalNormalMap = t_Device->GetTexture("textures/metal-normal.jpg");
	CommonSharedPtr<Material> t_MetalMat = t_Device->GetMaterial(GraphicDevice::PROGRAM_LIT_TEXTURE_3D, t_MetalTexture, NULL, t_MetalNormalMap);

	CommonSharedPtr<Texture> t_FaceTexture = t_Device->GetTexture("textures/face.jpg");
	CommonSharedPtr<Texture> t_FaceNormalMap = t_Device->GetTexture("textures/face-normal.jpg");
	CommonSharedPtr<Material> t_FaceMat = t_Device->GetMaterial(GraphicDevice::PROGRAM_LIT_TEXTURE_3D, t_FaceTexture, NULL, t_FaceNormalMap);

	//CommonSharedPtr<Texture> t_BoxTexture = t_Device->GetTexture("textures/Box.png");
	CommonSharedPtr<Texture> t_BoxTexture = t_Device->GetTexture("textures/face.jpg");
	CommonSharedPtr<Texture> t_BoxNormalTexture = t_Device->GetTexture("textures/face-normal.jpg");
    CommonSharedPtr<Material> t_BoxLitMat = t_Device->GetMaterial(GraphicDevice::PROGRAM_LIT_TEXTURE_3D, t_BoxTexture, t_BoxNormalTexture);
    

	AddVisualLight(t_SimpleMat);
	AddMesh();
    Scene::AddRepereToScene();
	AddGround(t_FaceMat);
	//AddGround(t_BoxLitMat);
//	AddCubesCylinder(t_BoxLitMat);

	ManageRenderPass();

	Application::GetSingleton()->GetIOManager()->GetOutputFromIndex(0)->GetCamera()->SetPosition(Engine::Vec3(0, 10, -15));
	Application::GetSingleton()->GetIOManager()->GetOutputFromIndex(0)->GetCamera()->LookAt(Engine::Vec3(1, 1, 3));


    std::function<void(int(&)[10][2])> t_cb = std::bind(&Game::Inputs, this, std::placeholders::_1);
    Application::GetSingleton()->GetIOManager()->GetOutputFromIndex(0)->RegisterInputCallback(t_cb);
}

////////////////////////////////////////////////////////////////////////
void TestGame::Game::Inputs(int(&a_inputs)[10][2])
{
    for (int i = 0; i < 10; ++i)
    {
        if (a_inputs[i][0] == GLFW_KEY_F)
        {
            int passtype = m_Renderer->GetPassType();

            if (a_inputs[i][1] == GLFW_PRESS && passtype != Renderer::SHADOWPASS)
                m_Renderer->SetPass(Renderer::SHADOWPASS);
            else if (a_inputs[i][1] == GLFW_RELEASE && passtype != (Renderer::SHADOWPASS | Renderer::FORWARDPASS))
                m_Renderer->SetPass(Renderer::SHADOWPASS | Renderer::FORWARDPASS);
            
        }
    }
}

////////////////////////////////////////////////////////////////////////
Game::Game(const Game& aCopy)
{
	(void) aCopy;
}

////////////////////////////////////////////////////////////////////////
Game::~Game()
{
    //Engine::Logger::LogDebugSoft("destroy Game", DEBUG_SOFT_INFO);
}

////////////////////////////////////////////////////////////////////////
void Game::Update(float aElapsedTime)
{
	static float tAdd = 0;
    (void) aElapsedTime;
//     GetLight()->RotateY(0.01f);
    tAdd += 0.01f;
//    Vec3 tPos = Vec3(5.f, 6 * Maths::cosf(tAdd), 6 * Maths::sinf(tAdd));
    Vec3 tPos = Vec3(100 * Maths::cosf(tAdd), 20.f, 100 * Maths::sinf(tAdd));
	GetLight()->UpdatePosition(tPos);
	GetLight()->LookAt(Engine::Vec3(0,5,0));

    mLightPhys->GetTransform()->SetPosition(tPos - Engine::Vec3(-1,0,0));
    Quaternion tQuat;

    Matrix& tViewMat = GetLight()->GetViewMatrix()->GetViewMatrix();
    tQuat.FromTransfMat(tViewMat);

    mLightPhys->GetTransform()->SetRotation(tQuat);

}


////////////////////////////////////////////////////////////////////////
void Game::NewSilhouette(CommonSharedPtr<Engine::FBO>& a_sil )
{
    float* t_Pix = (float*)a_sil->GetDepthPixels();

    delete[] t_Pix;
}


void TestGame::Game::ManageRenderPass()
{
    m_Renderer = Application::GetSingleton()->GetIOManager()->GetOutputFromIndex(0)->GetRenderer();
    m_Renderer->SetPass(Renderer::SHADOWPASS | Renderer::FORWARDPASS);
//    m_Renderer->SetPass(Renderer::FORWARDPASS);

//    std::function<void(CommonSharedPtr<FBO>&)> t_cb = std::bind(&Game::NewSilhouette, this, std::placeholders::_1);
//    ShadowMapRender* t_shadRend = (ShadowMapRender*)(m_Renderer->GetPass(Renderer::SHADOWPASS));
//
//    t_shadRend->RegisterFBOCallback(t_cb);
}

void TestGame::Game::AddCubesCylinder(CommonSharedPtr<Material>& t_BoxLitMat)
{
	CommonSharedPtr<Engine::GraphicComponent> tGraph;
        int i = 0, j = 0;
	//cube
	for (j = -30; j < 100; j += 30)
	{
		for (i = 0; i < 628; i += 30)
		{
			tGraph = CommonSharedPtr<Engine::GraphicComponent>(new Engine::Mesh("models/Cube.3DS"));
			tGraph->GetTransform()->SetScale(25,25,25);
			tGraph->GetTransform()->Move(cos((i % 314)*0.01f) * 10, sin((j % 314)*0.01f) * 10, sin(i*0.01f) * 10);
//			tGraph->GetTransform()->Move(0,1,0);
//			tGraph->SetColor(1);
			tGraph->SetMaterial(t_BoxLitMat);
			Scene::m3DObjects.AddFirst(tGraph);
		}
	}
}

void TestGame::Game::AddGround(CommonSharedPtr<Material>& a_Material)
{
	CommonSharedPtr<Engine::GraphicComponent> tGraph ;
	const int Min = -15;
	const int Max = -Min;
    int i = 0, j = 0;

	for (i = Min; i < Max; ++i)
		for (j = Min; j < Max; ++j)
		{
			tGraph = CommonSharedPtr<Engine::GraphicComponent>(new Engine::Plan());
			tGraph->GetTransform()->Move(i*4.f, 0, j*4.f);
			tGraph->GetTransform()->SetScale(2, 2, 2);
            tGraph->SetMaterial(a_Material);

			Scene::m3DObjects.AddFirst(tGraph);

			if (i == 0 && j == 0)
				mBase = tGraph;
		}

}

void TestGame::Game::AddMesh()
{
	//*/
	CommonSharedPtr<Engine::GraphicComponent> tGraph = CommonSharedPtr<Engine::Mesh>(new Engine::Mesh("models/Pergola.3DS"));
	tGraph->GetTransform()->SetPosition(Vec3(15.f, 0.f, 10.f));
	tGraph->GetTransform()->SetScale(Vec3(0.1f, 0.1f, .1f));
	Scene::m3DObjects.AddFirst(tGraph);
	//*/
	tGraph = CommonSharedPtr<Engine::Mesh>(new Engine::Mesh("models/phoenix_ugv.md2"));
	tGraph->GetTransform()->SetPosition(Vec3(0.f, 0.f, 0.f));
	tGraph->GetTransform()->SetScale(Vec3(2.f, 2.f, 2.f));
	Scene::m3DObjects.AddFirst(tGraph);
	//*/
}

void TestGame::Game::AddVisualLight(CommonSharedPtr<Material>& a_Mat)
{
    CommonSharedPtr<Engine::GraphicComponent> tGraph(new Engine::Cube());//(new Engine::Mesh("models/Cone.3DS"));
    tGraph->GetTransform()->SetRotation(Engine::Quaternion(Vec3(1, 0, 0), -Engine::PI1_2));
    tGraph->GetTransform()->SetPosition(Engine::Vec3(0,0,-2));
    tGraph->GetTransform()->SetScale(0.1f, 0.1f, 0.1f);

	//tGraph->SetColor(-1);
	tGraph->SetMaterial(a_Mat);
	Scene::m3DObjects.AddFirst(tGraph);
	mLightPhys = tGraph;
//    Vec3 tForward(0.0f, 0.0f, 1.0f);
//    Vec3 tUp(0.0f, 1.0f, 0.0f);
//
//    Vec3 tRight = tUp.Cross(tForward);  tRight.Normalize();
//    tUp = tForward.Cross(tRight);       tUp.Normalize();
//    tForward = tRight.Cross(tUp);       tForward.Normalize();
//
////    Vec3 tScale(0.01f, 0.01f, 0.01f);
//    Vec3 tScale(0.1f, 0.1f, 0.1f);
////    Vec3 tScale(10.f, 10.f, 10.f);
//    tRight -= tScale*2;
//    tForward -= tScale*2;
//    tUp -= tScale*2;
//    tRight /= tScale;
//    tForward /= tScale;
//    tUp /= tScale;
//
//
//    // x
//    tGraph = CommonSharedPtr<Engine::GraphicComponent>(new Engine::Cube());
//    tGraph->GetTransform()->Move(tRight);
//    tGraph->GetTransform()->SetScale(tRight/2);
//    tGraph->SetMaterial(a_Mat);
//    tGraph->SetColor(-1.0f);
//    tGraph->SetParent(mLightPhys.get());
//
//    // y
//    tGraph = CommonSharedPtr<Engine::GraphicComponent>(new Engine::Cube());
//    tGraph->GetTransform()->Move(tUp);
//    tGraph->GetTransform()->SetScale(tUp/2);
//    tGraph->SetMaterial(a_Mat);
//    tGraph->SetColor(1.0f);
//    tGraph->SetParent(mLightPhys.get());
//
//
//
//    // z
//    tGraph = CommonSharedPtr<Engine::GraphicComponent>(new Engine::Cube());
//    tGraph->GetTransform()->Move(tForward);
//    tGraph->GetTransform()->SetScale(tForward/2);
//    tGraph->SetMaterial(a_Mat);
//    tGraph->SetColor(0.5f);
//    tGraph->SetParent(mLightPhys.get());


    Vec3 tPos = Vec3(5.f, 6.f, 0.f);
    GetLight()->UpdatePosition(tPos);
    GetLight()->LookAt(Engine::ZeroVec3);

    mLightPhys->GetTransform()->SetPosition(tPos - Engine::Vec3(-1,0,0));
    Quaternion tQuat;

    Matrix& tViewMat = GetLight()->GetViewMatrix()->GetViewMatrix();
    tQuat.FromTransfMat(tViewMat);

    mLightPhys->GetTransform()->SetRotation(tQuat);


}

//} // end namespace TestGame